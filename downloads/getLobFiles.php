<!DOCTYPE html>
<html lang="en-us">
<head>
   <meta charset="UTF-8"><?php
   session_start(); ?>
   <title>Get Lob Files</title>
   <link rel="stylesheet" href="styles/stylesheet4iframeCERN.css">
</head>
<body>
   <div><?php
      $sessionId = session_id();
      $table = $_POST['table'];
      $idField = $_POST['idField'];
      $itemId = $_POST['itemId'];
      $itemName = $_POST['itemName'];
      $sql = "SELECT ".$idField.", ".$itemName." FROM ATLAS_MUON_NSW_MM_QAQC.".$table." WHERE ".$idField." ='".$itemId."'";
      $conn = oci_connect('ATLAS_MUON_NSW_MM_READER', 'NSWreadme2016', 'db-d0002.cern.ch:10654/int8r.cern.ch');
      $stid = oci_parse($conn, $sql);      
      $found = "false";
      oci_execute($stid);
      $i = 0;
      while($row = oci_fetch_array($stid, OCI_NUM+OCI_RETURN_LOBS+OCI_RETURN_NULLS)){
       	$found = ++$i;
       	$item = $row[1];
       	$tmpDir = "downloads/".$sessionId;
       	$FileName = $itemId."_".$itemName.".pdf";
       	$download = $tmpDir."/".$FileName;
         if(!is_dir($tmpDir)){mkdir($tmpDir, 0777, true);}
         $fileHandle = @fopen($download, "w");
         fwrite($fileHandle, $item);
         fclose($fileHandle);
      } ?>
      <div style="display:block; text-align:center;">
         <div>
            <p><b><?php echo $FileName; ?></b></p>
            <p>The Document is prepared for download!<br>You really want to download the file now?</p>
         </div>
         <div style="margin-top:30px;">
            <a class="back" href="<?php echo $download; ?>" download>YES</a>
            <a class="back" style="margin-left:20px;" href="#">NO</a>
         </div>
      </div>
   </div>
</body>