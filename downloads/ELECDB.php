<?php
  // error_reporting(E_ALL);
  // ini_set("display_errors", 1);
  //------------------------------
  // check permission, if not then kill the runtime
session_start();
$mainContent   = "";
$tableToChoose = "";
$formContent   = "";
$MaxRows = ""; 


if(!isset($_SESSION["username"])) exit(
				       "<div style=\"
       width: auto; height: auto;
	    margin: -40px 0 0 -150px;
	    padding: 30px 60px 30px 60px;
	    font-weight: bold;
       display: block;
       position: absolute;
       top: 50%; left: 50%;	 
	    background-color: #909090;
       color: #ffffff;
       -moz-border-radius: 3px;
       -webkit-border-radius: 3px;
       border-radius: 3px;      
       text-shadow: 2px 2px 7px #343434;
       -moz-box-shadow: inset 0 0 15px rgba(000,000,000, 0.15);
       -webkit-box-shadow: inset 0 0 15px rgba(000,000,000, 0.15);
       box-shadow: inset 0 0 15px rgba(000,000,000, 0.15);\">
       <p style=\"text-align:center;\">
          Please login first <a href=\"index.php\">here ...</a>
       </p>
    </div>");
//////////////////////////////////////////////////////////////////////////////
// get the page variable which tells ya, what ya gotta do (news, tables, etc.)
//////////////////////////////////////////////////////////////////////////////
$page = "";
$_SESSION['DBOwner'] = "ATLAS_MUON_NSW_ELEC_QAQC";
$username = $_SESSION['username'];
$isAdmin  = $_SESSION['isAdmin'];
$canEditElec  = $_SESSION['canEditElec'];




if (isset($_GET["page"])) $page = strtolower(trim($_GET["page"]));
if (isset($_POST["page"])) $page = strtolower(trim($_POST["page"]));
if ($page == "") $page="news"; // the default
// open the database connection

require('include/config.php');
if ($canEditElec)
  {
    $conn = oci_connect($userELEC, $passELEC, $connREADER);
  }
else
  {
    $conn = oci_connect($userREADER, $passREADER, $connREADER);
  }
$sql = "ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'";
$stid2 = oci_parse($conn, $sql);
oci_execute($stid2);    
if (!$conn) {
  $e = oci_error();
  trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
  echo 'error:'.$e;
}
//$playground = "playground"; // Development Site!!!
//////////////////////////////////////////////////////////////////////////////
// make navi stuff
//////////////////////////////////////////////////////////////////////////////
//$pages = array("news", "tables", "pviews", "cyoq", "downloads", "upload", "help", "contact", "logout.php"); 
//$links = array("News", "Tables", "Predefined Views", "DIY Query (Experts Only)", "Downloads", "Uploads", "Help / FAQ", "Contact", "Logout");
//$isLeft = array(true, true, true, true, true, true, false, false, false);
$pages = array("news","csearch", "pviews", "ameas", "tables", "help", "contact", "logout.php"); 
$links = array("News","Search","Predefined Views", "Add Measurment","Tables","Help / FAQ", "Contact", "Logout");
$isLeft = array(true, true, true, true,false, false, false, false);
$leftNavi = "";
$rightNavi = "";
for($i = 0; $i < count($pages); ++$i){
  $act = "";
  if($page == $pages[$i]) $act = " class=\"active elec\"";
  if(!strpos($pages[$i], ".php")) $pages[$i] = "?page=".$pages[$i];
  $line = "<li><a href=\"".$pages[$i]."\"".$act.">".$links[$i]."</a></li>";
  if($isLeft[$i]) $leftNavi .= $line;
  else $rightNavi .= $line;
}
//////////////////////////////////////////////////////////////////////////////
// make the news page
//////////////////////////////////////////////////////////////////////////////
if($page == "news") {
  $mainContent  = "<div class=\"whitediv\">\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "The purpose of this web page is to display the content
                          of the CERN Electronics Database for everyone who is interested.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "Most of the functionality is now in place. You can find all
                          the existing tables under the \"Tables\" tab, and some example
                          views can be found in the \"Predefined Views\" tab. Most of the
                          tables are still empty, since the actual data is still missing,
                          but the structure is already in place and the headers are displayed
                          even without data. You can do specialized searches under \"Custom Search\" and Add new measurments under \"Add Measurment\" (Privilages Required) ";
  $mainContent .=    "</p><br>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=    " To request any new functionality please contact ismet.siral@cern.ch";
  $mainContent .= "</div>\n";
}
//////////////////////////////////////////////////////////////////////////////
// make the tables page
//////////////////////////////////////////////////////////////////////////////
else if($page == "tables") {	
  $towner = "ATLAS_MUON_NSW_ELEC_QAQC";
	
  $form_tableToChoose = getGlobalVar("tableToChoose");
  $form_user          = getGlobalVar("user"       );
  $form_admin         = getGlobalVar("admin"       );
  $form_structures    = getGlobalVar("structures"  );
  $sql = "SELECT TABLENAME, TABLEDESCRIPTION, TABLETYPE, TABLEVIEWNAME FROM ".$towner.".DBCONTTABLES";
  //echo $sql;
  $stid = oci_parse($conn, $sql);
  oci_execute($stid);
  //echo $stid;
  $c0_user = array();
  $c0_admin = array();
  $c0_struc = array();
  $c1_user = array("Select a Table View");
  $c1_admin = array("Select a Table View");
  $c1_struc = array("Select a Table View");
  $c2 = array();
  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
#while($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
    switch($row[2]) {
    case "USER"         : array_push($c1_user,$row[1]); array_push($c0_user,$row[0]); break;
    case "ADMIN"        : array_push($c1_admin,$row[1]); array_push($c0_admin,$row[0]); break;
    case "STRUCTURE"    : array_push($c1_struc,$row[1]); array_push($c0_struc,$row[0]); break;
    }

  }
  // must come BEFORE the selectField stuff
  $table = getChosenTable($form_tableToChoose, $c0_user, $c0_admin, $c0_struc);

  //print_r($c0_type);
  $selectField = "<div class=\"whitediv\">";
  $selectField .= "<table class=\"noborder\">";
  $selectField .= "<tr> <td><b>User Tables:</b></td><td>";
  $selectField .= makeDropDown("user"       , $c1_user   , $form_user      , $form_tableToChoose, array("class=\"hideoption\""), "size=\"1\" onchange=\"this.form.elements['tableToChoose'].value='user';this.form.submit()\"");
  $selectField .= "</td><td><b>Admin Tables:</b></td><td>";
  $selectField .= makeDropDown("admin"       , $c1_admin , $form_admin     , $form_tableToChoose, array("class=\"hideoption\""), "size=\"1\" onchange=\"this.form.elements['tableToChoose'].value='admin';this.form.submit()\"");    
  $selectField .= "</td></tr><tr><td><b>DB Structure:</b></td><td>";
  $selectField .= makeDropDown("structures"  , $c1_struc, $form_structures , $form_tableToChoose, array("class=\"hideoption\""), "size=\"1\" onchange=\"this.form.elements['tableToChoose'].value='structures';this.form.submit()\"");
  $selectField .= "</td></tr></table>";

    
  $selectField .= "</div>";

  //$mainContent .= $testPrint;
  //echo $table;
    
  if($table != "" && $table != "none"){ 
    $sql = "SELECT * FROM ".$towner.".".$table; 
    $stid = oci_parse($conn, $sql);
    //echo $sql;
    #$testOutput = $table;
    #$mainContent .= $testOutput;
    oci_execute($stid);

    /*$tablePrint = "<p><b>Please be aware: </b>Especially Type and Structure Tables might not be easily human-readable. <br />
      The partly cryptic column titles will soon be replaced with better descriptions. <br />If you don't find the information you are looking for, maybe you need to ask for a special view.</p>";*/    
    
    //div open-------------------------------------------------------------------------------------------------------------------------------------
    $tablePrint .= "<div class=\"tablediv\">";
    
    #$tablePrint .= "<p>The selected table is <b><font color= #880064>".$table."</font></b>.</p>";
    $tablePrint .= "<table class=\"tablesorter floatThead\" id='t01'>\n";
    //$tablePrint .= "<table id=\"example\" class=\"dataTable display\" cellspacing=\"0\" width=\"100%\">\n";
    $invisiTable .= "<table class=\"invisiTable\" id='invisiHeader'>\n";

    // Print the column names as the headers of a table //
    $ncols = oci_num_fields($stid);
    $tablePrint .="<thead><tr>\n";
    $invisiTable .="<thead><tr>";
    /* $tablePrint.="<th class=\"vertical\"><div><b> Executing table: ".$sql." </b></div></th>\n";    */
    for ($i = 1; $i <= $ncols; ++$i) {
      if(oci_field_name($stid, $i) != "USERPASSWD") {
	$colName = oci_field_name($stid, $i);
	//echo $colName."\n";
	$found = "";
	 
	$shorttable = substr($table, 0, -4);
	#$query = 'SELECT comments FROM user_col_comments where column_name = \''.$colName.'\'';
         
	$query = 'SELECT DBCOLUMNFREETEXT FROM ATLAS_MUON_NSW_MM_LOG.WEBTRANSLATIONS where DBTABLENAME = \''.$shorttable.'\' AND DBTABLECOLUMN = \''.$colName.'\'';
	$comment = oci_parse($conn, $query);
	$commExecute = oci_execute($comment);



	while($commenttext = oci_fetch_array($comment, OCI_ASSOC+OCI_RETURN_NULLS)) {
	  //print_r ($commenttext);
	  //echo $commenttext["DBCOLUMNFREETEXT"];
	  if($commenttext["DBCOLUMNFREETEXT"] != ""){
	    $found = $commenttext["DBCOLUMNFREETEXT"];
	    break;
	  }
	}


	if ($found != "") {
	  $print_me = $found;
	} else {
	  $print_me = $colName;
	}

	$headerOutString .= $colName.";";
	if ($colName != "EDITTIME" && substr( $colName, 0, 3 ) != "ID_" ) {        	
	  $invisiTable .= "<th>".htmlentities($colName, ENT_QUOTES)."</th>\n";
	}
        
	$tablePrint .="<th class=\"vertical\"><div><b>".htmlentities($print_me, ENT_QUOTES)."</b></div></th>\n"; // was: $colName ... class=\"vertical\"
      }
    } // end for 
    //echo $headerOutString;
    //echo "<br />";
    //$headerOutString = substr_replace($headerOutString, '', -1);
    //echo $headerOutString;

    $tablePrint .="</tr></thead><tbody>\n";
    $invisiTable .="</tr></thead></table>\n";

#while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    $RowCount=0;


    $tmpMaxRows=getGlobalVar('MaxRows');
    if ($tmpMaxRows)
      {
	$MaxRows=$tmpMaxRows;
      }
    else
      {
	$MaxRows=100;
      }
    while($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS) and $RowCount<$MaxRows){
      $RowCount+=1;
      $tablePrint .= "<tr>\n";
      foreach ($row as $key => $item){
	//$isDate = strpos($item, '-16');
	//echo $item;
	//if ($isDate == 1) {
	// $a = date($item);
	//$tablePrint .= "    <td>" .  $item. "</td>\n"; 

	// echo $a;
	//} else {selectmenu green                     
	if($key == "PARTSBATCHMTFID") {
	  $pre = substr($item, 0, 5); 
	  $typ = substr($item, 5, 4);
	  $aft = substr($item, 9, 5);
	  $typ = "<span style=\"color:#990033; font-size:130%;\">".$typ."</span>";
	  $tog = "<b>".$pre."&nbsp;".$typ."&nbsp;".$aft."</b>";
	  $item = $tog;
	}
	if($key != "USERPASSWD") {
	  if($key == "PARTSBATCHMTFID") {
	    $tablePrint .= "    <td>" . $item. "</td>\n";
	  } else {
	    $tablePrint .= "    <td>" . ($item != null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
	  }
	}           
      }
		
      $tablePrint .= "</tr>\n";
    }
	
    $tablePrint .= "</tbody></table>\n";  
    $tablePrint .= "</div>";
    //div close--------------------------------------------------------------------------------------------------------------------------------------
    if( $RowCount>=$MaxRows )
      {
	$tablePrint .= "<form method=\"POST\" action=\"ELECDB.php?page=$page\" enctype=\"multipart/form-data\">";
	$tablePrint.=   "<input type=\"hidden\" name=\"page\" value=\"$page\">";
	$IncRows=$MaxRows+100;
	$tablePrint.= "<input type=\"hidden\" name=\"MaxRows\" value=\"$IncRows\">";    	
	$tablePrint.= "<input type=\"hidden\" name=\"tableToChoose\" value=\"$form_tableToChoose\">";    	   $tablePrint.= "<input type=\"hidden\" name=\"user\" value=\"$form_user\">";      
	$tablePrint.= "<input type=\"hidden\" name=\"admin\" value=\"$form_admin\">";    	   $tablePrint.= "<input type=\"hidden\" name=\"structures\" value=\"$form_structures\">";    	 	

	$tablePrint .= "<div id=\"button\"> <button type=\"button\" class=\"downloadbuttons elec\" onClick=\"this.form.submit()\">Load More $form_tableToChoose</button> </div>";
	$tablePrint .= "</form>";
      }
	
	
	
    $buttonContent .= "<div id=\"button\">";
    
    //$buttonContent .= "<button type=\"button\" onClick =\"$('.invisiTable').tableExport({type:'excel',escape:'false'});\">Download header as <b>EXCEL</b> file</button>";
    $buttonContent .= "<button type=\"button\" class=\"downloadbuttons elec\" onClick =\"$('.floatThead').tableExport({type:'csv',escape:'false'});\">Download table as <b>CSV</b> file</button>";
    $buttonContent .= "<button type=\"button\" class=\"downloadbuttons elec\" onClick =\"$('.floatThead').tableExport({type:'excel',escape:'false'});\">Download table as <b>EXCEL</b> file</button>";
    //$buttonContent .= "<button type=\"button\" onClick =\"$('.floatThead').tableExport({type:'pdf',escape:'false'});\">Download table as <b>PNG</b> file</button>";
    $buttonContent .= "<button type=\"button\" class=\"downloadbuttons elec\" onClick =\"$('.invisiTable').tableExport({type:'csv',escape:'false'});\">Download <b>import template</b> as <b>CSV</b> file</button>";
    $buttonContent .= "</div>";  
  }
    
      
      
      
               
   
  // put everything together into $mainContent
  // $mainContent .= $selectField . $buttonContent . $tablePrint . $invisiTable;
  // put NOT everything together into $mainContent
  $naviContent     = $selectField;
  $downloadButtons = $buttonContent;
  $mainContent     = $invisiTable . $tablePrint;

  
}


//////////////////////////////////////////////////////////////////////////////
// make the Predefined Views page
//////////////////////////////////////////////////////////////////////////////
else if($page == "pviews") {
  //
  $view_to_show = getGlobalVar("tableToChoose");
  //echo $view_to_show;
  //	
  $selectField  = "<div id='headerfield' class='whitediv'>
		                 <div class='sub2div'>
		                    <select name='pred_view' id='pred_view' class='selectmenu elec' size='1' onchange=\"this.form.elements['tableToChoose'].value='user';this.form.submit()\">
                             <option value=\"0\">---</option>
                             <option value=\"1\">WMM DeadChannels</option>                            
		                    </select>
		                 </div>
                   </div>";
  //
  if (isset($_POST["pred_view"])) {$submittedValue = $_POST["pred_view"];}
  //echo $submittedValue;
  //
  $selectField .= "<script type=\"text/javascript\">document.getElementById('pred_view').value = ".$submittedValue.";</script>";
  //

  if($submittedValue == 1) {
    //
    $sql   = "select EQENTRYID,CHANNEL from ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS where CONTEXTNAME = 'VMM3_deadChannel_TSU' and MEASVALUE='T'";
    $query = oci_parse($conn,$sql);
    $exec  = oci_execute($query);
    //
    $rows  = array();
    $table = array();
    $table['cols'] = array(array('id' => '','label' => 'EQENTRYID', 'type' => 'number'),
    			   array('id' => '','label' => 'CHANNEL', 'type' => 'number'),
    			   );
    //
    while($r = oci_fetch_array($query, OCI_ASSOC+OCI_RETURN_NULLS)) {
      // $temp = array();
      // //The below col names have to be in upper caps.
      $temp = array();
      $temp[] = array('v' => (string) $r["EQENTRYID"]);
      //
      $temp[] = array('v' => (float) $r["CHANNEL"]);
      $rows[] = array('c' => $temp);
    }
    //
    $table['rows'] = $rows;
    $jsonTable = json_encode($table);
    //
    $tablePrint .= "<script src='http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js'></script>
    			             <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    			             <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
    			             <script type='text/javascript'>
    			                google.load('visualization', '1', {packages:['corechart']});
    		                   google.setOnLoadCallback(drawChart);
    		                   function drawChart() {
    			                   var data = new google.visualization.DataTable(";
    $tablePrint .= json_encode($table);
    $tablePrint .= ")
    				                 var options = {
                               title: 'VMM Dead Channels', hAxis:{ title: 'EQ Id'}, vAxis:{ title: 'Channel'}

    				                 };
    			                    var chart = new google.visualization.ScatterChart(document.getElementById('piechart'));
    			                    chart.draw(data, options);
    			                    document.getElementById('png').outerHTML = '<a target=\"_blank\" href=\"' + chart.getImageURI() + '\">Printable version</a>';

    		                   }
    		                </script>
    			             <div class=\"whitediv\">
    			                <div class='plot' id='piechart' style='width:620px; height:500px;'></div>
    			                <div id='png'></div>
    			             </div>";
  }







  //     google.visualization.events.addListener(chart, 'ready', function () {
  // chart_div.innerHTML = '<img src=\"' + chart.getImageURI() + '\">';
  //       console.log(chart_div.innerHTML);
  //     });
  //
  $naviContent  = $selectField;
  $mainContent  = $tablePrint;	
}
//////////////////////////////////////////////////////////////////////////////
// make the Custom search
//////////////////////////////////////////////////////////////////////////////
else if($page == "csearch") {
  //
  $letter  = getProjectPattern($conn);
  $pattern = getPattern($conn);
  //
  $towner = "ATLAS_MUON_NSW_ELEC_QAQC";
  $sql  = "SELECT DISTINCT EQTYPEGROUPCODE FROM ".$towner.".ASPECTS";
  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);
  //
  $c0_types = array(""); array_push($c0_types," "); 
  //
  while (($row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) != false) {
    array_push($c0_types,$row[0]); 
  }
  //
  // echo $_POST['inMTFID'];
  //
  //$dropDown_pred = makeDropDown_pred("types_all", $c0_types,  $form_cern, $form_tableToChoose, array("class=\"hideoption\""),"onfocus='TYPE_selected()'");
  // 
  $selectFiled ="";
  $selectField = "<div id='headerfield' class='whitediv' style='width:1200px;'>
		                <div class='sub2div'>
		                   <div id='searchfields' class='in4div'>
		                      <div id='mftsearch'>

		                         <label><span class='desc'>Search for MTF - No. (20MN X XXXX # ####): </span></label><br>
<input name=\"inMTFID\" id=\"inMTFIDfield\" class=\"invisinput bigger\" type=\"text\" value=\"\" pattern=\"(20|99){1}(MN){1}(M|T|E){1}(RBRO|RBDR|RAKF|RINK|CLAY|GLFI|MAOR|COPO|HCS1|HCS2|HCS3|HCS4|HCS5|HCS6|HCS7|HCS8|HCL1|HCL2|HCL3|HCL4|HCL5|HCL6|HCL7|HCL8|BOS1|BOS2|BOS3|BOS4|BOS5|BOS6|BOS7|BOS8|BOL1|BOL2|BOL3|BOL4|BOL5|BOL6|BOL7|BOL8|CCS1|CCS2|CCL1|CCL2|FSE1|FSE2|FSE3|FSE4|FSE5|FSE6|FSE7|FSE8|FSS1|FSS2|FSS3|FSS4|FSS5|FSS6|FSS7|FSS8|FLE1|FLE2|FLE3|FLE4|FLE5|FLE6|FLE7|FLE8|FLS1|FLS2|FLS3|FLS4|FLS5|FLS6|FLS7|FLS8|BDS1|BDS2|BDS3|BDS4|BDS5|BDS6|BDS7|BDS8|BDL1|HOCS|HOCL|FDRF|FDRC|FDRA|FDIF|FDIC|FDIA|FRIF|FRID|FRIB|FMEG|FMEE|FMEC|FMEA|FGGG|FGGE|FGGC|FGGA|L280|L284|L480|L484|L680|L684|S280|S284|S480|S484|S680|S684|TDSA|CPS1|CPS2|CPL1|CPL2|FXS1|FX11|FXNC|SFEB|PFEB|L1CM|L1CT|L1CR|MFE8|ADDC|VMM2|IT71|IT72|IEED|IIR7|IIR8|IIRD|IIED|IID1|IID2|IRD1|IRD2|ISDR|S594|L211|L214|L217|L251|L282|L285|L311|L312|L313|L318|L319|L323|L327|L328|L391|L394|L411|L414|L417|L451|L485|L511|L512|L513|L518|L519|L523|L527|L528|L591|L594|L611|L614|L617|L651|L682|L685|L711|L712|L713|L718|L719|L720|L721|L723|L727|L728|L791|L794|P019|P020|P028|P078|P081|P082|P083|P086|P090|P095|P117|P122|P199|S211|S214|S217|S251|S285|S311|S312|S313|S318|S319|S323|S327|S328|S391|S394|S411|S414|S417|S451|S485|S511|S512|S513|S518|S519|S523|S527|S528|S591|BDL2|BDL3|BDL4|BDL5|BDL6|BDL7|BDL8|BSE1|BSE2|BSE3|BSE4|BSE5|BSE6|BSE7|BSE8|BSS1|BSS2|BSS3|BSS4|BSS5|BSS6|BSS7|BSS8|BLE1|BLE2|BLE3|BLE4|BLE5|BLE6|BLE7|BLE8|BLS1|BLS2|BLS3|BLS4|BLS5|BLS6|BLS7|BLS8|RS1E|RS1S|RS2E|RS2S|RL1E|RL1S|RL2E|RL2S|DS1O|DS1C|DS2O|DS2C|DL1O|DL1C|DL2O|DL2C|MES1|MES2|MEL1|MEL2|MMS1|MMS2|MML1|MML2|ORPH|TOPN|S611|S614|S617|S651|S685|S711|S712|S713|S718|S719|S723|S727|S728|S791|S794|FDRI|FDII|FRII|FMEI|FGGI|FDRM|FDIM|FRIM|FMEM|FGGM){1}[0-9]{5}\" onfocus=\"mtf_selected()\">

							       </div>
   						       <div id='eqidsearch'>
                               <label><span class='desc'>Search for unique DB EQ ID:</span></label><br>
										 <input id='inEQIDfield' class='invisinput medium' type='text' name='inEQID' value='".$inEQID."' onfocus='ID_selected()'>
									 </div>
									 <div id='aliassearch'>
									    <label><span class='desc'>Search for EQ ALIAS:</span></label><br />
										 <input id='inEQALIASfield' class='invisinput bigger' type=\"text\" name='inEQALIAS' value='".$inEQALIAS."' onfocus='ALIAS_selected()'>
								    </div>
									 <div id='typesearch'>
									   <label><span class='desc'>Search for EQUIP TYPE:</span></label><br />".
    makeDropDown_pred("types_all", $c0_types,  $form_cern, $form_tableToChoose, array("class=\"hideoption\""),"onfocus='TYPE_selected()'")."
	                         </div>
                         </div>
		                   <div class='clear'></div>
		                </div>
		             </div>\n";
  //
  $form_tableToChoose = getGlobalVar("tableToChoose");
  $form_cern          = getGlobalVar("cern"         );
  $form_other         = getGlobalVar("other"        );
  //$form_batches       = getGlobalVar("batches"     );
  //$form_structures    = getGlobalVar("structures"  );
  //$form_measurements  = getGlobalVar("measurements");
  //                                <input id='loadEQ' class='commentbutton smaller' name='linp' type='submit' value='Load EQ Entry'>
  //


  $sql  = "SELECT DISTINCT MEASMETHOD FROM ".$towner.".MEASMETHODS"; 
  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);

  $c0_method  = array();
  $c0_method1 = array();
  $c0_method2 = array();


  array_push($c0_method,"ALL");
  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    array_push($c0_method,$row[0]); 
  }




  $sql  = "SELECT DISTINCT SITENAME FROM ".$towner.".CONTEXTS"; 
  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);
  //
  $c0_site  = array();
  $c0_site1 = array();
  $c0_site2 = array();
  //
  $c0_cern  = array();
  $c0_other = array("");
  //
  $c1_cern = array("Select a ");
  $c1_other = array("");
  //
  array_push($c0_site,"ALL"); 
  //
  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    array_push($c1_other,"ALL"); 
    array_push($c0_other,$row[0]); 
    array_push($c0_site,$row[0]); 
  }
  //



  $table_site = getChosenTableP($form_tableToChoose, $c0_site1, $c0_site2); //, $c0_batch, $c0_struc, $c0_meas);
  //
  $table_method= getChosenTableP($form_tableToChoose, $c0_method1, $c0_method2); //, $c0_batch, $c0_struc, $c0_meas);

  $stid = oci_parse($conn, 'SELECT SITENAME, PRODSITEGROUP FROM ATLAS_MUON_NSW_ELEC_QAQC.PRODSITEGROUPCONT');
  oci_execute($stid);
  //
  $c2 = array();
  //
  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    switch($row[0]) {
    default : array_push($c1_other,$row[0]); array_push($c0_other,$row[1]); break;
    }

  }

  //COLLECTIONS
  $stid = oci_parse($conn, 'SELECT COLLECTIONNAME FROM ATLAS_MUON_NSW_ELEC_QAQC.COLLECTIONS');
  oci_execute($stid);
  $c0_collections =array();
  $c0_collections1 = array();
  $c0_collections2 = array();
  array_push($c0_collections,"ALL"); 

  $c0_collcomp =array("");
  $c0_collcomp1 = array(" ");

  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    array_push($c0_collcomp1,$row[0]); 
    array_push($c0_collcomp,"ALL"); 
    array_push($c0_collections,$row[0]); 

  }

  //COLLECTIONS
  $stid = oci_parse($conn, 'SELECT ASPECTNAME, COLLECTIONNAME FROM ATLAS_MUON_NSW_ELEC_QAQC.COLLCOMPONENTS');
  oci_execute($stid);


  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    array_push($c0_collcomp,$row[0]); 
    array_push($c0_collcomp1,$row[1]);
  }


  // must come BEFORE the selectField stuff
  $table = getChosenTableP($form_tableToChoose, $c0_cern, $c0_other); //, $c0_batch, $c0_struc, $c0_meas);

  $selectField  .= "<div class=\"whitediv\">\n";
  $selectField .=    "<table class=\"noborder\" style='width:100%;'>\n";
  $selectField .=       "<tr>\n";
  // $selectField .=          "<td><b> Measurement:</b></td>\n";
  $selectField .=          "<td><b><label class='labeltag'><span >Measurement:</span></label></b></td>\n";
 
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_pred("measurements"       , $c0_collections , $form_other       , $form_tableToChoose, array());
  $selectField .=          "</td>\n"; 

  // $selectField .=          "<td><b> Variable:</b></td>\n";
  $selectField .=          "<td><b><label class='labeltag'><span >Variable:</span></label></b></td>\n";

  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown2("collcomp_drop"       , $c0_collcomp , $c0_collcomp1, $form_other       , "var_coll", "coll_val_global", array(),"onchange=\"document.getElementById('myhidden1').value=$('#collcomp_drop option:selected').text()\"");
  $selectField .=          "</td>\n"; 
  $selectField .=          "<td>
	                             <input type='hidden' name='var_coll'  id='myhidden1' value='default'>
	                          </td>";

  $selectField .=    "</table>\n";        
  $selectField .= "</div>\n";


  $selectField  .=  "<div class=\"whitediv\">\n";
  $selectField .=    "<table class=\"noborder\" style='width:100%;'>\n";
  $selectField .=       "<tr>\n";
  // $selectField .=          "<td><b> Construction Sites:</b></td>\n";
  $selectField .=          "<td><b><label class='labeltag'><span >Construction Sites:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_pred("sites_nation", $c0_site,  $form_cern, $form_tableToChoose, array(),"","short");
  $selectField .=          "</td>\n";
  $selectField .=          "<td><b><label class='labeltag'><span >Measurment Method:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_pred("methods_nation", $c0_method,$form_cern, $form_tableToChoose, array(),"","short" );
  $selectField .=          "</td>\n";


  $selectField .=          "<td><b><label class='labeltag'><span >Channels:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=          " <input id='inChannel' class='invisinput bigger' type=\"text\" name='inChannel' value='".$inChannel."' >";
  $selectField .=          "</td>\n";





  // $selectField .=          "<td><b> Local Sites:</b></td>\n";
  /* $selectField .=          "<td><b><label><span class='labeltag'>Local Sites:</span></label></b></td>\n"; */


  /* $selectField .=          "<td>\n"; */
  /* $selectField .=              makeDropDown2("sites_national"       , $c1_other , $c0_other, $form_other       , "sites_local", "site_val_global",array(), "onchange=\";document.getElementById('myhidden').value=$('#sites_national option:selected').text()\""); */
  /* $selectField .=          "</td>\n";  */

  /* $selectField .=          "<td> */
  /*                              <input type='hidden' name='sites_local'  id='myhidden' value='default'> */
  /*                           </td>"; */

  $selectField .=       "</tr>\n";

  $selectField .=    "</table>\n";        
  $selectField .= "</div>\n";



  // 
  $selectField .= "<div class='whitediv' style=\"width:600px; margin:0 auto;\">
		<table class=\"noborder\">\n
		<tr>
		<td><input id='SEARCH' class='commentbutton elec' name='linp' type='submit' value='SEARCH'> </td>
		<td><input id='cancel_all_ID' class='commentbutton elec' name='cancel_all_name' type='button' value='CANCEL' onclick = 'cancel_all()'> </td>";

  $selectField .=          "<td> <input type='checkbox' name='get_only_last' id = 'get_only_last'";
  $selectField .= empty($_POST['get_only_last']) ? '' : ' checked="checked" ';
  $selectField .= "
		/> Get only latest measurement </td>\n";
  $selectField .=  "</tr> </table>";











  $selectField .= "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js'></script>";



  $selectField .= "</div>\n";

  $selectField .="<script> $(\"#sites_nation\").change(function() {
		if ($(this).data('options') == undefined) {
			$(this).data('options', $('#sites_national option').clone());
		}
	var id = $(this).val();
	if (id=='ALL'){
		$('#sites_national').prop('disabled', true);
		document.getElementById(\"sites_national\").value=\"0\";
		$('#sites_national').change();
	}
	else{
		$('#sites_national').prop('disabled', false);
	}
	$( 'select' ).change( displayVals );
	displayVals();
	function displayVals() {
		var singleValues = $( '#sites_nation' ).val();
	}
	var options = $(this).data('options').filter('[value=' + id + ']');
	$('#sites_national').change();           
	$('#sites_national').html(options);
	document.getElementById(\"sites_national\").onchange();
});";
  $selectField .="";

  $selectField .="</script> ";

  $selectField .="<script> $(\"#measurements\").change(function() {
if ($(this).data('options') == undefined) {
	$(this).data('options', $('#collcomp_drop option').clone());
}
var id = $(this).val();
if (id=='ALL'){
	$('#collcomp_drop').prop('disabled', true);
	document.getElementById(\"collcomp_drop\").value=\"0\";
	$('#collcomp_drop').change();
}
else{
	$('#collcomp_drop').prop('disabled', false);
}

var options = $(this).data('options').filter('[value=' + id + ']');
$('#sites_national').change();
$('#collcomp_drop').html(options);
document.getElementById(\"collcomp_drop\").onchange();


});";
  // document.getElementById(\"collcomp_drop\").onchange();

  $selectField .="</script> ";



  $selectField .= "<script>
$(document).ready(function() {
		if ($(\"#sites_nation\").data('options') == undefined) {
		$(\"#sites_nation\").data('options', $('#sites_national option').clone());
		}
		var id = $(\"#sites_nation\").val();
		if (id=='ALL'){
		$('#sites_national').prop('disabled', true);
		document.getElementById(\"sites_national\").value=\"0\";
		$('#sites_national').change();
		}
		else{
		$('#sites_national').prop('disabled', false);
		}

		$( 'select' ).change( displayVals );
		displayVals();
		function displayVals() {
		var singleValues = $( '#sites_nation' ).val();
		}
		var options = $(\"#sites_nation\").data('options').filter('[value=' + id + ']');
		$('#sites_national').change();
		$('#sites_national').html(options);
});";
  $selectField .= "</script> ";

  $selectField .= "<script>
$(document).ready(function() {
		if ($(\"#measurements\").data('options') == undefined) {
		$(\"#measurements\").data('options', $('#collcomp_drop option').clone());
		}
		var id = $(\"#measurements\").val();
		if (id=='ALL')
		{

		$('#collcomp_drop').prop('disabled', true);
		document.getElementById(\"collcomp_drop\").value=\"0\";
		$('#collcomp_drop').change();
		}
		else{
		$('#collcomp_drop').prop('disabled', false);
		}

		$( 'select' ).change( displayVals );
		displayVals();
		function displayVals() {
		var singleValues = $( '#measurements' ).val();
		}
		var options = $(\"#measurements\").data('options').filter('[value=' + id + ']');
		$('#collcomp_drop').change();
		$('#collcomp_drop').html(options);
});";
  $selectField .= "</script> ";

  $selectField .= "<script>
function mtf_selected() {
	document.getElementById(\"inEQIDfield\").value=\"\";
	document.getElementById(\"inEQALIASfield\").value=\"\";
	document.getElementById(\"types_all\").value=\"0\";

}";
  $selectField .= "</script> ";

  $selectField .= "<script>
function ALIAS_selected() {
	document.getElementById(\"inEQIDfield\").value=\"\";
	document.getElementById(\"inMTFIDfield\").value=\"\";
	document.getElementById(\"types_all\").value=\"0\";

}";
  $selectField .= "</script> ";

  $selectField .= "<script>
function ID_selected() {
	document.getElementById(\"inMTFIDfield\").value=\"\";
	document.getElementById(\"inEQALIASfield\").value=\"\";
	document.getElementById(\"types_all\").value=\"0\";

}";
  $selectField .= "</script> ";

  $selectField .= "<script>
function TYPE_selected() {
	document.getElementById(\"inEQIDfield\").value=\"\";
	document.getElementById(\"inEQALIASfield\").value=\"\";
	document.getElementById(\"inMTFIDfield\").value=\"\";

}";
  $selectField .= "</script> ";

  $selectField .= "<script>
function cancel_all() {
	document.getElementById(\"inEQIDfield\").value=\"\";
	document.getElementById(\"inEQALIASfield\").value=\"\";
	document.getElementById(\"inMTFIDfield\").value=\"\";
	document.getElementById(\"types_all\").value=\"0\";
	document.getElementById(\"sites_nation\").value=\"0\";
	document.getElementById(\"measurements\").value=\"0\";



	document.getElementById(\"sites_national\").value=\"0\";
	document.getElementById(\"collcomp_drop\").value=\"0\";

	$('#collcomp_drop').change();
	$('#sites_national').change();


}";






  $selectField .= "</script> ";

  if (isset($_POST['linp'])) {





    $MTF_code =  $_POST['inMTFID'];
    $where_loc = $_POST['sites_nation'];
    
    $ID_code =  $_POST['inEQID'];
    $ID_alias_code =  $_POST['inEQALIAS'];
    $constr_coll =  $_POST['sites_nation'];
    $collection_meas =  $_POST['measurements'];
    $ID_Channel = $_POST['inChannel'];
    $ID_Method = $_POST['methods_nation'];

    $locations = $_POST['sites_local'];
    //echo $locations."ff".$_SESSION['site_val_global'];
    if ($locations!= 'default'){
      //echo "changes";
      $_SESSION['site_val_global'] = $locations;
      //echo ($_SESSION['site_val_global']);
    }
    if ($location== 'default'){
      $locations = $_SESSION['site_val_global'];
    }


    $collcomp_var =  $_POST['var_coll'];
    if ($collcomp_var!= 'default'){
      $_SESSION['coll_val_global'] = $collcomp_var;
    }
    else{
      $collcomp_var = $_SESSION['coll_val_global'];
    }

    $types = $_POST['types_all'];
    $getlastonly = isset($_POST['get_only_last']) ? $_POST['get_only_last'] : 'off';


    
    $view = "ATLAS_MUON_NSW_ELEC_QAQC.FULLMEASVIEW";
    /* else */
    /*   //$view = "ATLAS_MUON_NSW_ELEC_QAQC.FULLMEASVIEWALL"; */
    /*   $view = "ATLAS_MUON_NSW_ELEC_QAQC.FULLMEASVIEWALL";		 */
    $tmptablePrint="<th>".$MTF_code." ".$ID_code." ".$ID_alias_code." ".$types."</th>";

    if ($MTF_code!="" AND $ID_code =="" AND $ID_alias_code=="" AND $types ==""){
      $string = ('SELECT * FROM '.$view.' WHERE PARTSBATCHMTFID = \''.$MTF_code.'\'');
    }
    else if ($MTF_code=="" AND $ID_code !="" AND $ID_alias_code=="" AND $types ==""){
      $string = ('SELECT * FROM '.$view.' WHERE EQENTRYID = \''.$ID_code.'\'');
    }
    else if ($MTF_code=="" AND $ID_code =="" AND $ID_alias_code!="" AND $types ==""  ){
      $string = ('SELECT * FROM '.$view.' WHERE OTHERID = \''.$ID_alias_code.'\'');
    }

    else if ($MTF_code=="" AND $ID_code =="" AND $ID_alias_code=="" AND $types !="" ){
      $string = ('SELECT * FROM '.$view.' WHERE EQTYPEGROUPCODE = \''.$types.'\'');
    }
    else {
      $string = ('SELECT * FROM '.$view.' WHERE EQENTRYID IS NOT NULL');
    }


    // $string = " ss";
    // echo $constr_coll;
    //   
    if ($where_loc!="" AND $where_loc!=" " AND $where_loc!="ALL" AND $where_loc!="default"  ){
      $string .=(' AND SITENAME = \''.$where_loc.'\'');
    }
    if ($ID_Method!="" AND $ID_Method!=" " AND $ID_Method!="ALL" AND $ID_Method!="default"  ){
      $string .=(' AND MEASMETHOD = \''.$ID_Method.'\'');
    }

    //
    if ($locations!="" AND $locations!=" " AND $locations!="ALL"){
      $string .= (' AND SITENAME = \''.$locations.'\'');
    }
    //
    if ($collection_meas!="" AND $collection_meas!=" " AND $collection_meas!="ALL" AND $collection_meas!="default") 
      $string .= (' AND COLLECTIONNAME = \''.$collection_meas.'\'');

    if ($collcomp_var!="" AND $collcomp_var!=" " AND $collcomp_var!="ALL")
      $string .= (' AND ASPECTNAME = \''.$collcomp_var.'\'');

    if($ID_Channel!="")
      $string.= ' AND CHANNEL = \''.$ID_Channel.'\'';


    // echo $string;
    // 
    $headerOutString = "";
    // 
    // $string .= (' AND SITENAME =\''.$locations.'\' ORDER BY COLLECTIONNAME,
    //   ASPECTNAME ASC');
    $stid = oci_parse($conn, $string );
    oci_execute($stid);
    $ncols = oci_num_fields($stid);
    
    $tmptablePrint=$string." ".$tmptablePrint;

    $transText = array();
    /* $sqlTrans  = "select DBTABLECOLUMN,DBCOLUMNFREETEXT from ATLAS_MUON_NSW_ELEC_QAQC.WEBTRANSLATIONS"; */
    /* 	$stidTrans = oci_parse($conn, $sqlTrans); */
    /* 	$execTrans = oci_execute($stidTrans); */

    /* 	while($row = oci_fetch_array($stidTrans, OCI_ASSOC+OCI_RETURN_NULLS)) { */
    /* 		array_push($transText,$row); */
    /* 	} */
		
    $sqlTrans  = "select DBTABLECOLUMN,DBCOLUMNFREETEXT from ATLAS_MUON_NSW_MM_LOG.WEBTRANSLATIONS";
    $stidTrans = oci_parse($conn, $sqlTrans);
    $execTrans = oci_execute($stidTrans);

    while($row = oci_fetch_array($stidTrans, OCI_ASSOC+OCI_RETURN_NULLS)) {
      array_push($transText,$row);
    }
	
    $tablePrint  = "<table class=\"tablesorter floatThead\" id=\"t01\">\n";    
    $tablePrint .=    "<thead><tr>\n";
    //
    //$tablePrint .= "ADS ".$tmptablePrint;
    
    $tablePrint .=     "<th class=\"vertical\"><div><b> Download </b></div></th>\n";

    for ($i = 1; $i <= $ncols; ++$i) {
      $colname = oci_field_name($stid, $i);
      $found = "";
       
      
      foreach($transText as $value) {
      	if($value['DBTABLECOLUMN'] == $colname) {
	  $found = $value['DBCOLUMNFREETEXT'];
	  break;
      	}
      }
      
      if ($found != "") {
	$print_me = $found;
      } else {
	$print_me = $colname;
      }        
      $tablePrint .=     "<th class=\"vertical\"><div><b>".htmlentities($print_me, ENT_QUOTES)."</b></div></th>\n";
    } //end for 
    $tablePrint .=    "</thead><tbody>\n";
    //


    
    /* $SearchRangeMin=getGlobalVar('SearchRangeMin'); */
    /* $SearchRangeMax=getGlobalVar('SearchRangeMax'); */
      $RowCount=0;
      $SearchRangeMin=isset($_POST['inMinRange']) ? $_POST['inMinRange'] : 1 ;
      $SearchRangeMax=isset($_POST['inMaxRange']) ? $_POST['inMaxRange'] : 100 ;

    
    while($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
      $RowCount+=1;
	if($RowCount < $SearchRangeMin)
	  {
	    continue;
	  }
	else if($RowCount > $SearchRangeMax)
	  {
	    break;
	  }

      $tablePrint .=    "<tr>\n";
      $precoll = '';

      if($row["MEASTYPENAME"]=="File")
	{


	  $tablePrint .= "<td><form id='DownForm' method='post' target='_blank' action='DownloadItem.php'><button class =\"submitbuttons elec\" type=\"submit\" name=\"SubID\" value=\"".$row["MEASSITEHASH"]."\" >Download</button>\n</form></td>";
	}
      else
	{
	  $tablePrint .= "<td></td>";
	}

      


      foreach ($row as $item){



	if ($precoll==$row[15]){
	  $tablePrint .=    "<td>" . ($item != null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
	}
	else { 
	  $tablePrint .= "<td></td>\n";
	  $tablePrint .=    "<td>" . ($item != null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";

	}
	$pre_coll = $row[15];
      }
      $tablePrint .=    "</tr>\n";        
    } // end while    
    $tablePrint .= "</tbody></table>\n";      
  




    
    

    $buttonContent .= "<div id=\"button\">\n";    
   
    $buttonContent .="<div class=\"whitebox\"> <label> Showing <input id=\"inMinRangeField\"  name=\"inMinRange\" patter=\"^\d+$\" type=\"text\" value=\"".$SearchRangeMin."\" size=5> - <input id=\"inMaxRangeField\"  name=\"inMaxRange\" patter=\"^\d+$\" type=\"text\" value=\"".$SearchRangeMax."\" size=5>  <input id='UPDATE' name='linp' class='commentbutton elec' type='submit' value='Update' > </div>";




    $buttonContent .= "<button type=\"button\" class=\"downloadbuttons elec\" onClick =\"$('.floatThead').tableExport({type:'csv',escape:'false'});\">Download table as <b>CSV</b> file</button>\n";
    $buttonContent .= "<button type=\"button\" class=\"downloadbuttons elec\" onClick =\"$('.floatThead').tableExport({type:'excel',escape:'false'});\">Download table as <b>EXCEL</b> file</button>\n";
    $buttonContent .= "</div>\n";  
  } // end if
  // put NOT everything together into $mainContent
  //$selectField .=              makeDropDown2("sites_national"       , $c1_other , $c0_other, $form_other       , $form_tableToChoose, array(), "onchange=\"document.getElementById('myhidden').value='ciao'\"");
  $naviContent     = $selectField;
  $downloadButtons = $buttonContent;
  $mainContent     .= $tablePrint;
 
}
//////////////////////////////////////////////////////////////////////////////
// make the DIY Query
//////////////////////////////////////////////////////////////////////////////
else if($page == "cyoq") {
  $mainContent  = "<div class=\"whitediv\">\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "For everybody, who really is an expert in reading database schemes,
                          here is the possibility to ask the database whatever you want (provided,
                          it is a valid select query ;-) ).";
  $mainContent .=       "<br><br>\n";
  $mainContent .=       "As an example, the query for the view &quot;All readout boards,
                          coming from a certain manufacturer (i.e. ELVIA), that did not
                          pass the QA/QC.&quot; would be...";
  $mainContent .=       "<br><br>\n";
  $mainContent .=       "select ROBOARDMTFID, ROBOARDRUIID, ROBOARDTYPE, PRODUCINGCOMPANY, ACC_DECISION, CERN_QC, REPAIR
                          FROM ATLAS_MUON_NSW_ELEC_QAQC.ROBOARDS left outer join ATLAS_MUON_NSW_ELEC_QAQC.ROBOARDTYPES                           
                          sectable on ROBOARDS.ROBOARDTYPEID = sectable.ID_ROBOARDTYPES where ACC_DECISION = &#39;F&#39;
                          and PRODUCINGCOMPANY = &#39;ELVIA&#39;";
  $mainContent .=       "<br><br>\n";
  $mainContent .=       "Feel free to try and copy and paste it!";
  $mainContent .=       "<br><br>\n";
  $mainContent .=    "</p>\n";   
  $mainContent .=    "<form id=\"cyoq\" name=\"form\" action=\"\" method=\"post\">\n";
  $mainContent .=       "<textarea class=\"FormElement\" style=\"background-color:#ffdcee;\" cols=\"80\" rows=\"5\" id=\"textbox\" name=\"textbox\" ></textarea>\n";
  $mainContent .=       "<input class =\"submitbuttons elec\" type=\"submit\" value=\"Submit\">\n";
  $mainContent .=    "</form>\n";
  // 
  $content = "";
  $content = $_POST['textbox'];
  $content = stripslashes($content); 
  if ($content != "") {
    $mainContent .= "<p style=\"color= #880064;\"><b>";   	
    $mainContent .=     $content;
    $mainContent .= "</b></p>\n";    	
  }
  $mainContent .= "</div>\n";
  //echo $_POST['textbox'];
  //$table = "RESISTIVEFOILS";
  //if ($content == "") {
  //   $query = "SELECT * FROM ATLAS_MUON_NSW_ELEC_QAQC.".$table;
  //} else {
  $query = $content;
  //}   
  //
  $stid = oci_parse($conn, $query );
  oci_execute($stid);   
  $tablePrint .= "<table class=\"tablesorter floatThead\" id='t01'>\n";
  $ncols = oci_num_fields($stid);
  $tablePrint .=    "<thead><tr>\n";
  for ($i = 1; $i <= $ncols; ++$i) {
    $colname = oci_field_name($stid, $i);
    $found = "";
    $query = "SELECT comments FROM user_col_comments where column_name = \'" .$colname. "\'";
    $comment = oci_parse($conn, $query);
    oci_execute($comment);
    //
    while($commenttext = oci_fetch_array($comment, OCI_ASSOC+OCI_RETURN_NULLS)){
      if($commenttext["COMMENTS"] != ""){
	$found = $commenttext["COMMENTS"];
	break;
      }
    }
    if ($found != "") {
      $print_me = $found;
    } else {
      $print_me = $colname;
    }
    $tablePrint .= "<th class=\"vertical\"><div><b>".htmlentities($print_me, ENT_QUOTES)."</b></div></th>\n"; // was: $colname ... class=\"vertical\"
  } //end for      
  $tablePrint .= "</thead><tbody>\n";
  //
  while($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
    $tablePrint .= "<tr>\n";
    foreach ($row as $item){
      $tablePrint .= "<td>" . ($item != null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
    $tablePrint .= "</tr>\n";
  } // end while     
  $tablePrint .= "</tbody></table>\n";    
  $mainContent .= $tablePrint;
}
//////////////////////////////////////////////////////////////////////////////
// make the upload page
//////////////////////////////////////////////////////////////////////////////
else if($page == "upload") {
  // upload
  if(isset($_FILES["files"]) && !empty($_FILES["files"]["tmp_name"])) {
    include("multupload.php");
  } else {
    // show the form with buttons
    $formContent  = "<div class=\"whitediv\">\n";
    $formContent .=    "<p>\n";
    $formContent .=       "You can upload your measurement files here.
	                       They will be imported into the database on a regular basis.";
    $formContent .=       "<br>";
    $formContent .=       "Please allow at least 2 workdays for me to complete the 
	                       import of freshly uploaded files!";
    $formContent .=    "</p>\n";
    $formContent .=    "<p><b>How should the files look like?</b></p>\n";
    $formContent .=    "<ul id=\"intextUl\">\n";
    $formContent .=       "<li>The <b>construction site </b>should be indicated in the <b>file name</b>.</li>\n";
    $formContent .=       "<li>The <b>file name</b> must also contain the name of the <b>corresponding database table</b>.</li>\n";
    $formContent .= 		 "<li>The <b>file extension</b> must be <b>.csv</b> for data files that should be imported.</li>\n";
    $formContent .=       "<li>If you want to <b>upload pictures or documents</b>: .jpg and .png are accepted by the system, also .pdf.
		                        It is necessary to <b>indicate the table and the database field (item-# and column name)</b>,
		                        i.e. \"ROBOARDS_20MNMBSS300002_supplierReport.jpg\".
		                    </li>\n";
    $formContent .=    "</ul>\n";
    $formContent .=    "<p>\n";
    $formContent .=       "<input type=\"file\" name=\"files[]\" multiple> \n";
    $formContent .=       "<input class=\"submitbuttons elec\" type=\"submit\" value=\"upload\">\n";
    $formContent .=    "</p>\n";
    $formContent .= "</div>\n";
  }
}
//////////////////////////////////////////////////////////////////////////////
// make the Downloads page
//////////////////////////////////////////////////////////////////////////////
else if($page == "downloads") {
  $mainContent  = "<div class=\"whitediv\">\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "There is only one template at the moment, which you can download here.
                          <br>\n
	                       It helps you to put together the data that is needed to complete the
	                       measurement tables of the CERN CENTRAL database.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p><a href=\"downloads/excel_template.xlsx\" download> Download Excel Template for measurement data format</a></p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "If you like to send me your measurement format data in this way, please download and 
	                       fill out the Excel file.
	                       <br>\n
	                       Then send it to me via Email: kim.temming@physik.uni-freiburg.de";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "In addition, you can download the headers for the csv import files for each table
                          here on the page. Go to \"Tables\" and select the desired table. Then you can download
                          a csv file (\"Download import template as CSV\"), which contains already the headers that
                          are needed for this specific table. Just keep the header line in the file and add your data
                          as line two, separated by &qout;&quot;exactly like in the header.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "You don't need to fill in all fields, but you always need to fill in the first column, since
                          this is the ID of the item which is described in the line. You may delete columns in the header
                          which you don't need. You also may change the order of the columns, except the first column:
                          This MUST STAY in the first position.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "This file is then ready for import and can be sent to me,
                          after changing the file name to something meaningful. (See \"Upload\" tab on this page.)";
  $mainContent .=    "</p>\n";
  $mainContent .= "</div>\n";
}
//////////////////////////////////////////////////////////////////////////////
// make the help page
//////////////////////////////////////////////////////////////////////////////
else if($page == "help") {
  $mainContent  = "<div class=\"whitediv\">\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "<b>Q: I do not find the information I need. It is scattered over many tables! :( </b>\n";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "You can ask for special, predefined views of the tables.";
  $mainContent .=       "To get this view here on the web-view page, you can send me an email (ismet.siral@cern.ch),
                          asking for what you need. You should be quite precise in the description of your desired view";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "If you need the data for plotting, it might be wise to specify this.
                          All unwanted columns should be removed from the view then, to simplify the plotting.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<br>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "<b>Q: I need information on the import files. How does the structure look and can I produce them myself?</b>";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "Yes, you can. You can download the headers for the csv import files for each table here on the page.
                          Go to \"Tables\" and select the desired table. Then you can download a csv file (\"Download import template as CSV\"),
                          which contains already the headers that are needed for this specific table.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "Just keep the header line in the file and add your data as line two, separated by \";\", exactly like in the header. 
                          You don't need to fill in all fields, but you always need to fill in the first column, since this is the ID of the 
                          item which is described in the line. You may delete columns in the header which you don't need. You also may change 
                          the order of the columns, except the first column: This MUST STAY in the first position.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "This file is then ready for import and can be sent to me, after changing the file name to something meaningful.
                          (See \"Upload\" tab on this page.)";
  $mainContent .=    "</p>\n";  
  $mainContent .=    "<br>\n";
  $mainContent .=    "<p><b>Q: I want to do my own select-queries. Will this be implemented?</b></p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "If you really (really!) want to, we can give you this opportunity in the next few weeks. But you have to know, 
                          that you need to understand the very complex table structure.
                          <br>                          
                          It would be much easier for you to just ask for a custom &quot;Predefined View&quot; and I will provide that to you.
                          <br>
                          The queries will also not be saved somewhere if you like it and need it again later, but you can 
                          of course just send it to me via Email, and it will be added to the predefined views in very short time.";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<br>\n";
  $mainContent .= "</div>\n";
}
//////////////////////////////////////////////////////////////////////////////
// make the contact page
//////////////////////////////////////////////////////////////////////////////
else if($page == "contact") {
  $mainContent  = "<div class=\"whitediv\">\n";
  $mainContent .=    "<p>This WebView of the <b>CERN Central New Small Wheel Electronics Database</b> has been developed by</p>\n";
  $mainContent .=    "<br>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "Ismet Siral, Dr. Kim Temming and Ulrich Holland<br><br>
University Of Michigan <br>
Pysikalisches Institut University Of Freiburg ";

  $mainContent .=    "</p>\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "If you have any questions or if you need some special view of the 
	                       database which is not yet available, please contact Ismet at ...";
  $mainContent .=    "</p>\n";
  $mainContent .=    "<p><b><span style=\"color: #880064;\">ismet.siral@cern.ch</span></b></p>\n";
  $mainContent .=    "<p>... or by skype ... </p>\n";
  $mainContent .=    "<p><b><span style=\"color: #880064;\">i.siral</span></b></p>\n";
  $mainContent .= "</div>\n";
} 
else if($page == "ameas" AND $canEditElec) {


  $selectField="";
  $towner = "ATLAS_MUON_NSW_ELEC_QAQC";

  $ReturnMessage="";
  if($_SESSION['ReturnMessage']!="")
    {
    $ReturnMessage=$_SESSION['ReturnMessage'];
    $selectField.="<div class=\"whitediv\">\n".$ReturnMessage."</div>\n"; 
    $_SESSION['ReturnMessage']="";
    }
  $Veqtype = $_SESSION['eqtype'];
  $Veqsite = $_SESSION["eqsite"];
  $Veqmethod = $_SESSION["eqmethod"];
  $Vcoll = $_SESSION["coll"];
  $VmeasDrop = $_SESSION["measDrop"];
  $VShifter= $_SESSION["Shifter"];
  /* $VinMTFID = $_SESSION("inMTFID"); */
  /* $VinEQID = $_SESSION("inEQID"); */
  /* $VinALIAS = $_SESSION("inEQID"); */
  /* $Vmeas = $_SESSION("meas"); */
  /* $Vmeas2 = $_SESSION("meas2"); */
  

  $sql  = "SELECT DISTINCT EQTYPEGROUPCODE FROM ".$towner.".ASPECTS";
  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);

  $c0_types = array(""); 

  while (($row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) != false) {
    array_push($c0_types,$row[0]); 
  }



  $sql  = "SELECT DISTINCT SITENAME FROM ".$towner.".CONTEXTS"; 
  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);
  //
  $c0_site  = array("");

  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    array_push($c0_site,$row[0]); 
  }

  $sql  = "SELECT DISTINCT MEASMETHOD FROM ".$towner.".MEASMETHODS"; 
  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);
  //
  $c0_method  = array("");

  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    array_push($c0_method,$row[0]); 
  }

  $sql  = "SELECT CL.COLLECTIONNAME,ASP.EQTYPEGROUPCODE,CTX.CONTEXTNAME,CTX.SITENAME,ASP.MEASTYPENAME,ASP.NUMBEROFCHANNELS,CTX.MEASMETHOD FROM ".$towner.".CONTEXTS CTX LEFT JOIN ".$towner.".ASPECTS ASP ON CTX.ASPECTNAME=ASP.ASPECTNAME LEFT JOIN ".$towner.".COLLCOMPONENTS CL ON CL.ASPECTNAME=ASP.ASPECTNAME";

  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);
  //

  $c0_type2coll = array();
  $c0_coll2meas = array();
  $c0_site2meas = array();
  $c0_method2meas = array();
  $c0_collNsite2meas = array();
  $c0_collNmethod2meas = array();
  $c0_collNmethodsite2meas = array();
  $c0_Meas2MType = array();
  $c0_Meas2NChan = array();
  $c0_MType2patNdesc = array();

  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
    
 
    //Make the list of Collections according to the Type
    if( array_key_exists( $row[1],$c0_type2coll) )
      {
	if( (! in_array($row[0],$c0_type2coll[$row[1]])))
	  array_push($c0_type2coll[$row[1]],$row[0]);
      }
    else
      {
    	$c0_type2coll[$row[1]]=array($row[0]);
      }

    //Link measurments to their measurment variable
    if( ! array_key_exists( $row[2],$c0_Meas2MType) )
      {
    	$c0_Meas2MType[$row[2]]=array($row[4]);
      }
    if( ! array_key_exists( $row[2],$c0_Meas2NChan) )
      {
    	$c0_Meas2NChan[$row[2]]=array($row[5]);
      }

    //Make the list of to measurments and collections (aspects and context) accroding to chip and site.
    if( array_key_exists( $row[0],$c0_coll2meas) )
      {
	if( (! in_array($row[2],$c0_coll2meas[$row[0]])))
	  array_push($c0_coll2meas[$row[0]],$row[2]);

	if( array_key_exists( $row[3],$c0_collNsite2meas[$row[0]]) )
	  array_push($c0_collNsite2meas[$row[0]][$row[3]],$row[2]);
	else
	  $c0_collNsite2meas[$row[0]][$row[3]]=array($row[2]);

	if( array_key_exists( $row[6],$c0_collNmethod2meas[$row[0]]) )
	  array_push($c0_collNmethod2meas[$row[0]][$row[6]],$row[2]);
	else
	  {
	    $c0_collNmethod2meas[$row[0]][$row[6]]=array($row[2]);
	  }
	if( array_key_exists( $row[6],$c0_collNmethodsite2meas[$row[0]]) )
	  {
	    if (array_key_exists( $row[3],  $c0_collNmethodsite2meas[$row[0]][$row[6]] ))
	      {
		array_push($c0_collNmethodsite2meas[$row[0]][$row[6]][$row[3]],$row[2]);
	      }
	    else
	      {
		$c0_collNmethodsite2meas[$row[0]][$row[6]][$row[3]]=array($row[2]);
	      }
	  }	      
	else
	  {
	    $c0_collNmethodsite2meas[$row[0]][$row[6]]=array($row[3]=>array($row[2]));
	  }
      }
    else
      {
	
    	$c0_coll2meas[$row[0]]=array($row[2]);
	$c0_collNsite2meas[$row[0]]=array($row[3]=>array($row[2]));
	$c0_collNmethod2meas[$row[0]]=array($row[6]=>array($row[2]));
	$c0_collNmethodsite2meas[$row[0]]=array($row[6]=>array($row[3]=>array($row[2])));
      }
    
  }



  $sql  = "SELECT MEASTYPENAME,MEASTYPEDESC,INPUTPATTERN FROM ".$towner.".MEASTYPES";

  $stid = oci_parse($conn,$sql);
  $exec = oci_execute($stid);
  
  while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {

    $c0_MType2patNdesc[$row[0]]=array($row[2],$row[1]);

  }


  //
  $selectField .= "<div class=\"whitediv\">\n";
  $selectField .=       "<tr>\n";
  // $selectField .=          "<td><b> Construction Sites:</b></td>\n";
  
  $selectField .=          "<td><b><label class='labeltag'><span >Equipment Type:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_add("eqtype", $c0_types,  $Veqtype, array());
  $selectField .=          "</td>\n";
  $selectField .=          "<td><b><label class='labeltag' 'short2'><span >Site:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_add("eqsite", $c0_site,  $Veqsite,  array(), "", "short2");
  $selectField .=          "</td>\n";
  $selectField .=          "<td><b><label class='labeltag' 'short2'><span >Meas Method:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_add("eqmethod", $c0_method,  $Veqmethod,  array(), "","short2");
  $selectField .=          "</td>\n";


  $selectField .=       "</tr>\n";

  $selectField .=       "<tr>\n";

  $c0_coll=array("");
  $c0_meas=array("");

  if($Veqtype!="")
    {

      $c0_coll=$c0_type2coll[$Veqtype];
      array_unshift($c0_coll,"");

    }


  if($Vcoll!="" AND $Veqsite!="")
    {
      $c0_meas=$c0_collNsite2meas[$Vcoll][$Veqsite];
      array_unshift($c0_meas,"");
    }

  if($Vcoll!="" AND $Veqmethod!="")
    {
      $c0_meas=$c0_collNmethod2meas[$Vcoll][$Veqmethod];
      array_unshift($c0_meas,"");
    }


  if($Vcoll!="" AND $Veqmethod!="" AND $Veqsite!="")
    {
      $c0_meas=$c0_collNmethodsite2meas[$Vcoll][$Veqmethod][$Veqsite];
      array_unshift($c0_meas,"");
    }



  $selectField .=          "<td><b><label class='labeltag'><span >Measurment Type:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_add("coll", $c0_coll,  $Vcoll, array());
  $selectField .=          "</td>\n";
  
  $selectField .=       "</tr>\n";

  $selectField .=          "<td><b><label class='labeltag'><span >Measurments:</span></label></b></td>\n";
  $selectField .=          "<td>\n";
  $selectField .=              makeDropDown_add("measDrop", $c0_meas,  $VmeasDrop,  array());
  $selectField .=          "</td>\n";








  $selectField .= "</div>\n";

  


  $selectField .= "<div id='ChipInfo' class='whitediv' style='width:1200px;'>\n";
  /* $selectField .= "<div id=field1 class=in3div>"; */
  /* $selectField .= "<label>MTF - No. (20MN X XXXX # ####):<br></label>"; */
  /*   $selectField .= "<input name=\"inMTFID\" id=\"inMTFIDfield\" class=\"invisinput medium\" type=\"text\" value=\"\" pattern=\"(20|99){1}(MN){1}(M|T|E){1}(RBRO|RBDR|RAKF|RINK|CLAY|GLFI|MAOR|COPO|HCS1|HCS2|HCS3|HCS4|HCS5|HCS6|HCS7|HCS8|HCL1|HCL2|HCL3|HCL4|HCL5|HCL6|HCL7|HCL8|BOS1|BOS2|BOS3|BOS4|BOS5|BOS6|BOS7|BOS8|BOL1|BOL2|BOL3|BOL4|BOL5|BOL6|BOL7|BOL8|CCS1|CCS2|CCL1|CCL2|FSE1|FSE2|FSE3|FSE4|FSE5|FSE6|FSE7|FSE8|FSS1|FSS2|FSS3|FSS4|FSS5|FSS6|FSS7|FSS8|FLE1|FLE2|FLE3|FLE4|FLE5|FLE6|FLE7|FLE8|FLS1|FLS2|FLS3|FLS4|FLS5|FLS6|FLS7|FLS8|BDS1|BDS2|BDS3|BDS4|BDS5|BDS6|BDS7|BDS8|BDL1|HOCS|HOCL|FDRF|FDRC|FDRA|FDIF|FDIC|FDIA|FRIF|FRID|FRIB|FMEG|FMEE|FMEC|FMEA|FGGG|FGGE|FGGC|FGGA|L280|L284|L480|L484|L680|L684|S280|S284|S480|S484|S680|S684|TDSA|CPS1|CPS2|CPL1|CPL2|FXS1|FX11|FXNC|SFEB|PFEB|L1CM|L1CT|L1CR|MFE8|ADDC|VMM2|IT71|IT72|IEED|IIR7|IIR8|IIRD|IIED|IID1|IID2|IRD1|IRD2|ISDR|S594|L211|L214|L217|L251|L282|L285|L311|L312|L313|L318|L319|L323|L327|L328|L391|L394|L411|L414|L417|L451|L485|L511|L512|L513|L518|L519|L523|L527|L528|L591|L594|L611|L614|L617|L651|L682|L685|L711|L712|L713|L718|L719|L720|L721|L723|L727|L728|L791|L794|P019|P020|P028|P078|P081|P082|P083|P086|P090|P095|P117|P122|P199|S211|S214|S217|S251|S285|S311|S312|S313|S318|S319|S323|S327|S328|S391|S394|S411|S414|S417|S451|S485|S511|S512|S513|S518|S519|S523|S527|S528|S591|BDL2|BDL3|BDL4|BDL5|BDL6|BDL7|BDL8|BSE1|BSE2|BSE3|BSE4|BSE5|BSE6|BSE7|BSE8|BSS1|BSS2|BSS3|BSS4|BSS5|BSS6|BSS7|BSS8|BLE1|BLE2|BLE3|BLE4|BLE5|BLE6|BLE7|BLE8|BLS1|BLS2|BLS3|BLS4|BLS5|BLS6|BLS7|BLS8|RS1E|RS1S|RS2E|RS2S|RL1E|RL1S|RL2E|RL2S|DS1O|DS1C|DS2O|DS2C|DL1O|DL1C|DL2O|DL2C|MES1|MES2|MEL1|MEL2|MMS1|MMS2|MML1|MML2|ORPH|TOPN|S611|S614|S617|S651|S685|S711|S712|S713|S718|S719|S723|S727|S728|S791|S794|FDRI|FDII|FRII|FMEI|FGGI|FDRM|FDIM|FRIM|FMEM|FGGM){1}[0-9]{5}\"></div>"; */
  $selectField .= "<div id=field2 class=in3div>\n";
  $selectField .= "<label>Unique DB EQ ID:<br></label>";
  $selectField .= "<input id=\"inEQIDfield\" class=\"invisinput medium\" type=\"text\" name=\"inEQID\" value=\"\">";
  $selectField .= "</div><div id=field3 class=in3div>\n";
  $selectField .= "<label>EQ ALIAS:<br></label>";
    $selectField .= "<input id=\"inALIASfield\" class=\"invisinput medium\" type=\"text\" name=\"inALIAS\" value=\"\">";
  $selectField .= "</div>\n";

  $selectField .= "<div id=divChannel class=in3div>\n";
  $selectField .= "<label id=lblChannel >Channels: <br></label>";
  $selectField .= "<input id=\"inChannel\" class=\"invisinput medium\" type=\"number\" name=\"inChannel\" value=\"\"></div>";
  
  $selectField .= "<div id=measdv class=in3div>";
  $selectField .= "<label id=\"MeasLabel\" >Measurement:<br></label>";
  $selectField .= "<input name=\"meas\" id=\"inMeas\" class=\"invisinput medium\" value=\"\">";
  $selectField .= "</div>\n";

  $selectField .= "<div id=updv class=in3div>";
  $selectField .= "<label id=\"FUploadLabel\" >Measurement File:<br></label>";
  $selectField .= "<input type=\"file\" name=\"inUpload\" id=\"inUpload\" >";
  $selectField .= "</div>\n";




  $selectField .= "<div id=measdv2 class=in3div>";
  $selectField .= "<label>Is Valid? (T/F):<br></label>";
  $selectField .= "<input name=\"meas2\" id=\"inMeas2\" class=\"invisinput medium\" value=\"\" pattern=\"(t|f|T|F){1}\">";
  $selectField .= "</div>\n";



  $selectField .= "<div id=measdv2 class=in3div>";
  $selectField .= "<label>Shifter: <br></label>";
  $selectField .= "<input name=\"Shifter\" id=\"inShifter\" class=\"invisinput short\" value=\"".$VShifter."\">";

  $selectField .= "</div>\n";




  $selectField .= "<div id=measdv2 class=in3div>";
  $selectField .= "<label>Meas. Site Hash: (Not Mandatory)<br> </label>";
  $selectField .= "<input name=\"MSiteHash\" id=\"inMHash\" class=\"invisinput short\" value=\"\">";
  $selectField .= "</div>\n";

  $selectField .= "<div id=measdv2 class=in3div>";
  $selectField .= "<label>Index Hash: (Not Mandatory)<br> </label>";
  $selectField .= "<input name=\"IHash\" id=\"inIHash\" class=\"invisinput short\" value=\"\">";
  $selectField .= "</div>\n";



  $selectField .= "<div id=measdv2 class=in3div>";
;
  $selectField .= "<input id=\"ADD\" class=\"commentbutton elec\" name=\"ADD\" type=\"submit\" value=\"Add Value\" onclick='this.form.action=\"AddToSQL.php\"'>";
  $selectField .= "<br>";
$selectField .= "<input id=\"Clear\" class=\"commentbutton elec\" name=\"Clear\" type=\"button\" value=\"Clear\" onclick=\"clearField()\">";
  $selectField .= "</div>\n";


  $selectField .= "</div>\n";




  $selectField .="<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>";


  $selectField .= "<script>
    $(function() {


$(\"#measDrop\").change(function() {

if ($(this).data('options') == undefined) {
$(this).data('options', $('#measDrop option').clone());
}

var key = $(this).val(); 

var vals =\"\";
var pattern =\"\";
var desc =\"\";

switch(key) {
";

    foreach ( $c0_Meas2MType as $key => $arr )

    {
      if (sizeof($arr)==0) continue;
      $selectField .= "case '".$key."':\n vals = \"".implode ("\",\"", $arr)."\"; break; \n";
    }
    $selectField .=" default: \n vals=\"\"; break; \n
}

switch(vals) {
";

    foreach ( $c0_MType2patNdesc as $key => $arr )

    {
      /* if (sizeof($arr)==0) continue; */
      $selectField .= "case '".$key."':\n pattern = \"".$arr[0]."\"; desc = \"".$arr[1]."\"; break; \n";
    }
    $selectField .=" default: \n pattern=\"\"; desc=\"\"; break; \n
}



document.getElementById('MeasLabel').innerHTML = 'Measurement: '+desc+' <br>';
document.getElementById('inMeas').pattern = pattern;

NChan=\"\";
switch(key) {
";

    foreach ( $c0_Meas2NChan as $key => $arr )

    {
      if (sizeof($arr)==0) continue;
      $selectField .= "case '".$key."':\n NChan = \"".implode ("\",\"", $arr)."\"; break; \n";
    }
    $selectField .=" default: \n NChan=\"\"; break; \n
}

if(NChan==\"\"){
document.getElementById('lblChannel').style.display = 'none';
document.getElementById('inChannel').style.display = 'none';
document.getElementById('divChannel').style.display = 'none'; 

}
else{
document.getElementById('divChannel').style.display = 'block'; 
document.getElementById('lblChannel').innerHTML = 'Channel: (0-'+NChan+')';
document.getElementById('lblChannel').style.display = 'block';

document.getElementById('inChannel').min = 0;
document.getElementById('inChannel').max = NChan;
document.getElementById('inChannel').pattern = '[0-9]*';

document.getElementById('inChannel').style.display = 'block';
document.getElementById('inChannel').style.marginLeft='auto';
document.getElementById('inChannel').style.marginRight='auto';
}



if(vals==\"File\"){
document.getElementById('updv').style.display = 'block';
document.getElementById('measdv').style.display = 'none';
}
else{
document.getElementById('updv').style.display = 'none';
document.getElementById('measdv').style.display = 'block';
}



}).change();
});
</script>";




  $selectField .= "<script>
$(\"#eqtype\").change(function() {

if ($(this).data('options') == undefined) {
$(this).data('options', $('#eqtype option').clone());
}

var key = $(this).val(); 

var vals = [];

switch(key) {
";

    foreach ( $c0_type2coll as $key => $arr )

    {
      if (sizeof($arr)==0) continue;
      $selectField .= "case '".$key."':\n vals = [\"".implode ("\",\"", $arr)."\"]; break; \n";
    }
$selectField .="
}


var secondChoice = $(\"#coll\");
    secondChoice.empty();
var thirdChoice = $(\"#measDrop\");
    thirdChoice.empty();

  secondChoice.append(\"<option>\" + \"\"+ \"</option>\");
  thirdChoice.append(\"<option>\" + \"\"+ \"</option>\");
  for ( value of vals) {
      secondChoice.append(\"<option>\" + value + \"</option>\");

    };


$('#measDrop').change();

});

</script>";


  $selectField .= "<script>
$(\"#coll\").change(function() {

if ($(this).data('options') == undefined) {
$(this).data('options', $('#coll option').clone());
}

var key = $(this).val(); 
var key2 = $(\"#eqsite\").val(); 

var vals = [];

switch(key) {
";


  foreach ( $c0_collNsite2meas as $key => $arr )
    {
      if (sizeof($arr)==0) continue;
      $selectField .= "case '".$key."':\n";

      $selectField .= "\t switch(key2) {\n";
	
      foreach ( $arr as $key2 => $arr2)
  	{
  	  if (sizeof($arr2)==0) continue;
  	  $selectField .= "\t case '".$key2."':\n\t\t  vals = [\"".implode ("\",\"", $arr2)."\"]; break; \n";
  	}
      $selectField .= "   \n\t\t} break; \n";
    }
  $selectField .="
}


var secondChoice = $(\"#measDrop\");
    secondChoice.empty();
      secondChoice.append(\"<option>\" +\"\"+ \"</option>\");
  for ( value of vals) {
      secondChoice.append(\"<option>\" + value + \"</option>\");

    };

$('#measDrop').trigger(\"change\");
});

</script>";



$selectField .= "<script>
function DoubleSelect() { 

var key = $(\"#coll\").val(); 
var key2 = $(\"#eqmethod\").val(); 
var key3 = $(\"#eqsite\").val(); 

var vals = [];

switch(key) {
";

  foreach ( $c0_collNmethodsite2meas as $key => $arr )
    {
      if (sizeof($arr)==0) continue;
      $selectField .= "case '".$key."':\n";

      $selectField .= "\t switch(key2) {\n";

      foreach ( $arr as $key2 => $arr2)
  	{
	  if (sizeof($arr2)==0) continue;

	  if ($key2!="")
	    {
	      $selectField .= "case '".$key2."':\n";
	      $selectField .= "\t switch(key3) {\n";
	      foreach ($arr2 as $key3 => $arr3)
		{
		  if (sizeof($arr3)==0) continue;

		  $selectField .= "\t case '".$key3."':\n\t\t  vals = [\"".implode ("\",\"", $arr3)."\"]; break; \n";
		}
	      $selectField .= "   \n\t\t} break; \n";
	    }
	}
      $selectField .= "   \n\t\t} break; \n";
      $selectField .= "case '':\n";
      $selectField .= "\t switch(key3) {\n";
      foreach ( $c0_collNsite2meas[$key] as $key3 => $arr3)
	{
	  if (sizeof($arr3)==0) continue;
	  $selectField .= "\t case '".$key3."':\n\t\t  vals = [\"".implode ("\",\"", $arr3)."\"]; break; \n";

	}
      $selectField .= "   \n\t\t} break; \n";
    }
  $selectField .="
}

var secondChoice = $(\"#measDrop\");
    secondChoice.empty();
      secondChoice.append(\"<option>\" +\"\"+ \"</option>\");
  for ( value of vals) {
      secondChoice.append(\"<option>\" + value + \"</option>\");


    };
$(\"#measDrop\").change();
}




</script>";





  $selectField .= "<script>
$(\"#eqsite\").change(function() {

if ($(this).data('options') == undefined) {
    $(this).data('options', $('#coll option').clone());
}

var key = $(\"#coll\").val(); 
var key2 = $(this).val(); 
var key3 = $(\"#eqmethod\").val(); 


if ( key3 != \"\" || key2 == \"\" )
   DoubleSelect();
else
{

  var vals = [];

  switch(key) {
  ";

  foreach ( $c0_collNsite2meas as $key => $arr )
    {
      if (sizeof($arr)==0) continue;
      $selectField .= "case '".$key."':\n";

      $selectField .= "\t switch(key2) {\n";
	
      foreach ( $arr as $key2 => $arr2)
  	{
  	  if (sizeof($arr2)==0) continue;
  	  $selectField .= "\t case '".$key2."':\n\t\t  vals = [\"".implode ("\",\"", $arr2)."\"]; break; \n";
  	}
      $selectField .= "   \n\t\t} break; \n";
    }
  $selectField .="
  }


  var secondChoice = $(\"#measDrop\");
  secondChoice.empty();
  secondChoice.append(\"<option>\" +\"\"+ \"</option>\");
  for ( value of vals) {
    secondChoice.append(\"<option>\" + value + \"</option>\");
  };
  $(\"#measDrop\").change();
}
});
</script>";



  $selectField .= "<script>
$(\"#eqmethod\").change(function() {

if ($(this).data('options') == undefined) {
 $(this).data('options', $('#coll option').clone());
}

var key = $(\"#coll\").val(); 
var key2 = $(this).val(); 
var key3 = $(\"#eqsite\").val(); 


if ( key3 != \"\" )
{
  DoubleSelect();
}

$(\"#measDrop\").change();

});
</script>";



  $selectField .= "<script>
function clearField(doChange=true) {
document.getElementById(\"inMeas\").value=\"\";
document.getElementById(\"inMeas2\").value=\"\";
document.getElementById(\"inEQIDfield\").value=\"\";
document.getElementById(\"inALIASfield\").value=\"\";
document.getElementById(\"inChannel\").value=\"\";
/* document.getElementById(\"inMTFIDfield\").value=\"\"; */

if (doChange){
document.getElementById(\"coll\").value=\"0\";
document.getElementById(\"measDrop\").value=\"0\";
document.getElementById(\"eqsite\").value=\"0\";
document.getElementById(\"eqmethod\").value=\"0\";
document.getElementById(\"eqtype\").value=\"0\";

$('#coll').change();
$('#measDrop').change();
$('#eqsite').change();
$('#eqmethod').change();
$('#eqtype').change();
}


}
</script>";





  


    $naviContent     = $selectField;
  /* $downloadButtons = $buttonContent; */
  /* $mainContent     .= $tablePrint; */

}
else if($page == "ameas" AND $canEditElec==false)
    {
  $mainContent  = "<div class=\"whitediv\">\n";
  $mainContent .=    "<p>\n";
  $mainContent .=       "You don't have the accesses right to add measurments.";
  $mainContent .=    "</p>\n";
  $mainContent .= "</div>\n";
    }
else { 
  $mainContent = "This shouldn't happen."; 
}
//////////////////////////////////////////////////////////////////////////////
// close database connection
oci_close($conn);
// read the template
$tpl = addslashes(file_get_contents("templates/ElecTemplateCERN.tpl"));
eval("echo \"".$tpl."\";");
//////////////////////////////////////////////////////////////////////////////
function makeDropDown($name, $options, $selected, $chosen, $addon = array(), $params = "") {
  if(count($options) != count($addon)) $addon = array_pad($addon, count($options), "");
  $addon[$selected].= " selected=\"selected\"";
  //echo $chosen;
  //echo $name;
  if($chosen == $name) {
    $style = "selectmenu elecmark"; 
  } else { $style = "selectmenu elec"; }
  $html = "<select class=\"".$style."\" name=\"". $name ."\" ".$params.">";
    
  for($i=0;$i<count($options);++$i)
    $html .= "<option value=\"".$i."\" ".$addon[$i].">".$options[$i]."</option>";
  $html .= "</select>";
  return $html;
}
function makeDropDown_pred($name, $options, $selected, $chosen, $addon = array(), $params = "", $style2= "") {
  if(count($options) != count($addon)) $addon = array_pad($addon, count($options), "");
  $addon[$selected].= " selected=\"selected\"";
  //echo $chosen;


  if($chosen == $name) {
    $style = "selectmenu elecmark"; 
  } else { $style = "selectmenu elec"; }
  $html = "<select class=\"".$style." ".$style2."\"  id=\"". $name ."\" name=\"". $name ."\" ".$params.">";
  for($i=0;$i<count($options);++$i){
    if(isset($_POST['linp']) &&  $_POST[$name] == $options[$i] ){
      $addon[$i].= " selected=\"selected\"";
    }
    $html .= "<option ".$addon[$i].">".$options[$i]."</option>";
  }
  $html .= "</select>";


  return $html;
}

function makeDropDown_add($name, $options, $selected, $addon = array(), $params = "", $style2= "") {
  if(count($options) != count($addon)) $addon = array_pad($addon, count($options), "");
  
  
  


  $style = "selectmenu elec"; 
  $html = "<select class=\"".$style." ".$style2."\"  id=\"". $name ."\" name=\"". $name ."\" ".$params.">";
  for($i=0;$i<count($options);++$i){
    if($options[$i]==$selected){
      $addon[$i].= " selected=\"selected\"";
    }
    $html .= "<option ".$addon[$i].">".$options[$i]."</option>";
  }
  $html .= "</select>";


  return $html;
}



function makeDropDown2($name, $options, $second, $selected, $chosen, $glob_var,  $addon = array(), $params = "") {
  if(count($options) != count($addon)) $addon = array_pad($addon, count($options), "");
  $addon[$selected].= " selected=\"selected\"";
  //echo $chosen;&
  //echo $name;
  if($chosen == $name) {
    $style = "selectmenu elecmark";
  } else { $style = "selectmenu elec"; }
  $html = "<select class=\"".$style."\" id=\"". $name ."\" name=\"". $name ."\" ".$params.">";


  for($i=0;$i<count($options);++$i){
    if(isset($_POST['linp'])    ){
      // if ($_SESSION[$glob_var] == $options[$i]) {
      //
      //     $addon[$i].= " selected=\"selected\"";
      //  }
      //
      if(($_POST[$chosen])!='default' ){

	if ($_POST[$chosen] == $options[$i]){

	  $addon[$i].= " selected=\"selected\"";
	}
      }
    }
    $html .= "<option value=\"".$second[$i]."\" ".$addon[$i].">".$options[$i]."</option>";}
  $html .= "</select>";
  return $html;
}
function getGlobalVar($name) {
  if     ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST[$name])) return $_POST[$name];
  else if($_SERVER['REQUEST_METHOD'] == 'GET'  && isset($_GET [$name])) return $_GET [$name];

  return "";
}
function resetAll($exclude = ""){
  global $form_user, $form_admin, $form_structures;
  if($exclude != "user") $form_user = "";
  if($exclude != "admin") $form_admin = "";
  if($exclude != "structures") $form_structures = "";
}
function resetAllP($exclude = ""){
  global $form_cern, $form_other;//, $form_batches, $form_structures, $form_measurements;
  if($exclude != "cern") $form_cern = "";
  if($exclude != "other") $form_other = "";
  //if($exclude != "batches") $form_batches = "";
  //if($exclude != "structures") $form_structures = "";
  //if($exclude != "measurements") $form_measurements = "";
}
function getChosenTable($choose, $user, $admin, $structures) {
  global $form_user, $form_admin, $form_structures;
  $table = "";

  switch($choose) {
  case "user"        : $table = $user        [$form_user        -1]; resetAll("user"); break;
  case "admin"       : $table = $admin       [$form_admin       -1]; resetAll("admin"); break;
  case "structures"  : $table = $structures  [$form_structures  -1]; resetAll("structures"); break;
  }
  return $table;
}
function getChosenTableP($choose, $cern, $other) { //, $items, $batches, $structures, $measurements) {
  global $form_cern, $form_other; //, $form_batches, $form_structures, $form_measurements;
  $table = "";

  switch($choose) {
  case "cern"       : $table = $cern       [$form_cern       -1]; resetAllP("cern"); break;
    //case "items"       : $table = $items       [$form_items       -1]; resetAll("items"); break;
    //case "batches"     : $table = $batches     [$form_batches     -1]; resetAll("batches"); break;
    //case "structures"  : $table = $structures  [$form_structures  -1]; resetAll("structures"); break;
    //case "measurements": $table = $measurements[$form_measurements-1]; resetAll("measurements"); break;
  default:   $table = $other       [$form_other      -1]; resetAllP("other"); break;
  }
  return $table;
}
function getProjectPattern($conn) { // get list of available projects (letter) to validate inputs of MTF-IDs
  $DBUser  = $_SESSION['DBOwner'];
  $DBTable = "PROJECTS";
  $sql  = "SELECT LETTER FROM ".$DBUser.".".$DBTable;	
  $stid = oci_parse($conn,$sql);
  oci_execute($stid);
  while($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
    $pattern .= $row['LETTER']."|";
  }
  $pattern = substr($pattern, 0, -1);
  return $pattern;
}
function getPattern($conn) {
  $towner = $_SESSION['DBOwner'];
  $table = "EQUIPMENTTYPES";
  $stid = oci_parse($conn, 'SELECT EQTYPECODE, HIGHESTVERSION, ACTUALVERSION FROM '.$towner.'.'.$table);
  oci_execute($stid);
  while($eqtype = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
    $pattern .= $eqtype['EQTYPECODE']."|";
  }
  $pattern = substr($pattern, 0, -1);
  return $pattern;
}
?>