<?php

require_once "library/page.php";


// Contact Page
// ============================================
class ContactPage extends Page {

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		return $this->html->template("contact");
	}
}

$page = new ContactPage($this, "contact");

?>
