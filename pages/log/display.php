<?php

require_once "library/page.php";
require_once "pages/all/all.php";


// DisplayPage
// ============================================
class DisplayPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $table  = NULL;
	public $assoc  = array();
	public $tables = array();

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->table  = NULL;
		$this->assoc  = array();
		$this->tables = array();
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if(array_key_exists("table", $this->post)) $this->table = $this->post["table"];
		if(in_array($this->post["do"], array("tablesUser", "tablesAdmin", "tablesStruct", "tablesPViews", "tablesGViews"))){
			$this->table = $this->post[$this->post["do"]];
			$this->post["p"] = 0;
		}

		if     ($this->post["do"]=="exportCsv"   ) $this->submitExport("csv");
		else if($this->post["do"]=="exportExcel" ) $this->submitExport("xls");
		else if($this->post["do"]=="exportInvCsv") $this->submitExport("inv");
		else if($this->post["do"]=="download"    ) $this->submitDownload();

		return $this->loadPage();
	}



	// private members and methods
	// ======================================== 

	// loadBody
	// ---------------------------------------- 
	private function loadBody(){
		/* Builds the bottom part of the form given a table has been selected */

		if(empty($this->table)) return;

		// buttons
		$this->html->set("buttons", $this->html->template("display_buttons"));

		// build the table
		$table_body = "";
		$p = array_key_exists("p", $this->globals) ? $this->globals["p"] : 0;
		$s = $this->master->config->has("pageSize") ? $this->master->config->pageSize->value : 25;

		$tableAtts = $this->loadTable();
		$this->html->set("table_head", $tableAtts[0]);
		$this->html->set("table_body", $tableAtts[1]);

		// the template
		$this->html->set("table"     , $this->table);
		$this->html->set("body"      , $this->html->template("display_body"));
	}

	// loadAssoc
	// ---------------------------------------- 
	private function loadAssoc(){
		/* Loads the association between tablename and viewname */

		$raw = $this->db->readSql(strtoupper("select tablename, tabledescription, tabletype, tableviewname from ".$this->db->owner.".DBCONTTABLES"));
		$this->assoc  = array();
		$this->tables = array("user"         =>array("none"=>"Select a Table View"), 
		                      "admin"        =>array("none"=>"Select a Table View"),
		                      "structure"    =>array("none"=>"Select a Table View"),
		                      "personalviews"=>array("none"=>"Select a Table View"),
		                      "generalviews" =>array("none"=>"Select a Table View"));
		foreach($raw as $row){
			$type = strtolower($row["tabletype"]);
			if(!array_key_exists($type, $this->tables)) continue;
			$this->assoc        [strtolower($row["tableviewname"])] = $row["tablename"];
			$this->tables[$type][strtolower($row["tableviewname"])] = $row["tabledescription"];
		}
	}

	// loadHeader
	// ---------------------------------------- 
	private function loadHeader(){
		/* Builds the page and fills it according to the _POST selection */

		// building the table menu
		$users     = $this->html->makeSelect("tablesUser"  , $this->tables["user"         ], $this->table, array(), array("none"=>"class=\"hidden\""), false, "submitIt('tablesUser')"  , "size=\"1\"", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"");
		$admin     = $this->html->makeSelect("tablesAdmin" , $this->tables["admin"        ], $this->table, array(), array("none"=>"class=\"hidden\""), false, "submitIt('tablesAdmin')" , "size=\"1\"", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"");
		$structure = $this->html->makeSelect("tablesStruct", $this->tables["structure"    ], $this->table, array(), array("none"=>"class=\"hidden\""), false, "submitIt('tablesStruct')", "size=\"1\"", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"");
		$persviews = $this->html->makeSelect("tablesPViews", $this->tables["personalviews"], $this->table, array(), array("none"=>"class=\"hidden\""), false, "submitIt('tablesPViews')", "size=\"1\"", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"");
		$genviews  = $this->html->makeSelect("tablesGViews", $this->tables["generalviews" ], $this->table, array(), array("none"=>"class=\"hidden\""), false, "submitIt('tablesGViews')", "size=\"1\"", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"");

		$this->html->set("header", $this->html->template("display_header", array("dropdown_user"      =>$users, 
		                                                                         "dropdown_admin"     =>$admin, 
		                                                                         "dropdown_structures"=>$structure, 
		                                                                         "dropdown_pviews"    =>$persviews,
		                                                                         "dropdown_gviews"    =>$genviews)));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage(){
		/* Builds the page and fills it according to the _POST selection */
		$this->loadAssoc();
		$this->loadHeader();
		$this->loadBody();
		return $this->html->template("display");
	}

	// loadTable
	// ---------------------------------------- 
	private function loadTable($noPageSel=false, $exclude=array(), $useRaw=false){
		/* Generates the HTML table from the data in the data base */

		$exclude = array_merge(array("docloc"), $exclude); // list of columns to exclude always

		$short = substr($this->table, 0, -4);
		$table = $this->db->get($this->table);
		$cs = new DbConfig($this->master, "fetchall");
		$cs->table = $this->table; // pageSelector is running numConfig, thus needs to know about the table name
		//$cs->avoid = $exclude; // columns NOT to display // FIXME: NOT WORKING YET!
		$cs->slim  = true;

		// custom sorting
		if(isset($this->post["orderBy"]) && isset($this->post["orderType"]) && !empty($this->post["orderBy"]) && !empty($this->post["orderType"])){
			if($this->post["orderBy"]!="none" && $this->post["orderType"]!="none")
				$cs->order($this->post["orderBy"], $this->post["orderType"]);
			if($this->post["orderType"]=="none"){ 
				$this->post["orderBy"  ]="none";
				$this->post["orderType"]="asc";
			}
		}

		// the page selector and the limit attributes to the config
		$start = 0;
		if(!$noPageSel) $start = pageSelector($this, $cs);

		// build table header
		$oidx    = 0;
		$colraws = array("none");
		$columns = array("No.");
		$colsuse = array();
		foreach($table->columns() as $colname){

			if($colname=="userpasswd") continue;
			$any = findAny($exclude, $colname);
			if(!empty($any)) continue;

			$webtr = $this->db->readTable("webtranslations", array("dbcolumnfreetext"), array("dbtablename"     =>strtoupper($this->assoc[$this->table]),  // need to get the table name not the tableviewname
			                                                                                  "dbtablecolumn"   =>strtoupper($colname),
			                                                                                  "dbcolumnfreetext"=>"not null"));
			$use = $colname;
			//if($webtr->count()==0) continue; // exclude column without description
			if(!$useRaw && $webtr->count()!=0) $use = $webtr->dbcolumnfreetext;

			array_push($columns, htmlentities($use, ENT_QUOTES));
			array_push($colsuse, $colname);
			array_push($colraws, $colname); //replaceFirst("_", ".", $colname)); single view here
		}
		if(isset($this->post["orderBy"])) $oidx = array_search($this->post["orderBy"], $colraws);
		$type = isset($this->post["orderType"]) ? $this->post["orderType"] : "asc";
		$cols = array();
		$i    = 0;
		foreach($columns as $col){
			$add = $oidx==$i ? ($type=="asc" ? "tablesorterThDesc" : "tablesorterThAsc") : "";
			array_push($cols, sprintf("<th class=\"%s\" onclick=\"toggleSort('%s');submitIt('resort');\"><div><b>%s</b></div></th>", $add, $colraws[$i], $col));
			++$i;
		}
		$table_head = implode("",$cols);


		// build table body
		$table->read($cs, true);
		$rows = array();
		$i    = 1;
		foreach($table->slim as $row){
			$theRow = array("<td>".($start+$i)."</td>");
			foreach(array_keys($row) as $colname){
				if(!in_array($colname, $colsuse)) continue;
				$any = findAny($exclude, $colname);
				if(!empty($any)) continue;
				$item = $row[$colname];
				if(in_array($colname, array("mtfid", "partsbatchmtfid"))){
					$pre = substr($item, 0, 5);
					$typ = "<span style=\"color:#990033; font-size:130%;\">".substr($item, 5, 4)."</span>";
					$aft = substr($item, 9, 5);
					$item = "<b>".$pre."&nbsp;".$typ."&nbsp;".$aft."</b>";
					array_push($theRow, "<td>".$item."</td>");
					continue;
				}
				if($colname=="docname" && !empty($row["id_documents"]) && !empty($item)){
					$item = "<a href=\"#\" onclick=\"set('docId', ".$row["id_documents"].");submitIt('download')\">" . ($row["docname"] != null ? htmlentities($row["docname"], ENT_QUOTES) : "document") . "</a>";
					array_push($theRow, "<td>".$item."</td>");
					continue;
				}
				if(                            strpos($colname, "margin")!==false && !empty($item))
					$item = strval(floatval($item));
				if( $colname != "eventdate" && strpos($colname, "date")!==false  && !empty($item))
					$item = timestamp(1, $item);
				if(($colname == "eventdate" || strpos($colname, "time")!==false) && !empty($item))
					$item = timestamp(2, convertDate($item));
				if(                            strpos($colname, "_ts" )!==false  && !empty($item))
					$item = timestamp(2, convertDate($item));

				array_push($theRow, "<td>".(!empty($item) ? htmlentities($item, ENT_QUOTES) : "&nbsp;")."</td>");
			}
			array_push($rows, "<tr>".implode("", $theRow)."</tr>");
			++$i;
		}
		return array(implode("",$cols), implode("",$rows));
	}

	// submitDownload
	// ---------------------------------------- 
	private function submitDownload(){
		/* Downloads a single document */
		downloadDoc($this);
	}

	// submitExport
	// ---------------------------------------- 
	private function submitExport($type="csv") {
		/* When requesting an export */

		// generate the full table but without start/end range
		$this->loadAssoc();
		$tableAtts = $this->loadTable(true, $type=="inv" ? array('id_', 'edittime') : array(), $type=="inv");

		$this->html->set("invisi_head", $tableAtts[0]);
		$this->html->set("invisi_body", $tableAtts[1]);
		$this->html->set("onload"     , sprintf("exportTable('.invisiTable', '%s')", $type=="xls" ? "excel" : "csv"), "main");
	}
}

$page = new DisplayPage($this, "display");

?>
