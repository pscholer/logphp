<?php

require_once "library/page.php";
require_once "pages/all/all.php";


// AccessType
// ============================================
class AccessType {
	const None  = 0;
	const Item  = 1;
	const Batch = 2;
	const Group = 3;
}

// note there is a three-step logic in the assembly of the MTF id
// 1st step: ask eqType with pattern according to choice of accessType => submit with do='code'
// 2nd step: ask version with highest value according to eqType, eqType field becomes read-only => submit with do='version'
// 3rd step: assemble the entire MTF id, also the version becomes ready-only now


// Register Page
// ============================================
class RegisterPage extends Page {


	// public members and methods
	// ========================================    

	// members
	// ----------------------------------------    
	public $accessType;
	public $mtfId;
	public $eqId;

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->mtfId      = Null;
		$this->eqId       = Null;
		$this->accessType = isset($this->post["accessType"]) ? $this->post["accessType"] : AccessType::None;
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit(){
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if(isset($this->post["accessType"])) $this->accessType = $this->post["accessType"];
		if(isset($this->post["mtfId"     ])) $this->mtfId      = $this->post["mtfId"];

		if($this->post["do"]!="version")
			$this->html->set("buttonState", "disabled");

		$res = false;
		if     ($this->post["do"]=="code"   ) $this->submitCode();
		else if($this->post["do"]=="version") $this->submitVersion();
		else if($this->post["do"]=="save"   ) $res = $this->submitSave();
		
		if($res) $this->db->commit();
		else     $this->db->undo(); 

		return $this->loadPage();
	}



	// private members and methods
	// ========================================    

	// loadBody
	// ---------------------------------------- 
	private function loadBody(){
		/* Building the body of the form */

		if($this->accessType == AccessType::None) return;	

		$statOpts = $this->master->getOptionsStatus(                    );
		$locOpts  = $this->master->getOptionsSites (                    );
		$statIgn  = $this->master->getOptionsStatus("duringregisterflag"); // statuses to change it to
		$locIgn   = $this->master->getOptionsSites ("duringregisterflag"); // sites to change it to
		$statDis  = array_keys(array_diff($statOpts, $statIgn)); // id of statuses to disable
		$locDis   = array_keys(array_diff($locOpts , $locIgn )); // id of sites to disable   

		// selects

		$statusId = array_key_exists("statusId", $this->post) ? $this->post["statusId"] : array_search("registered", $statOpts);
		$this->html->set("selectStatus", $this->html->makeSelect("statusId", $statOpts, $statusId, $statDis, array(), false, "", "class='invisiblelike'"));

		$locId    = array_key_exists("majorLocId", $this->post) ? $this->post["majorLocId"] : $this->master->getUserSite();
		$this->html->set("selectSite"  , $this->html->makeSelect("majorLocId", $locOpts, $locId, $locDis, array(), false, "", "class='invisiblelike'"));

		$docType = array_key_exists("docType", $this->post) ? $this->post["docType"] : -1;
		$this->html->set("selectDocType", $this->html->makeSelect("docType", $this->master->doctypes, $docType, array(), array(), false, "", "class='invisiblelike'"));

		// extra stuff for batch and group
		if($this->accessType==AccessType::Group)
			$this->html->set("extra", $this->html->template("register_body_group"));
		if($this->accessType==AccessType::Batch)
			$this->html->set("extra", $this->html->template("register_body_batch"));

		// eqType pattern
		if($this->accessType==AccessType::Batch)
			$this->html->set("patternType", $this->master->patternBatch);
		else
			$this->html->set("patternType", $this->master->patternNoBatch);

		// other variables
		$this->html->set("mypattern", $this->accessType==AccessType::Batch ? $this->master->patternBatch : $this->master->patternNoBatch);
		$this->html->set("username" , $this->globals["username"]);
		$this->html->set("entity"   , $this->accessType==AccessType::Group ? "GROUP" : ($this->accessType==AccessType::Batch ? "BATCH" : "ITEM"));
		$this->html->set("eventDate", array_key_exists("eventDate", $this->post) ? $this->post["eventDate"] : $this->master->eventDateSh);

		// the template
		$this->html->set("body", $this->html->template("register_body"));
	}

	// loadHeader
	// ---------------------------------------- 
	private function loadHeader(){
		/* Build the header, where you can select the access type */

		$opts = array();
		$opts[0] = "Select the Access Type";
		$opts[1] = "Single Equipment Item";
		$opts[2] = "Batch";
		$opts[3] = "Group of Items";

		$this->html->set("accessTypeSel", $this->html->makeSelect("accessTypeSel", $opts, $this->accessType, array(0), array(), false, "submitItAs('accessType', this.value)", "class='buttonlike nomargin'"));
		$this->html->set("mtfId"        , $this->mtfId);
		$this->html->set("eqId"         , $this->eqId );
		$this->html->set("header"       , $this->html->template("register_header"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage(){
		/* Build the form */
		$this->loadHeader();
		$this->loadBody();
		return $this->html->template("register");
	}

	// submitCode
	// ---------------------------------------- 
	private function submitCode(){
		/* MTF assembly second step: eqType submitted, asking version */

		if(!array_key_exists("eqType", $this->post)) return;

		$et = $this->db->readTable("equipmenttypes", array("actualversion", "highestversion"),
		                                             array("eqtypecode" => $this->post["eqType"]));
		$eqVersion = $et->actualversion < $et->highestversion ? $et->actualversion : $et->highestversion; // asking POST value does not make sense here
	
		$this->html->set("eqTypeReadOnly", "readonly"                 );
		$this->html->set("eqTypeDisabled", "disabled"                 );
		$this->html->set("eqVersion"     , strval($eqVersion         ));
		$this->html->set("highestVersion", strval($et->highestversion));
		$this->html->set("versionField"  , $this->html->template("register_body_typeVersion"));
	}

	// submitSave
	// ---------------------------------------- 
	private function submitSave(){
		/* When pressing the SAVE button */

		if(!array_key_exists("eqType", $this->post) || !array_key_exists("eqVersion", $this->post) || empty($this->post["mtfId"])) return;

		// block form once submitted
		$this->html->set("activeState"        , "readonly");
		$this->html->set("activeStateDisabled", "disabled");

		// preparing variables
		$dates = array($this->post["orderedDate"], $this->post["deliveryDate"], $this->post["manufacturingDate"]);
		$eventDate   = earliest($this->post["eventDate"], $dates);
		$eventDateDb = isValidDate($eventDate) ? dbStringDate($eventDate) : $this->master->eventDateDb;

		$eqTypeCode = $this->db->readTable("equipmenttypes", array("id_equipmenttypes"), 
		                                                     array("eqtypecode"=>$this->post["eqType"]))->id_equipmenttypes;


		// inserting equipment
		$insert = array("partsbatchmtfid" => $this->post["mtfId"],
		                "otherid"         => $this->post["otherId"],
		                "eqtypecodeid"    => $eqTypeCode,
		                "websiteusercr"   => $this->globals["username"],
		                "websiteusered"   => $this->globals["username"],
		                "eventdate"       => $eventDateDb);
		if($this->accessType == AccessType::Batch){
			$insert["quantity"    ] = $this->post["quantity"];
			$insert["goodquantity"] = $this->post["goodQuantity"] <= $this->post["quantity"] ? $this->post["goodQuantity"] : $this->post["quantity"];
			$this->html->set("goodQuantity", $insert["goodquantity"]);
		}
		else if($this->accessType == AccessType::Group){
			$insert["quantity"    ] = $this->post["quantity"];
		}	

		$this->db->equipment->append($insert);
		$this->db->equipment->push();	
		if($this->db->error()){
			$this->vb->error("Could not create new equipment entry!", true);
			return false;
		}

		$eqId = $this->db->equipment->id_equipment;
		$this->html->set("eqId", $eqId);

		// orphanage
		$this->db->parenting->append(array("eqentryid"       =>$eqId,
		                                   "parenteqentryid" =>$this->master->config->eqIdOrph->value,
		                                   "isactiveflag"    =>"T",
		                                   "websiteusercr"   =>$this->globals["username"],
		                                   "websiteusered"   =>$this->globals["username"],
		                                   "eventdate"       =>$eventDateDb));
		$this->db->parenting->push();	
		if($this->db->error()){
			$this->vb->error("Could not create orphanage entry for new equipment!", true);
			return false;
		}

		// status and location
		$this->db->statuslocation->append(array("eqentryid"     =>$eqId,
		                                        "statusid"      =>$this->post["statusId"],
		                                        "majorlocid"    =>$this->post["majorLocId"],
		                                        "minorlocdesc"  =>$this->post["minorLocDesc"],
		                                        "websiteusercr" =>$this->globals["username"],
		                                        "websiteusered" =>$this->globals["username"],
		                                        "eventdate"     =>$eventDateDb));
		$this->db->statuslocation->push();	
		if($this->db->error()){
			$this->vb->error("Could not create status-location entry for new equipment!", true);
			return false;
		}

		// equipment details
		$this->db->equipmentdetails->append(array("eqentryid"         =>$eqId,
		                                          "supplier"          =>$this->post["supplier"],
		                                          "cablelength"       =>$this->post["cableLength"],
		                                          "deliverydate"      =>dbStringDate($this->post["deliveryDate"]),
		                                          "manufacturingdate" =>dbStringDate($this->post["manufacturingDate"]),
		                                          "orderedby"         =>$this->post["orderedBy"],
		                                          "ordereddate"       =>dbStringDate($this->post["orderedDate"]),
		                                          "orderreference"    =>$this->post["orderRef"],
		                                          "websiteusercr"     =>$this->globals["username"],
		                                          "websiteusered"     =>$this->globals["username"],
		                                          "eventdate"         =>$eventDateDb));
		$this->db->equipmentdetails->push();	
		if($this->db->error()){
			$this->vb->error("Could not insert equipment details!", true);
			return false;
		}

		// comment
		if(!empty($this->post["comment"])){
			if(!addComment($this, $this->post["comment"], -1, $eventDate, array($eqId), array(), "register", true, false)) {
				$this->vb->error("Could not insert equipment comment!", true);
				return false;
			}
		}

		// document
		if(is_uploaded_file($_FILES['uplnewdoc']['tmp_name'])) {
			if(!uploadDoc($this, "uplnewdoc", intval($this->post["docType"]), $eventDate, array($eqId), array(), "register", true, false)){
				$this->vb->error("Could not upload document!", true);
				return false;
			}
		}

		// since we inserted an equipment, we need to increase the highest entry!
		if($this->accessType != AccessType::Group){
			$et = $this->db->readTable("equipmenttypes", array("id_equipmenttypes", "highestentry"), array("eqtypecode"=>$this->post["eqType"]));
			$et->highestentry++;
			$et->push();
			if($this->db->error()){
				$this->vb->error("Could not increase highest entry for equipment type!", true);
				return false;
			}
		}

		// return
		$this->vb->success(sprintf("New equipment (EQ Id = %d, MTF Id = %s, Alias = %s) created successfully!", $eqId, $this->mtfId, $this->post["otherId"]));
		return true;
	}

	// submitVersion
	// ---------------------------------------- 
	private function submitVersion(){
		/* MTF assembly third step: eqVersion submitted, assembling MTF */

		if(!array_key_exists("eqType", $this->post) || !array_key_exists("eqVersion", $this->post)) return;

		$this->html->set("eqTypeReadOnly"   , "readonly"); // comment these four lines (and the one in the 'code' step) if you want to ...
		$this->html->set("eqVersionReadOnly", "readonly"); // ... give the user a chance to change it afterwards
		$this->html->set("eqTypeDisabled"   , "disabled"); // ...
		$this->html->set("eqVersionDisabled", "disabled"); // ...

		$this->html->set("versionField", $this->html->template("register_body_typeVersion")); // keeping the version field displayed ...
		                                                                                      // (highest version is part of the post, ...
		                                                                                      // thus set automatically (hidden field)

		$pre       = "20MN";
		$eqType    = $this->post["eqType"];
		$eqVersion = $this->post["eqVersion"];

		$c = new DbConfig($this->master, "projects");
		$c->column = "pj.letter";
		$c->joinon = "pj.id_projects = et.projectid";
		$c->select("et.eqtypecode", $eqType);
		$view = $this->db->read("view", $c);
		$letter = $view->pj_letter;

		$half    = sprintf("%s%s%s%d", $pre, $letter, $eqType, $eqVersion);
		$highest = 0;
		if($this->accessType == AccessType::Group){
			for($i = 9999; $i > 9900; $i--) {
				$exists = ($this->db->readTable("equipment", array("id_equipment"), array("partsbatchmtfid"=>$half.str_pad($i, 4, "0", STR_PAD_LEFT)))->count()>0);
				if(!$exists) {
					$highest = $i; 
					break;
				}
			}
		}
		else {
			$highest = $this->db->readTable("equipmenttypes", array("highestentry"), array("eqtypecode"=>$eqType))->highestentry+1;
		}

		$post = str_pad($highest, 4, "0", STR_PAD_LEFT);
		$this->mtfId = $half.$post;
	}
}


$page = new RegisterPage($this, "register");

?>
