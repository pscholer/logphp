<?php

require_once "library/page.php";
require_once "pages/all/all.php";


// Overview Page
// ============================================
class OverviewPage extends Page {


	// public members and methods
	// ======================================== 

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if     ($this->post["do"]=="exportCsv"  ) $this->submitExport("csv");
		else if($this->post["do"]=="exportExcel") $this->submitExport("xls");
		return $this->loadPage();
	}



	// private members and methods
	// ======================================== 

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Generates the form and fills it with the requested information */

		// define values --------------------

		// defaults
		$defs = array();
		$defs["filterSiteId"   ] = 0;
		$defs["filterStatusId" ] = 0;
		$defs["filterNcf"      ] = "0";
		$defs["filterProjectId"] = 0;
		$defs["filterTypeId"   ] = 0;
		$defs["predef"         ] = "none";
		$defs["predefPrev"     ] = "";

		foreach($defs as $key=>$val){
			if(!array_key_exists($key, $this->post)) 
				$this->post[$key] = $val;
		}

		// for certain search cases, reset some of the filters
		if($this->post["predef"]=="none" && $this->post["predefPrev"]!="none"){ // "All material"
			if($this->post["predefPrev"]=="") $this->post["predefPrev"]="none";
			foreach($defs as $key=>$val){ // resetting all filters in case we switch to it
				if(strpos($key, "filter")===false) continue;
				$this->post[$key] = $defs[$key];
			}
		}
		if($this->post["predef"]=="here"){ // "Material here"
			if($this->post["filterSiteId"]==$defs["filterSiteId"]) 
				$this->post["filterSiteId"] = $this->master->getUserSite();
		}
		if($this->post["predef"]=="order"){ // "Material ordered"
			$this->post["filterSiteId"] = $defs["filterSiteId"];
		}



		// make option selects --------------

		// Options Sites
		$opts = array(0=>"no filter");
		$opts = $opts + $this->master->getOptionsSites();
		$this->html->set("options_filterSites", $this->html->makeOptions($opts, $this->post["filterSiteId"]));

		// Options Status
		$opts = array(0=>"no filter");
		$opts = $opts + $this->master->getOptionsStatus();
		$this->html->set("options_filterStatus", $this->html->makeOptions($opts, $this->post["filterStatusId"]));

		// Options NCF
		$opts = array("0"=>"no filter");
		$opts = $opts + $this->master->ncfs;
		$this->html->set("options_filterNcfs", $this->html->makeOptions($opts, $this->post["filterNcf"]));

		// Options Project
		$table = $this->db->readTable("projects", array("id_projects", "projname"), array(), array(), array("sort"=>"asc"));
		$opts  = array(0=>"no filter");
		foreach($table as $row) $opts[$row->id_projects] = $row->projname;
		$this->html->set("options_filterProjects", $this->html->makeOptions($opts, $this->post["filterProjectId"]));

		// Options Types
		$where = array();
		$sort  = (!isset($this->post["filterTypeIdSort"]) || empty($this->post["filterTypeIdSort"])) ? array("eqtypecode"=>"asc") : array("eqtypename"=>"asc");
		if(isset($this->post["filterProjectId"]) && !empty($this->post["filterProjectId"])) 
			$where["projectid"] = $this->post["filterProjectId"];
		$table = $this->db->readTable("equipmenttypes", array("id_equipmenttypes", "eqtypecode", "eqtypename"), $where, array(), $sort);
		$opts  = array(0=>"no filter");
		foreach($table as $row) $opts[$row->id_equipmenttypes] = sprintf("%s - %s", $row->eqtypecode, $row->eqtypename);
		$type = array_key_exists("filterTypeId", $this->post) ? $this->post["filterTypeId"] : 0;
		$this->html->set("options_filterTypes", $this->html->makeOptions($opts, $type));

		// Options What
		$choice = array("none"=>"All material", "here"=>"Material now here", "herewas"=>"Material has been here", "hereship"=>"Material has been shipped here", "incom"=>"Material incoming", "outgo"=>"Material outgoing", "order"=>"Material ordered");
		$this->html->set("options_filterWhats", $this->html->makeOptions($choice, $this->post["predef"]));


		// build the table
		$tableAtts = $this->loadTable();
		$this->html->set("table_head", $tableAtts[0]);
		$this->html->set("table_body", $tableAtts[1]);


		// ------ the template ------
		return $this->html->template("overview");
	}

	// loadTable
	// ---------------------------------------- 
	private function loadTable($noPageSel = false){
		/* builds the table with header and body */

		// FIXME: in case you wanna do a select field to add or remove columns to be displayed: this is the place to do it!

		// columns to be selected
		$allcols = array("distinct e.id_equipment", "sh.shipmentintid", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", "e.subbatchid", "t.transtext", "e.quantity", "eds.deliverydate", "s.statusname", "l.sitename", "sh.shippingfrom", "sh.shippingdestination", "sh.shippingperson", "sh.shippingcompany", "sh.shippingdate", "sh.receivingdate");

		// columns to be displayed
		$display = array("e.id_equipment", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", "e.subbatchid", "t.transtext", "e.quantity", "eds.deliverydate", "s.statusname", "l.sitename");
		if(isset($this->post["display"]) && !empty($this->post["display"]))
			$display = explode($this->post["display"]);
		else if($this->post["predef"] == "hereship")
			$display = array("sh.shipmentintid", "e.id_equipment", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", "e.subbatchid", "t.transtext", "e.quantity", "sh.shippingfrom", "s.statusname", "l.sitename", "sh.shippingdate", "sh.receivingdate");
		else if($this->post["predef"] == "incom")
			$display = array("sh.shipmentintid", "e.id_equipment", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", "e.subbatchid", "t.transtext", "e.quantity", "eds.deliverydate", "sh.shippingfrom", "sh.shippingperson", "sh.shippingcompany", "sh.shippingdate");
		else if($this->post["predef"] == "outgo")
			$display = array("sh.shipmentintid", "e.id_equipment", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", "e.subbatchid", "t.transtext", "e.quantity", "eds.deliverydate", "sh.shippingdestination", "sh.shippingperson", "sh.shippingcompany", "sh.shippingdate");


		// get the good column names
		$colNames = array();
		foreach($display as $col){
			$cc = explode(".", $col);
			$c  = count($cc)==2 ? $cc[1] : $cc[0];
			$t  = count($cc)==2 ? array_search($cc[0], $this->db->reformat) : "equipment";
			$webtrnsl = $this->db->readTable("webtranslations", array("dbcolumnfreetext"), array("dbtablename"  =>strtoupper($t), 
			                                                                                     "dbtablecolumn"=>strtoupper($c)));
			if($webtrnsl->count()==1) $colNames[$col] = $webtrnsl->dbcolumnfreetext;
			else                      $colNames[$col] = $c;
		}

		// request
		$cj = new DbConfig($this->master, "sub");
		$cj->column = "eqentryid";
		$cj->reformat("FIRST_VALUE(STATUSID   IGNORE NULLS) OVER (PARTITION BY EQENTRYID ORDER BY EVENTDATE DESC)", "last_status"  );
		$cj->reformat("FIRST_VALUE(MAJORLOCID IGNORE NULLS) OVER (PARTITION BY EQENTRYID ORDER BY EVENTDATE DESC)", "last_location");
		$cj->select("isvalidflag", "T");
		$cj->table = "statuslocation";

		$cs = new DbConfig($this->master, "table");
		$cs->columns = $allcols; 
		$cs->joinconfig = $cj;
		$cs->joinon = "*sub.eqentryid       = e.id_equipment";
		$cs->joinon = "s.id_statuses        = last_status";
		$cs->joinon = "l.id_locations       = last_location";
		$cs->joinon = "et.id_equipmenttypes = e.eqtypecodeid";
		$cs->joinon = "eds.eqentryid        = e.id_equipment";
		$cs->joinon = "t.code               = e.isnonconfflag";
		$cs->joinon = "sh.eqentryid         = e.id_equipment";
		$cs->select("s.statusname"       , "not null");
		$cs->select("s.prioroverviewflag", "A"       );
		$cs->select("l.sitename"         , "not null");
		$cs->select("l.prioroverviewflag", "A"       );
		$cs->slim = true;

		// custom sorting
		if(isset($this->post["orderBy"]) && isset($this->post["orderType"]) && !empty($this->post["orderBy"]) && !empty($this->post["orderType"])){
			if($this->post["orderBy"]!="none" && $this->post["orderType"]!="none")
				$cs->order($this->post["orderBy"], $this->post["orderType"]);
			if($this->post["orderType"]=="none"){ 
				$this->post["orderBy"  ]="none";
				$this->post["orderType"]="asc";
			}
		}

		// checkboxes
		$this->html->set("filterSiteIdAny"  , !isset($this->post["filterSiteIdAny"  ]) || empty($this->post["filterSiteIdAny"  ]) ? "" : "checked");
		$this->html->set("filterStatusIdAny", !isset($this->post["filterStatusIdAny"]) || empty($this->post["filterStatusIdAny"]) ? "" : "checked");
		$this->html->set("filterTypeIdSort" , !isset($this->post["filterTypeIdSort" ]) || empty($this->post["filterTypeIdSort" ]) ? "" : "checked");


		// filter site
		if($this->post["filterSiteId"]>0) {
			//if(isset($this->post["filterSiteIdAny"]) && $this->post["filterSiteIdAny"]==1)
			//	$cs->raw(strtoupper("and exists (select majorlocid from ".$this->db->owner.".statuslocation where eqentryid=e.id_equipment and majorlocid=".strval($this->post["filterSiteId"])." and isvalidflag='T')"),7); 
			//else
			//	$cs->select("last_location", $this->post["filterSiteId"]);
			$this->html->set("classFilter1", "emphasized");
		}
		else
			$this->html->set("classFilter1", "general");

		// filter status
		if($this->post["filterStatusId"]>0) {
			if(isset($this->post["filterStatusIdAny"]) && $this->post["filterStatusIdAny"]==1)
				$cs->raw(strtoupper("and exists (select statusid from ".$this->db->owner.".statuslocation where eqentryid=e.id_equipment and statusid=".strval($this->post["filterStatusId"])." and isvalidflag='T')"),7); 
			else
				$cs->select("last_status", $this->post["filterStatusId"]);
			$this->html->set("classFilter2", "emphasized");
		}
		else
			$this->html->set("classFilter2", "general");

		// filter project
		if($this->post["filterProjectId"]>0) {
			$cs->select("et.projectid", $this->post["filterProjectId"]);
			$this->html->set("classFilter3", "emphasized");
		}
		else
			$this->html->set("classFilter3", "general");

		// filter EQ Type
		if($this->post["filterTypeId"]>0) {
			$cs->select("e.eqtypecodeid", $this->post["filterTypeId"]);
			$this->html->set("classFilter4", "emphasized");
		}
		else
			$this->html->set("classFilter4", "general");

		// filter NCF
		if(strval($this->post["filterNcf"])!="0") {
			$cs->select("e.isnonconfflag", $this->post["filterNcf"]=="X" ? NULL : $this->post["filterNcf"]);
			$this->html->set("classFilter5", "emphasized");
		}
		else
			$this->html->set("classFilter5", "general");

		// filter what
		if(in_array($this->post["predef"], array("none", "here")) && $this->post["filterSiteId"]>0) { // material here (default)
			$cs->select("last_location"         , $this->post["filterSiteId"]);
		}
		else if($this->post["predef"] == "herewas") { // material has been here
			$cs->raw(strtoupper("and exists (select majorlocid from ".$this->db->owner.".statuslocation where eqentryid=e.id_equipment and majorlocid=".strval($this->post["filterSiteId"])." and isvalidflag='T')"),7); 
		}
		else if($this->post["predef"] == "hereship") { // material has been shipped here
			$cs->select("sh.shippingdestination", $this->post["filterSiteId"]);
		}
		else if($this->post["predef"] == "incom") { // material incoming
			$cs->select("sh.shippingdestination", $this->post["filterSiteId"]);
			$cs->select("sh.receivingdate"      , "null"                     );
		}
		else if($this->post["predef"] == "outgo") { // material outgoing
			$cs->select("sh.shippingfrom"       , $this->post["filterSiteId"]);
			$cs->select("sh.receivingdate"      , "null"                     );
		}
		else if($this->post["predef"] == "order") { // material ordered
			$cs->select("s.statusname"          , "ordered"                   );
		}
		$this->html->set("classFilter6", "emphasized");


		// the page selector and the limit attributes to the config
		$start = 0;
		if(!$noPageSel) $start = pageSelector($this, $cs);
		
		// query
		$tview = $this->db->read("table", $cs); 

		// columns
		$oidx    = 0;
		$colraws = array("none");
		$columns = array("No.");
		foreach($display as $col){
			$use = $colNames[$col];
			if     ($col == "sh.shippingfrom"       ) $use = "Shipping Site";
			else if($col == "sh.shippingdestination") $use = "Shipping Destination";
			array_push($columns, htmlentities($use, ENT_QUOTES));
			array_push($colraws, $col);
		}
		if(isset($this->post["orderBy"])) $oidx = array_search($this->post["orderBy"], $colraws);
		$type = isset($this->post["orderType"]) ? $this->post["orderType"] : "asc";
		$cols = array();
		$i    = 0;
		foreach($columns as $col){
			$add = $oidx==$i ? ($type=="asc" ? "headerSortDown" : "headerSortUp") : "";
			array_push($cols, sprintf("<th class=\"vertical header %s\" onclick=\"toggleSort('%s');submitIt('resort');\"><div><b>%s</b></div></th>", $add, $colraws[$i], $col));
			++$i;
		}

		// rows
		$allSites = $this->master->getOptionsSites();
		$columns  = $tview->columns();
		$rows     = array();
		$i        = 1;
		foreach($tview->slim as $row){
			$theRow = array("<td>".($start+$i)."</td>");
			foreach($display as $col){
				$c = str_replace(".", "_", $col);
				$item = $row[$c];
				if(strpos($c, "DATE")!==false) $item = timestamp(1, $row[$c]);
				if($col=="sh.shippingfrom"       ) $item = $allSites[$item];
				if($col=="sh.shippingdestination") $item = $allSites[$item];
				//$item = $row->get(str_replace(".", "_", $col));
				//if(strpos(strtoupper($col), "DATE")!==false) $item = timestamp(1, $row->get($col));
				array_push($theRow, sprintf("<td>%s</td>", empty($item) ? "&nbsp;" : htmlentities($item)));
			}
			array_push($rows, "<tr>".implode("", $theRow)."</tr>");
			++$i;
		}

		return array(implode("", $cols), implode("", $rows));
	}

	// submitExport
	// ---------------------------------------- 
	private function submitExport($type="csv") {
		/* Exporting the table */

		$tableAtts = $this->loadTable(true);
		$this->html->set("invisi_head", $tableAtts[0]);
		$this->html->set("invisi_body", $tableAtts[1]);
		
		$this->html->set("onload", sprintf("exportTable('.invisiTable', '%s')", $type=="xls" ? "excel" : "csv"), "main");
	}
}

$page = new OverviewPage($this, "overview");

?>
