<?php

require_once "library/page.php";
require_once "pages/all/all.php";


// ShippingPage
// ============================================
class ShippingPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $shipId;

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		$this->shipId = 0;
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit(){
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if(empty($this->post["shipId"])) return;
		$this->shipId = $this->post["shipId"];

		$res = false;
		if     ($this->post["do"]=="download") $res = $this->submitDownload();
		else if($this->post["do"]=="export"  ) $res = $this->submitExport  ();
		else if($this->post["do"]=="paper"   ) $res = $this->submitPaper   ();

		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}


	
	// private members and methods
	// ======================================== 

	// loadPage
	// ---------------------------------------- 
	private function loadPage(){
		/* Builds the page */

		// header
		$allSites = $this->master->getOptionsSites();	
		$cs = new DbConfig($this->master, "lastshipment");
		$cs->columns = array("shipmentintid", "shippingfrom", "shippingdestination");
		$cs->order("createtime", "desc");
		$cs->limit(1);
		$shipment = $this->db->read("shippinghistory", $cs);
		$this->html->set("lastShipId"  ,           $shipment->shipmentintid       );
		$this->html->set("lastShipFrom", $allSites[$shipment->shippingfrom       ]);
		$this->html->set("lastShipTo"  , $allSites[$shipment->shippingdestination]);

		$this->html->set("shipId"      , !empty($shipId) ? $this->shipId : 0);

		// body
		$this->loadShipment();

		// template
		$tpl= $this->html->template("shipping");
		return $tpl;
	}

	// loadShipment
	// ---------------------------------------- 
	private function loadShipment(){
		/* Loads the shipment */

		if(empty($this->shipId)) return;

		// database query
		$c = new DbConfig($this->master, "shipment");
		$c->columns = array("sh.id_shippinghistory", "sh.eqentryid", "sh.shippingfrom", 
		                    "sh.shippingdestination", "sh.shippingperson", "sh.shippingcompany", 
		                    "sh.receivingdate", "sh.receivingperson", "sh.shipmentintid", 
		                    "sh.leavingshipref", "sh.returningshipref", "sh.shippingdate", 
		                    "e.partsbatchmtfid", "e.otherid", "e.quantity", "et.eqtypename");
		$c->joinon  = "sh.eqentryid = e.id_equipment";
		$c->joinon  = "e.eqtypecodeid = et.id_equipmenttypes";
		$c->select("sh.shipmentintid", $this->shipId);
		$c->order("sh.createtime");
		$c->order("sh.eqentryid" );
		$this->db->read("shipment", $c);

		$shipments = array();
		foreach($this->db->shipment as $row){
			array_push($shipments, $this->html->template("shipping_row_item", array("shipId" =>$row->sh_shipmentintid , 
			                                                                        "mtfId"  =>$row->e_partsbatchmtfid, 
			                                                                        "otherId"=>$row->e_otherid        , 
			                                                                        "eqId"   =>$row->sh_eqentryid     , 
			                                                                        "eqType" =>$row->et_eqtypename    ))); 
		}

		if(count($shipments)==0){
			//array_push($shipments, "<td class='center fault bold' colspan='5'>Shipment-ID does not exist!</td>");
			$this->vb->error("This shipment ID does not exist!");
			return;
		} 
		
		$this->html->set("shipments", implode("", $shipments));

		$sites = $this->master->getOptionsSites();
		foreach($this->db->shipment as $row){
			$this->html->set("shipLoc"    , $sites[$row->sh_shippingfrom]);
			$this->html->set("shipBy"     , $row->sh_shippingperson  );
			$this->html->set("shipDate"   , $row->sh_shippingdate    );
			$this->html->set("shipComp"   , $row->sh_shippingcompany );
			$this->html->set("shipRef"    , $row->sh_leavingshipref  );
			$this->html->set("receiveLoc" , $sites[$row->sh_shippingdestination]);
			$this->html->set("receiveBy"  , $row->sh_receivingperson );
			$this->html->set("receiveDate", $row->sh_receivingdate   );
			$this->html->set("receiveRef" , $row->sh_returningshipref);
			break;
		}


		// Comments
		$cv = new DbConfig($this->master, "shipcomms");
		$cv->joinon  = "ec.shipid = sh.id_shippinghistory";
		$cv->columns = array("ec.eqcomment", "eventdatedb", "ec.websiteusercr");
		$cv->reformat("TO_CHAR(EC.EVENTDATE, 'YYYY-MM-DD HH24:MI')", "eventdatedb");
		$cv->select("sh.shipmentintid", $this->shipId);
		$cv->select("ec.eqcomment"    , "not null"   );
		$cv->group ("ec.eqcomment"    );
		$cv->group ("ec.eventdate"    );
		$cv->group ("ec.websiteusercr");
		$this->db->read("shipcomms", $cv);

		$comms = array();
		foreach($this->db->shipcomms as $row)
			array_push($comms, $this->html->template("shipping_row_eqcomm", array("eqcomm"=>$row->ec_eqcomment    , 
			                                                                      "date"  =>$row->eventdatedb     ,
			                                                                      "user"  =>$row->ec_websiteusercr)));
		$disp="block";
		if(count($comms)==0) {
			$this->html->set("shipcomm", "none");
			$disp="none";
		}
		$this->html->set("shipdisp"  , $disp);
		$this->html->set("eqcomments", implode("", $comms));


		// Documents
		$view = $this->db->readView("shipdocs", array("dl.docid = d.id_documents",
		                                              "dl.shipid = sh.id_shippinghistory"),
		                                        array("dl.docid", "d.typeofdoc", "d.fileformat", "d.docname"),
		                                        array("sh.shipmentintid"=>$this->shipId,
		                                              "dl.docid"        =>"not null"));
		$docs = array();
		foreach($view as $row)
			array_push($docs, $this->html->template("shipping_row_doc", array("type"  =>$row->d_typeofdoc ,
			                                                                  "docId" =>$row->dl_docid    ,
			                                                                  "name"  =>$row->d_docname   ,
			                                                                  "format"=>$row->d_fileformat)));
		$disp="block";
		if(count($docs)==0) {
			$this->html->set("shipdocs", "none");
			$disp="none";
		}
		$this->html->set("docdisp"  , $disp);
		$this->html->set("documents", implode("", $docs));
	}    

	// submitDownload
	// ---------------------------------------- 
	private function submitDownload(){
		/* Downloads a single document */
		downloadDoc($this);
	}

	// submitExport
	// ---------------------------------------- 
	private function submitExport(){
		/* Export the shipping view */

		$path = $this->master->downloadPath."/shipping.txt";
		$f = fopen($path, "w");

		$c = new DbConfig($this->master, "shipment");
		$c->columns = array("sh.id_shippinghistory", "sh.eqentryid", "sh.shippingfrom", 
		                    "sh.shippingdestination", "sh.shippingperson", "sh.shippingcompany", 
		                    "sh.receivingdate", "sh.receivingperson", "sh.shipmentintid", 
		                    "sh.leavingshipref", "sh.returningshipref", "sh.shippingdate", 
		                    "e.partsbatchmtfid", "e.otherid", "e.quantity", "et.eqtypename");
		$c->joinon  = "sh.eqentryid = e.id_equipment";
		$c->joinon  = "e.eqtypecodeid = et.id_equipmenttypes";
		$c->select("sh.shipmentintid", $this->shipId);
		$c->order("sh.createtime");
		$c->order("sh.eqentryid" );
		$this->db->read("shipment", $c);

		foreach($this->db->shipment as $row){
			fwrite($f, "shipment-ID: ".$row->sh_shipmentintid."\n\n");
			fwrite($f, "shipping date: ".$row->sh_shippingdate."\n\n");
			if(!empty($row->sh_shippingcompany))
				fwrite($f, "service provider: ".$row->sh_shippingcompany."\n\n");
			break;
		}

		fwrite($f, "comments:\n");
		foreach($this->db->shipcomms as $row)
			fwrite($f, sprintf("%s %s %s\n", $row->ec_eqcomment, $row->ec_eventdate, $row->ec_websiteusercr));
		fwrite($f, "\n");

		fwrite($f, "parts/groups/batches:\n");
		fwrite($f, "MTF-ID | ALIAS | QUANTITY | DESCRIPTION\n");
		foreach($this->db->shipment as $row)
			fwrite($f, sprintf("%s | %s | %s | %s\n", $row->e_partsbatchmtfid, $row->e_otherid, !empty($row->e_otherid) ? $row->e_otherid : "1", $row->et_eqtypename));
		fclose($f);
		
		$this->vb->success(sprintf("Download is available <a href='%s' target='_blank' class='back'>here</a>.", $path));

		//header("Content-Type: text/plain");
		//header("Content-disposition: attachment; filename=test.txt");
		//readfile($path);
	}

	// submitPaper
	// ---------------------------------------- 
	private function submitPaper(){
		/* Attaches a paper to the currently selected shipment */

		$c = new DbConfig($this->master, "shipment");
		$c->columns = array("sh.id_shippinghistory", "sh.eqentryid", "sh.shippingfrom", 
		                    "sh.shippingdestination", "sh.shippingperson", "sh.shippingcompany", 
		                    "sh.receivingdate", "sh.receivingperson", "sh.shipmentintid", 
		                    "sh.leavingshipref", "sh.returningshipref", "sh.shippingdate", 
		                    "e.partsbatchmtfid", "e.otherid", "e.quantity", "et.eqtypename");
		$c->joinon  = "sh.eqentryid = e.id_equipment";
		$c->joinon  = "e.eqtypecodeid = et.id_equipmenttypes";
		$c->select("sh.shipmentintid", $this->shipId);
		$c->order("sh.createtime");
		$c->order("sh.eqentryid" );
		$this->db->read("shipment", $c);

		$shipids = array();
		foreach($this->db->shipment as $row)
			$shipids[$row->sh_eqentryid] = $row->sh_id_shippinghistory;
		
		return uploadDoc($this, "newPaper", "shipping", $this->post["eventDate"], array($this->db->shipment->sh_eqentryid), $shipids); 
	}
}


$page = new ShippingPage($this, "shipping");

?>
