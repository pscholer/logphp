<?php

require_once "library/page.php";
require_once "pages/all/splitting.php";
require_once "pages/all/equip.php";
require_once "pages/all/parenting.php";


// Groups Page
// ============================================
class GroupsPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $eqId         = NULL;
	public $mtfId        = NULL;
	public $otherId      = NULL;
	public $isAllowed    = false;
	public $mtfs         = array();
	public $others       = array();
	public $reachedLimit = false;

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->eqId    = NULL;
		$this->mtfId   = NULL;
		$this->otherId = NULL;
		$this->mtfs    = array();
		$this->others  = array();
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if(array_key_exists("eqId"   , $this->post)) $this->eqId    = $this->post["eqId"   ];
		if(array_key_exists("mtfId"  , $this->post)) $this->mtfId   = $this->post["mtfId"  ];
		if(array_key_exists("otherId", $this->post)) $this->otherId = $this->post["otherId"];

		$this->loadAlias();

		$res = false;
		if     ($this->post["do"]=="load"     ) equipSubmitLoad($this);
		else if($this->post["do"]=="range"    ) $this->submitRange();
		else if($this->post["do"]=="save"     ) $res = $this->submitSave();
		else if($this->post["do"]=="parenting") $res = $this->submitParenting();

		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}




	// private members and methods
	// ======================================== 

	// checkEquipment
	// ---------------------------------------- 
	private function checkEquipment($isForm=false){
		/* Checks if splitting of the group actually is allowed */

		if(empty($this->eqId)) return false;

		// this function is invoked at two locations: when building the form
		// and when submitting it; in the latter case, since the loadPage function
		// is called thereafter, this method would be invoked twice, hence, we
		// need a way to switch it off when loading the form in the submit "save" case
		if($isForm && array_key_exists("do", $this->post) && $this->post["do"]=="save") return true;

		// basic equipment info
		$eq = $this->db->readTable("equipment", array("eqtypecodeid", "quantity"), array("id_equipment"=>$this->eqId));
		if($eq->count()==0){
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) does not exist!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// if equipment is a group
		$et = $this->db->readTable("equipmenttypes", array("isbatchflag"), array("id_equipmenttypes"=>$eq->eqtypecodeid));
		if(!(!empty($eq->quantity) && $et->isbatchflag=="F")) {
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) is not a group!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// status info
		$statattr = $this->master->getEqStatus  ($this->eqId, array("s.statusname"));
		$locattr  = $this->master->getEqLocation($this->eqId, array("l.sitename"  ));
		if($statattr->count()<1){
			$this->vb->error(sprintf("Status information for the requested equipment entry (%d, %s, %s) could not be found!", 
			                                  $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		$allowedStat = $this->master->getOptionsStatus("priorsplittingflag");
		$allowedLoc  = $this->master->getOptionsSites ("priorsplittingflag");
		if(!in_array($statattr->s_statusname, $allowedStat) || !in_array($locattr->l_sitename, $allowedLoc)){
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) cannot be splitted due to its current status or location!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}
		return true;
	}

	// loadAddChild
	// ---------------------------------------- 
	private function loadAddChild(){
		/* Form for adding children to the items */

		$bh = $this->db->readView("newitems", array("bh.eqentryid = e.id_equipment"),
		                                      array("e.id_equipment", "e.partsbatchmtfid", "e.otherid"),
		                                      array("bh.parentbatcheqentryid"=>$this->eqId));
		$rows = array();
		foreach($bh as $row)
			array_push($rows, $this->html->template("groups_created_row", array("rowEqId"   =>$row->e_id_equipment, 
			                                                                    "rowMtfId"  =>$row->e_partsbatchmtfid,
			                                                                    "rowOtherId"=>$row->e_otherid)));

		$this->html->set("heritageED", $this->html->template("eventDate", array("title"=>"EVENT DATE", "name"=>"parEventDate", "addd"=>"", "addl"=>"invisdesc"), NULL, "all"));
		$body =  $this->html->template("groups_created" , array("rows"=>implode($rows)));
		$body .= $this->html->template("groups_addChild", array("eqId"=>$this->eqId  ));
		$this->html->set("body", $body);
	}

	// loadAlias
	// ---------------------------------------- 
	private function loadAlias(){
		/* Retrieves the MTFs and OtherIds from the buffer */
		if(!isset($this->post["mtfs"]) || !isset($this->post["others"])) return;
		$this->mtfs   = explode(",", $this->post["mtfs"  ]);
		$this->others = explode(",", $this->post["others"]);
	}

	// loadForm
	// ---------------------------------------- 
	private function loadForm() {
		/* Loads the information for the equipment entry and stores it in the HTML handler */

		// basic checks
		if(!$this->checkEquipment(true)) return;

		// basic equipment info
		$equipment = $this->db->readTable("equipment", array("*"),
		                                               array("id_equipment"=>$this->eqId));
		$et = $this->db->readTable("equipmenttypes", array("*"), array("id_equipmenttypes"=>$equipment->eqtypecodeid));

		$this->html->set("eqTypeName"  , $et       ->eqtypename  );
		$this->html->set("subBatchId"  , $equipment->subbatchid  );
		$this->html->set("quantity"    , $equipment->quantity    );
		$this->html->set("goodQuantity", $equipment->goodquantity);
		$this->html->set("quantityUnit", $et       ->quantityunit);

		//if(!isset($this->post["rangeOtherId"]))
		//	$this->html->set("rangeOtherId", $equipment->otherid);


		// alias stuff
		$rangeMax      = 99999;
		$rangeMaxQuant = $rangeMax - $equipment->quantity;
		$inClass       = !empty($this->otherId) ? "light" : "";
		if(!isset($this->post["rangeFrom"  ])) $this->html->set("rangeFrom"  , 1                   );
		if(!isset($this->post["rangeTo"    ])) $this->html->set("rangeTo"    , $equipment->quantity);
		if(!isset($this->post["rangeDigits"])) $this->html->set("rangeDigits", 5                   );
		$this->html->set("rangeMax"     , $rangeMax     );
		$this->html->set("rangeMaxQuant", $rangeMaxQuant);
		$this->html->set("inClass"      , $inClass      );

		// in case it is available, prepare the prospective other Ids
		if(count($this->mtfs)>0){
			$rows = array();
			for($i=0; $i<count($this->mtfs); ++$i)
				array_push($rows, $this->html->template("groups_body_aliasRange_row", array("rowMtfId"=>$this->mtfs[$i], "rowOtherId"=>$this->others[$i])));

			$this->html->set("mtfs"      , implode(",", $this->mtfs  ));
			$this->html->set("others"    , implode(",", $this->others));
			$this->html->set("rows"      , implode("" , $rows        ));
			$this->html->set("aliasRange", $this->html->template("groups_body_aliasRange"));
		}

		// splitting independently
		$this->html->set("check_splitIndep" , isset($this->post["splitIndep" ]) ? "checked" : "");

		// ------ the template ------
		$this->html->set("body", $this->html->template("groups_body"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Generates the form and fills it with the information of the selected 
		* equipment entry (if so, i.e. if $this->eqid is not NULL), otherwise the
		* form will be simply empty */

		if(isset($this->post["do"]) && in_array($this->post["do"], array("save", "parenting"))) {
			$this->loadAddChild();
			return $this->html->template("groups");
		}

		equipLoadPage($this, "SPLIT GROUP<br />OF ITEMS", "if(checkAliasRange()){submitIt('save')}");
		$this->loadForm();
		return $this->html->template("groups");
	}

	// submitRange
	// ---------------------------------------- 
	private function submitRange() {
		/* Finding a free OtherId range */

		//if(empty($this->post["rangeOtherId"])) return;

		// defaults
		if(empty($this->post["rangeFrom"  ])) $this->post["rangeFrom"  ] = 1;
		if(empty($this->post["rangeTo"    ])) $this->post["rangeTo"    ] = $this->post["quantity"];
		if(empty($this->post["rangeDigits"])) $this->post["rangeDigits"] = 5;

		// prepare for MTF
		$half    = substr($this->mtfId, 0, -4); // everything but the last 4 digits
		$eqType  = substr($this->mtfId, 5,  4);
		$highest = $this->db->readTable("equipmenttypes", array("highestentry"), array("eqtypecode"=>$eqType))->highestentry;

		// check if required range is free
		$this->mtfs   = array();
		$this->others = array();
		$digits       = max(strlen((string) $this->post["rangeFrom"]), strlen((string) $this->post["rangeTo"]), $this->post["rangeDigits"]);
		$isFree       = true;
		$j            = 1;
		for($i=$this->post["rangeFrom"];$i<=$this->post["rangeTo"];++$i){
			if(!empty($this->post["rangeOtherId"])){
				$otherId = $this->post["rangeOtherId"].str_pad(strval($i), $digits, "0", STR_PAD_LEFT);
				$eq1     = $this->db->readTable("equipment", array("id_equipment"), array("otherid"=>$otherId));
				if($eq1->count()!=0){
					$isFree = false; 
					break;
				}
				array_push($this->others, $otherId);
			}
			else
				array_push($this->others, NULL); // for consistency: every mtfid gets an entry in others
			$mtfId = $half.str_pad($highest+$j, 4, "0", STR_PAD_LEFT);
			array_push($this->mtfs, $half.str_pad($highest+$j, 4, "0", STR_PAD_LEFT));
			++$j;
		}

		if(!$isFree){
			$this->vb->error("The chosen Eq Alias range is not available!");
			return false;
		}
	}

	// submitSave
	// ---------------------------------------- 
	private function submitSave() {
		/* Splitting the group step 1: core data */

		// basic checks
		if(!$this->checkEquipment()) return false;

		// check if alias range has been selected
		if(count($this->mtfs)==0){
			$half    = substr($this->mtfId, 0, -4); // everything but the last 4 digits
			$eqType  = substr($this->mtfId, 5,  4);
			$highest = $this->db->readTable("equipmenttypes", array("highestentry"), array("eqtypecode"=>$eqType))->highestentry;
		}

		// preparing variables
		$eventDateDb  = isValidDate($this->post["eventDate"]) ? dbStringDate($this->post["eventDate"]) : $this->master->eventDateDb;
		$equipment    = $this->db->readTable("equipment", array("*"), array("id_equipment"=>$this->eqId));
		$quantity     = $equipment->quantity;
		$eqtypecodeid = $equipment->eqtypecodeid;
		$ncf          = $equipment->isnonconfflag;

		// it is a feature of Oracle that nested selects should be done before the inserts
		$locattr       = NULL;
		$statattr      = NULL;
		$statloc       = NULL;
		$parequipment  = NULL;
		$pareqdetails  = NULL;
		$pareqcomments = NULL;
		$pardoclinks   = NULL;

		if(!isset($this->post["splitIndep"]) || $this->post["splitIndep"]!=1){
			$locattr  = $this->master->getEqLocation($this->eqId, array("sl.statusid", "sl.minorlocdesc", "sl.isvalidflag", "eventdate"));
			$statattr = $this->master->getEqStatus  ($this->eqId, array("sl.isvalidflag", "eventdate"));
		}

		else {
			$statloc       = $this->db->readTable("statuslocation", array("statusid", "majorlocid", "minorlocdesc", "isvalidflag", "websiteusercr", "eventdatedb"),
			                                                  array("eqentryid"=>$this->eqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"), array("eventdate"=>"asc"));
			$parequipment  = $this->db->readTable("equipment"       , array("*"), array("id_equipment"=>$this->eqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"));
			$pareqdetails  = $this->db->readTable("equipmentdetails", array("*"), array("eqentryid"   =>$this->eqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb",
			                                                                                                             "TO_CHAR(manufacturingdate, 'YYYY-MM-DD HH24:MI:SS')"=>"manufacturingdatedb",
			                                                                                                             "TO_CHAR(deliverydate     , 'YYYY-MM-DD HH24:MI:SS')"=>"deliverydatedb",
			                                                                                                             "TO_CHAR(ordereddate      , 'YYYY-MM-DD HH24:MI:SS')"=>"ordereddatedb"));
			$pareqcomments = $this->db->readTable("eqcomments"      , array("*"), array("eqentryid"   =>$this->eqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"));
			$pardoclinks   = $this->db->readTable("doclink"         , array("*"), array("eqentryid"   =>$this->eqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"));
		}

		// for as many quantities there are, create dedicated items
		$items   = array();
		$details = array();
		for($i=0; $i<$quantity; ++$i){
		
			// creating new equipment entry for the item
			if(count($this->mtfs)>0) $newMtfId = $this->mtfs[$i];
			else                     $newMtfId = $half.str_pad($highest+$i+1, 4, "0", STR_PAD_LEFT);

			$insert = array("partsbatchmtfid"=>$newMtfId,
			                "eqtypecodeid"   =>$eqtypecodeid, 
			                "isnonconfflag"  =>$ncf,
			                "websiteusercr"  =>$this->globals["username"],
			                "websiteusered"  =>$this->globals["username"],
			                "eventdate"      =>$eventDateDb);
			if(count($this->others)>0 && !empty($this->others[$i])) $insert["otherid"] = $this->others[$i];
			$equipment->append($insert);
			$equipment->push();

			if($this->db->error()){
				$this->vb->error("Could not create equipment entry for splitted item!", true);
				return false;
			}
	
			$eqId    = $equipment->id_equipment; // EQ ID of the newly created item, do NOT confuse with $this->eqId of the group!
			$mtfId   = $equipment->partsbatchmtfid;
			$otherId = $equipment->otherid;	

			// create orphanage entry
			$this->db->parenting->append(array("eqentryid"      =>$eqId,
			                                   "parenteqentryid"=>$this->master->config->eqIdOrph->value,
			                                   "isactiveflag"   =>"T",
			                                   "websiteusercr"  =>$this->globals["username"],
			                                   "websiteusered"  =>$this->globals["username"],
			                                   "eventdate"      =>$eventDateDb));
			$this->db->parenting->push();
			if($this->db->error()){
				$this->vb->error("Could not create orphanage entry for splitted item!", true);
				return false;
			}

			array_push($items  , $eqId);
			array_push($details, array($eqId, $mtfId, $otherId));

		}
		$this->html->set("items", implode(",", $items));


		// normal splitting (NOT independent)
		if(!isset($this->post["splitIndep"]) || $this->post["splitIndep"]!=1){
			if(!splittingNormal($this, $this->eqId, $items, $eventDateDb, $locattr, $statattr)){
				$this->vb->error("Could not copy data to new equipment items!", true);
				return false;
			}
		}	

		// independent splitting!
		else {
			if(!splittingIndependent($this, $this->eqId, $items, $statloc, $parequipment, $pareqdetails, $pareqcomments, $pardoclinks)){
				$this->vb->error("Could not copy data to new equipment items!", true);
				return false;
			}
		}


		// update highest entry
		$eqType  = substr($this->mtfId, 5,  4);
		$eqTypes = $this->db->readTable("equipmenttypes", array("id_equipmenttypes", "highestentry"), 
		                                                  array("eqtypecode"=>$eqType));
		$eqTypes->highestentry += count($items);
		$eqTypes->push();
		if($this->db->error()){
			$this->vb->error("Could not update highest entry value of the equipment type!", true);
			return false;
		}

			
		// insert fully split up status
		$statIdFullySplitUp = array_search("fully split up", $this->master->getOptionsStatus());
		$locIdUndefined     = array_search("undefined"     , $this->master->getOptionsSites ());
		$this->db->statuslocation->append(array("eqentryid"     => $this->eqId,
		                                        "statusid"      => $statIdFullySplitUp,
		                                        "majorlocid"    => $locIdUndefined,
		                                        "minolocdesc"   => "",
			                                    "websiteusercr" => $this->globals["username"],
			                                    "websiteusered" => $this->globals["username"],
			                                    "eventdate"     => $eventDateDb));
		$this->db->statuslocation->push();
		if($this->db->error()){
			$this->vb->error(sprintf("Could not insert status for entry EQ ID = %d!", $this->eqId), true);
			return false;
		}


		// if there has been an error, set isactiveflag to false for all parenting entries
		$parenting = $this->db->readTable("parenting", array("id_parenting", "isactiveflag"),
		                                               array("eqentryid"      =>$this->eqId, 
		                                                     "parenteqentryid"=>$this->master->config->eqIdOrph->value,
		                                                     "isactiveflag"   =>"T"));
		if($parenting->count()==1){ 
			foreach($parenting as $row) $row->isactiveflag="F";
		}
		$parenting->push();	
		if($this->db->error()){
			$this->vb->error(sprintf("Could not update isactiveflag for entry EQ ID = %d!", $this->eqId), true);
			return false;
		}


		// success
		//for($i=0;$i<count($details);++$i){
		//	$this->vb->success(sprintf("Unit No. <b>%d</b> registered as unique EQ ID <b>%d</b> with MTF-No <b>%s</b> and EQ Alias <b>%s</b>!", $i+1, $details[$i][0], $details[$i][1], $details[$i][2]));
		//}
		$this->vb->success(sprintf("Successfully created %d items from group EQ ID = %d!", count($details), $this->eqId));
		return true;
	}


	// submitParenting
	// ---------------------------------------- 
	private function submitParenting() {
		/* Splitting the group step 2: parenting */
	
		if(empty($this->post["items"]) || empty($this->post["idChild"])) return false;

		$items = explode(",", $this->post["items"]);
	
		$idsChild  = equipGetIds($this, $this->post["typeIdChild"], $this->post["idChild"], $this->post["idChild"], $this->post["idChildSub"], $this->post["idChild"]);
		$childEqId = $idsChild[0];
		$container = array("idChild"      =>$childEqId, 
		                   "numberOfLinks"=>$this->post["parNum"]>0 ? $this->post["parNum"] : 1,
		                   "parEventDate" =>$this->post["parEventDate"],
		                   "isActiveFlag" =>"T",
		                   "parService"   =>$this->post["parService"],
		                   "parPosition"  =>$this->post["parPosition"]);

		// check if parenting allowed for all entries before doing it
		$this->reachedLimit = false;
		$successful = true;
		$i          = 0;
		foreach($items as $eqId){
			$container["prevLinks"] = $i;
			$container["idParent" ] = $eqId;
			if(!parentingCheck($this, $container)){
				$successful = false;
				break;
			}
			++$i;
		}
		if(!$successful){
			$this->vb->error("Parenting of this item to at least one of the splitted equipments is not allowed! Aborting..");
			return false;
		}

		// do parenting for all entries
		foreach($items as $eqId){
			$container["prevLinks"] = 0; // now we're doing it
			$container["idParent" ] = $eqId;
			if(!parentingDoIt($this, $container)){
				$successful = false;
				break;
			}
		}

		$this->db->parenting->push();
		if($this->db->error() || !$successful){
			$this->vb->error("Could not create parenting entries for splitted items!", true);
			return false;
		}

		$this->vb->success(sprintf("Successfully parented child EQ ID = %d to %d equipment entries!", $childEqId, count($items)));
		return true;
	}

}

$page = new GroupsPage($this, "groups");
		

?>
