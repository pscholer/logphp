<?php

require_once "library/page.php";
require_once "pages/all/all.php";
require_once "pages/all/equip.php";
require_once "pages/all/splitting.php";



// Batches Page
// ============================================
class BatchesPage extends Page {



	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $eqId    = NULL;
	public $mtfId   = NULL;
	public $otherId = NULL;

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->eqId    = NULL;
		$this->mtfId   = NULL;
		$this->otherId = NULL;
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if(array_key_exists("eqId"   , $this->post)) $this->eqId    = $this->post["eqId"   ];
		if(array_key_exists("mtfId"  , $this->post)) $this->mtfId   = $this->post["mtfId"  ];
		if(array_key_exists("otherId", $this->post)) $this->otherId = $this->post["otherId"];

		$res = false;
		if     ($this->post["do"]=="load") equipSubmitLoad($this);
		else if($this->post["do"]=="save") $res = $this->submitSave();

		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}



	// private members and methods
	// ======================================== 

	// checkEquipment
	// ---------------------------------------- 
	private function checkEquipment($isForm=false) {
		/* Loads the information for the equipment entry and stores it in the HTML handler */

		if(empty($this->eqId)) return false;

		// this function is invoked at two locations: when building the form
		// and when submitting it; in the latter case, since the loadPage function
		// is called thereafter, this method would be invoked twice, hence, we
		// need a way to switch it off when loading the form in the submit "save" case
		if($isForm && array_key_exists("do", $this->post) && $this->post["do"]=="save") return true;

		// check if equipment exists
		$equipment = $this->db->readTable("equipment", array("id_equipment"), 
		                                               array("id_equipment"=>$this->eqId));
		if($equipment->count()!=1) { 
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) does not exist!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// check if subbatches exist
		$equipment = $this->db->readTable("equipment", array("id_equipment"), 
		                                               array("partsbatchmtfid"=>$this->mtfId, "subbatchid"=>"not null"));
		if($equipment->count()>0) { 
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) already has subbatches!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// eq type info
		$eqtypeatts = $this->db->readView("eqtypeatts", array("e.eqtypecodeid = et.id_equipmenttypes"),
		                                                array("et.isbatchflag"), 
		                                                array("e.id_equipment"=>$this->eqId));

		if($eqtypeatts->et_isbatchflag!="T"){
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) is not a batch!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// status info
		$statattr = $this->master->getEqStatus  ($this->eqId, array("s.statusname"));
		$locattr  = $this->master->getEqLocation($this->eqId, array("l.sitename"  ));
		if($statattr->count()<1){
			$this->vb->error(sprintf("Status information for the requested equipment entry (%d, %s, %s) could not be found!", 
			                                  $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		$allowedStat = $this->master->getOptionsStatus("priorsplittingflag");
		$allowedLoc  = $this->master->getOptionsSites ("priorsplittingflag");
		if(!in_array($statattr->s_statusname, $allowedStat) || !in_array($locattr->l_sitename, $allowedLoc)){
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) cannot be splitted due to its current status or location!", $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		return true;
	}

	// loadEquipment
	// ---------------------------------------- 
	private function loadEquipment() {
		/* Loads the information for the equipment entry and stores it in the HTML handler */

		// basic checks
		if(!$this->checkEquipment(true)) return;

		// basic equipment info
		$equipment    = $this->db->readTable("equipment", array("*"), array("id_equipment"=>$this->eqId));
		$isncf        = !empty($equipment->isnonconfflag) ? $equipment->isnonconfflag : "X";
		$quantity     = $equipment->quantity;
		$goodQuantity = $equipment->goodquantity;

		$this->html->set("subBatchId"   , $equipment->subbatchid   );
		$this->html->set("isNcf"        , $isncf                   );
		$this->html->set("eqTypeCodeId" , $equipment->eqtypecodeid );
		$this->html->set("quantity"     , $quantity                );
		$this->html->set("goodQuantity" , $goodQuantity            );
		$this->html->set("edittime"     , $equipment->edittime     );
		$this->html->set("websiteusered", $equipment->websiteusered);
		$this->html->set("createTime"   , substr($equipment->createtime, 0, 10));
		$this->html->set("createUser"   , $equipment->websiteusercr);

		// eq type info
		$ct = new DbConfig($this->master, "eqtype");
		$ct->columns = array("et.quantityunit", "et.eqtypename", "et.isbatchflag", "et.alterneqtype");
		$ct->joinon  = "e.eqtypecodeid = et.id_equipmenttypes";
		$ct->select("e.id_equipment", $this->eqId);
		$this->db->read("eqtypeatts", $ct);
		$eqtypeatts = $this->db->eqtypeatts;

		$this->html->set("quantityUnit", $eqtypeatts->et_quantityunit);
		$this->html->set("eqTypeName"  , $eqtypeatts->et_eqtypename  );
		$this->html->set("isBatchFlag" , $eqtypeatts->et_isbatchflag );
		//$this->html->set("alternEqType", $eqtypeatts->et_alterneqtype);


		// quantity
		if($quantity>0){
			$this->html->set("maxSubbatches"     , min(9, $quantity));
			$this->html->set("quantitySubbatches", isset($this->post["quantitySubbatches"]) ? $this->post["quantitySubbatches"] : 2);
			$this->html->set("quantreqsubbatch"  , $this->html->template("batches_body_reqsubbatch1"));
		}
		else {
			$this->html->set("batch"           , $equipment->subbatchid>0 ? "SubBatch" : "Batch");
			$this->html->set("quantreqsubbatch", $this->html->template("batches_body_reqsubbatch0")  );
		}


		// units per subbatch
		$this->loadSubbatches();


		// ------ the template ------
		$this->html->set("body", $this->html->template("batches_body"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Generates the form and fills it with the information of the selected 
		* equipment entry (if so, i.e. if $this->eqid is not NULL), otherwise the
		* form will be simply empty */

		equipLoadPage($this, "CREATE<br />SUBBATCHES", "if(isfullysplitup()){submitIt('save')}");
		$this->loadEquipment();
		return $this->html->template("batches");
	}

	// loadSubbatches
	// ---------------------------------------- 
	private function loadSubbatches() {
		/* Loading the form to define the quantities; actually after POST submission
		but part of the form, thus denoted as "load..." not "submit..." */

		if(!isset($this->post["quantitySubbatches"]) || $this->post["quantitySubbatches"]<=0) return;
		$this->html->set("quantitySubbatches", $this->post["quantitySubbatches"]);

		$equipment    = $this->db->equipment;
		$eqtypeatts   = $this->db->eqtypeatts;
		$quantity     = $equipment->quantity;
		$goodQuantity = $equipment->goodquantity;
		$quantityUnit = $eqtypeatts->et_quantityunit;

		// form with row per subbatch
		$entries   = array();
		$units     = array();
		$goodUnits = array();
		for($idx=0; $idx<$this->post["quantitySubbatches"]; ++$idx){
			$vars = array();
			array_push($units    , isset($this->post["units"    ][$idx]) ? $this->post["units"    ][$idx] : 1);
			array_push($goodUnits, isset($this->post["goodUnits"][$idx]) ? $this->post["goodUnits"][$idx] : 0);
			$vars["idx"     ] = strval($idx);
			$vars["unit"    ] = end($units    );
			$vars["goodUnit"] = end($goodUnits);
			array_push($entries, $this->html->template("batches_body_quantities_row", $vars));
		}
		$this->html->set("entries"   , implode("", $entries));
		$this->html->set("quantities", $this->html->template("batches_body_quantities"));

		// end part of the form
		$divUnits      = array_sum($units);
		$divGoodUnits  = array_sum($goodUnits);
		$leftUnits     = $quantity    -$divUnits;
		$leftGoodUnits = $goodQuantity-$divGoodUnits;       

		$this->html->set("leftUnits"    , $leftUnits    );
		$this->html->set("leftGoodUnits", $leftGoodUnits);

		$vars = array();
		$vars["divUnits"      ] = $divUnits;
		$vars["divGoodUnits"  ] = $divGoodUnits;
		$vars["subbatchclass1"] = "";
		$vars["subbatchclass2"] = "";
		$base1 = "There %s still %d %s left in the batch to be assigned!";
		$base0 = "There are no %s left in the batch! GOOD!";
		if($leftUnits==1)
			$vars["subbatchmsg1"  ] = sprintf($base1, "is" , 1         , "Unit");
		else if($leftUnits>1)
			$vars["subbatchmsg1"  ] = sprintf($base1, "are", $leftUnits, "Units");
		else {
			$vars["subbatchmsg1"  ] = sprintf($base0, "Units"                   );
			$vars["subbatchclass1"] = "batchesFullysplit";
		}

		if($leftGoodUnits==1)
			$vars["subbatchmsg2"  ] = sprintf($base1, "is" , 1         , "Good Unit");
		else if($leftGoodUnits>1)
			$vars["subbatchmsg2"  ] = sprintf($base1, "are", $leftUnits, "Good Units");
		else {
			$vars["subbatchmsg2"  ] = sprintf($base0, "Good Units"                   );
			$vars["subbatchclass2"] = "batchesFullysplit";
		}
		$this->html->set("end", $this->html->template("batches_body_end", $vars));
	}

	// submitSave
	// ---------------------------------------- 
	private function submitSave() {
		/* Insert the subbatches and create new status entries */

		// basic checks
		if(!$this->checkEquipment()) return false;

		$eventDate = isValidDate($this->post["eventDate"]) ? dbStringDate($this->post["eventDate"]) : $this->master->eventDateDb;

		$statattr  = $this->master->getEqStatus  ($this->post["eqId"], array("eventdate"));
		$locattr   = $this->master->getEqLocation($this->post["eqId"], array("sl.statusid", "sl.minorlocdesc", "eventdate"));

		// it is a feature of Oracle that nested selects should be done before the inserts
		$locattr       = NULL;
		$statattr      = NULL;
		$statloc       = NULL;
		$parequipment  = NULL;
		$pareqdetails  = NULL;
		$pareqcomments = NULL;
		$pardoclinks   = NULL;

		$equipment = $this->db->equipment;
		$parenting = $this->db->parenting;
		$statloc   = $this->db->statuslocation;
		$heritage  = $this->db->batchheritage;

		$units     = array();
		$goodUnits = array();
		$items     = array();
		$details   = array();
		for($idx=0; $idx<$this->post["quantitySubbatches"]; ++$idx){

			$unitNo = $idx+1;	
			if(isset($this->post["units"    ][$idx])) array_push($units    , $this->post["units"    ][$idx]);
			if(isset($this->post["goodUnits"][$idx])) array_push($goodUnits, $this->post["goodUnits"][$idx]);

			// equipment entry
			$insert = array(//"id_equipment"    => "incONE_EQUIPMENT.nextval",
							"partsbatchmtfid" => $this->post["mtfId"        ],
			                "subbatchid"      => "incONE_SUBBATCHES.nextval",
			                "otherid"         => NULL,
			                "eqtypecodeid"    => $this->post["eqTypeCodeId" ],
			                "quantity"        => $this->post["units"    ][$idx],
			                "goodquantity"    => $this->post["goodUnits"][$idx],
			                "websiteusercr"   => $this->globals["username"],
			                "websiteusered"   => $this->globals["username"],
			                "eventdate"       => $eventDate);
			if($this->post["isNcf"]!="X") $insert["isnonconfflag"] = $this->post["isNcf"];
			$equipment->append($insert);
			$equipment->push();
			if($this->db->error()){
				$this->vb->error(sprintf("Could not create new subbatch %d to batch %s!", $unitNo, $this->post["mtfId"]), true);
				return false;
			}

			$eqId = $equipment->id_equipment; // latest eqId, do NOT confuse with $this->post["eqId"] which is the parent!
			$sbId = $equipment->subbatchid  ; // latest subbatchId

			// update other id (only possible if you know the new subbatchId
			if(!empty($this->post["otherId"])){
				$newOtherId = sprintf("%s_%d", $this->post["otherId"], $sbId);
				$equipment->otherid = $newOtherId;
				$equipment->push();
			}

			// parenting
			$parenting->append(array("eqentryid"       => $eqId,
			                         "parenteqentryid" => $this->master->config->eqIdOrph->value,
			                         "isactiveflag"    => "T", 
			                         "websiteusercr"   => $this->globals["username"],
			                         "websiteusered"   => $this->globals["username"],
			                         "eventdate"       => $eventDate));
			$parenting->push();
			if($this->db->error()){
				$this->vb->error(sprintf("Could not create orphanage entry for new subbatch %d with EQ ID = %d!", $unitNo, $eqId), true);
				return false;
			}

			array_push($items  , $eqId);
			array_push($details, array($eqId, $sbId));
		}

		// copy remaining bits and pieces
		if(!splittingNormal($this, $this->post["eqId"], $items, $eventDate, $locattr, $statattr)){
			$this->vb->error("Could not copy data for new equipment items!", true);
			return false;
		}

		// report upon the awesomeness of your endeavor
		for($i=0;$i<count($details);++$i){
			$this->vb->success(sprintf("<b>Subbatch No. %d</b> was created with EQ ID = %d, MTF-No. = %s and Subbatch ID = %s, and Alias = %s", $i+1, $details[$i][0], $this->post["mtfId"], $details[$i][1], $this->post["otherId"]));
		} 


		// check again if batch is fully split up
		$leftUnits          = ((int) $this->post["quantity"]    )-array_sum($units);
		$leftGoodUnits      = ((int) $this->post["goodQuantity"])-array_sum($goodUnits);
		$statIdFullySplitUp = array_search("fully split up", $this->master->getOptionsStatus());
		$locIdUndefined     = array_search("undefined"     , $this->master->getOptionsSites ());

		if($leftUnits!=0 || $leftGoodUnits!=0)
			$this->vb->error("Batch is not fully split up!");

		
		// insert fully split up status
		$statloc->append(array("eqentryid"     => $this->post["eqId"],
		                       "statusid"      => $statIdFullySplitUp,
		                       "majorlocid"    => $locIdUndefined,
		                       "minolocdesc"   => "",
			                   "websiteusercr" => $this->globals["username"],
			                   "websiteusered" => $this->globals["username"],
			                   "eventdate"     => $eventDate));
		$statloc->push();
		if($this->db->error()){
			$this->vb->error(sprintf("Could not insert status for entry EQ ID = %d!", $this->post["eqId"]), true);
			return false;
		}


		// if there has been an error, set isactiveflag to false for all parenting entries
		$parenting = $this->db->readTable("parenting", array("id_parenting", "isactiveflag"),
		                                               array("eqentryid"=>$this->post["eqId"], "parenteqentryid"=>$this->master->config->eqIdOrph->value));
		if($parenting->count()==1){ 
			foreach($parenting as $row) $row->isactiveflag="F";
		}
		$parenting->push();	
		if($this->db->error()){
			$this->vb->error(sprintf("Could not update isactiveflag for entry EQ ID = %d!", $this->post["eqId"]), true);
			return false;
		}


		// success
		$this->vb->success(sprintf("Successfully created %d subbatches to entry EQ ID = %d!", count($details), $this->post["eqId"]));
		return true;
	}

}

$page = new BatchesPage($this, "batches");

?>
