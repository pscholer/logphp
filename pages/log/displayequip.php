<?php

require_once "library/page.php";
require_once "pages/all/all.php";
require_once "pages/all/equip.php";
require_once "pages/all/parenting.php";


// HistTableEntry
// ============================================
class HistTableEntry {

	// members
	// ---------------------------------------- 
	public $statOpts    = array();
	public $locOpts     = array();
	public $direct      = false;
	public $check       = "";

	public $shipId      = "";
	public $sentFrom    = "";
	public $sentTo      = "";
	public $statusId    = 0;
	public $locId       = 0;
	public $status      = "";
	public $location    = "";
	public $eventdatedb = "";
	public $date        = "";
	public $user        = "";
	public $created     = "";

	// __construct
	// ---------------------------------------- 
	public function __construct($statOpts, $locOpts, $status=NULL){
		/* Add status */
		$this->statOpts = $statOpts;
		$this->locOpts  = $locOpts;
		if(empty($status)) return;
		$this->direct      = $status->iseqflag=='T';
		$this->check       = $this->direct ? "<center><input type='checkbox' name='idstats[]' value='".strval($status->id_statuslocation)."'></center>" : "";
		$this->shipId      = 0;
		$this->sentFrom    = "";
		$this->sentTo      = "";
		$this->statusId    = $status->statusid;
		$this->locId       = $status->majorlocid;
		$this->status      = array_key_exists($status->statusid  , $statOpts) ? $statOpts[$status->statusid]  : "";
		$this->location    = array_key_exists($status->majorlocid, $locOpts ) ? $locOpts[$status->majorlocid] : "";
		$this->eventdatedb = $status->eventdatedb;
		$this->date        = timestamp(2, $status->eventdatedb );
		$this->user        = $status->websiteusercr;
		$this->created     = timestamp(2, $status->createtimedb);
	}

	// add
	// ---------------------------------------- 
	public function add($shipping){
		/* Add shipping to existing status */
		$this->check    = ""; // shipping statusses not deletable!
		$this->shipId   = $shipping->shipmentintid;
		$this->sentFrom = array_key_exists($shipping->shippingfrom       , $this->locOpts) ? $this->locOpts[$shipping->shippingfrom       ] : "";
		$this->sentTo   = array_key_exists($shipping->shippingdestination, $this->locOpts) ? $this->locOpts[$shipping->shippingdestination] : "";
	}

	// insert
	// ---------------------------------------- 
	public function insert($shipping){
		/* Add shipping without status */
		$this->add($shipping);
		$this->eventdatedb = $shipping->shippingdatedb;
		$this->date        = timestamp(2, $shipping->shippingdatedb);
		$this->user        = $shipping->websiteusercr;
		$this->created     = timestamp(2, $shipping->createtimedb  );
		$this->direct      = $shipping->iseqflag=='T';
	}
}		



// Display Equipment Page
// ============================================
class DisplayEquipPage extends Page {


	// public members and methods
	// ========================================

	// members
	// ---------------------------------------- 
	public $eqId         = NULL;
	public $mtfId        = NULL;
	public $otherId      = NULL;
	public $reachedLimit = false;

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->eqId    = NULL;
		$this->mtfId   = NULL;
		$this->otherId = NULL;
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if(array_key_exists("eqId"   , $this->post)) $this->eqId    = $this->post["eqId"   ];
		if(array_key_exists("mtfId"  , $this->post)) $this->mtfId   = $this->post["mtfId"  ];
		if(array_key_exists("otherId", $this->post)) $this->otherId = $this->post["otherId"];

		$res = false;
		if($this->post["do"]=="load"     ) {
			$this->post["docMtfId"]=NULL; 
			$this->html->set("docMtfId", "");
			equipSubmitLoad($this);
		}
		else if($this->post["do"]=="save"     ) $res = $this->submitSave      ();
		else if($this->post["do"]=="delstat"  ) $res = $this->submitDelStatus ();
		else if($this->post["do"]=="newcom"   ) $res = $this->submitNewComment();
		else if($this->post["do"]=="delcom"   ) $res = $this->submitDelComment();
		else if($this->post["do"]=="newdoc"   ) $res = $this->submitNewDoc    ();
		else if($this->post["do"]=="adddoc"   ) $res = $this->submitAddDoc    ();
		else if($this->post["do"]=="deldoc"   ) $res = $this->submitDelDoc    ();
		else if($this->post["do"]=="parenting") $res = $this->submitParenting ();
		else if($this->post["do"]=="delpar"   ) $res = $this->submitDelParent ();
		else if($this->post["do"]=="delchl"   ) $res = $this->submitDelChild  ();
		else if($this->post["do"]=="download" ) $res = $this->submitDownload  ();

		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}




	// private members and methods
	// ========================================

	// deleteEntries
	// ---------------------------------------- 
	private function deleteEntries($table, $ids, $col="isvalidflag") {
		if(empty($ids) || count($ids)==0) return false;
		$c = new DbConfig($this->master, "deleteEntries");
		$c->select("id_".$table, $ids);
		$t = $this->db->get($table);
		$t->update(array($col=>"F"), $c);
		$t->push();
		//$t->delete($c);
		return true;
	}

	// loadEquipment
	// ---------------------------------------- 
	private function loadEquipment() {
		/* Loads the information for the equipment entry and stores it in the HTML handler */

		if(empty($this->eqId)) return;

		// basic equipment info
		$equipment = $this->db->readTable("equipment", array("*"),
		                                               array("id_equipment"=>$this->eqId));
		if($equipment->count()!=1) { 
			$this->vb->error(sprintf("The requested equipment entry (%d, %s, %s) does not exist!", $this->eqId, $this->mtfId, $this->otherId));
			return "";
		}
		$isncf = !empty($equipment->isnonconfflag) ? $equipment->isnonconfflag : "X";


		// status and locations
		$statOpts = $this->master->getOptionsStatus(                   ); // proper id association
		$locOpts  = $this->master->getOptionsSites (                   ); // proper id association
		$statIgn  = $this->master->getOptionsStatus("duringdispleqflag"); // statuses to change it to
		$locIgn   = $this->master->getOptionsSites ("duringdispleqflag"); // sites to change it to
		$statDis  = array_keys(array_diff($statOpts, $statIgn)); // id of statuses to disable
		$locDis   = array_keys(array_diff($locOpts , $locIgn )); // id of sites to disable   
		$statLock = $this->master->getOptionsStatus("priordispleqflag" , "F"); // statuses to lock
		$locLock  = $this->master->getOptionsSites ("priordispleqflag" , "F"); // sites to lock



		// ------ left side ---------

		// hierarchical equipment tree
		if(isset($this->post["showTree"]) && $this->post["showTree"]==1){
			$cppl = new DbConfig($this->master, "parentinglist");
			//$cp->columns = array("p.parenteqentryid", "p.eqentryid", "p.position",
			//                    "e.partsbatchmtfid", "e.otherid", "t.eqtypename", "t.eqtypecode", "level");
			$cppl->columns = array("p.eqentryid", "p.position", "e.otherid", "et.eqtypename", "et.projectid", "level");
			$cppl->joinon  = "p.eqentryid = e.id_equipment";
			$cppl->joinon  = "e.eqtypecodeid = et.id_equipmenttypes";
			$cppl->select(NULL, "p.isactiveflag='T' start with p.parenteqentryid = '1' connect by p.parenteqentryid = prior p.eqentryid");
			$cppl->select("p.isactiveflag"   , "T", "eq" , 1);
			//$cppl->select("et.projectid"     , "5", "neq", 2);
			//$cppl->select("et.treetype"      , "T", "eq" , 2);
			$cppl->select("et.treetype"      , "not null", "eq", 2);
			$cppl->select("p.parenteqentryid", "2", "neq", 3);
			$cppl->spattern = "%0 AND %1 AND (%2 OR %3)"; // to avoid bracketing before custom select
			$cppl->order ("et.treetype"      , "asc", "siblings");
			$cppl->order ("et.eqtypename"    , "asc", "siblings");
			$cppl->order ("e.partsbatchmtfid", "asc", "siblings");
			$cppl->slim = true;
			$this->db->read("parentinglist", $cppl, true);

			$entries = array();
			foreach($this->db->parentinglist->slim as $row)
				array_push($entries, array(sprintf("<span %s>%s %s <a href=\"#\" class=\"%s\" onclick=\"set('newEqId', '%s');submitItAs('load', 'eqId')\"> (%s)</a></span>", $row["et_projectid"]==5?"class=\"fpentry\"":"", $row["e_otherid"], $row["p_position"], $row["et_projectid"]==5?"fplink":"treelink", $row["p_eqentryid"], $row["et_eqtypename"]), $row["level"]));
			$this->html->set("htmllist", $this->html->makeUlTree("expList", $entries));
			$this->html->set("tree"    , $this->html->template("displayequip_body_tree"));
		}


		// ------ middle ------------

		// basic equipment info
		$this->html->set("subBatchId"   , $equipment->subbatchid   );
		$this->html->set("isNcf"        , $isncf                   );
		$this->html->set("quantity"     , $equipment->quantity     );
		$this->html->set("goodQuantity" , $equipment->goodquantity );
		$this->html->set("edittime"     , $equipment->edittime     );
		$this->html->set("websiteusered", $equipment->websiteusered);
		$this->html->set("createTime"   , timestamp(1, $equipment->createtime));
		$this->html->set("createUser"   , $equipment->websiteusercr);


		// details
		$eqdetails = $this->db->readTable("equipmentdetails", array("*"), array("eqentryid"=>$this->eqId));
		$orderedby   = $eqdetails->orderedby;	
		$ordereddate = $eqdetails->ordereddate;	
		$orderref    = $eqdetails->orderreference;
		$supplier    = $eqdetails->supplier;
		$cablelength = $eqdetails->cablelength;
		$manudate    = $eqdetails->manufacturingdate;
		$deldate     = $eqdetails->deliverydate;	


		// parent info
		$heritage = $this->db->batchheritage;
		$cp = new DbConfig($this->master, "parent");
		$cp->column = "PARENTBATCHEQENTRYID";
		$cp->select(NULL, strtoupper("connect_by_isleaf = 1 start with eqentryid='".$this->eqId."' connect by prior parentbatcheqentryid = eqentryid"));
		$heritage->read($cp);
		$hasParent = ($heritage->count()==1);
		$pareqid   = $hasParent ? $heritage->parentbatcheqentryid : -1;
		$disabled  = $hasParent ? "disabled" : "";

		if($hasParent){
			$pardetails  = $this->db->readTable("equipmentdetails", array("*"), array("eqentryid"=>$pareqid));
			$orderedby   = $pardetails->orderedby        ;
			$ordereddate = $pardetails->ordereddate      ;
			$orderref    = $pardetails->orderreference   ;
			$supplier    = $pardetails->supplier         ;
			$cablelength = $pardetails->cablelength      ;
			$manudate    = $pardetails->manufacturingdate;
			$deldate     = $pardetails->deliverydate     ;
		}

		$paratt   = $this->db->readTable("parenting", array("position"), array("eqentryid"=>$this->eqId));
		$position = $paratt->position;

		$origin   = $hasParent ? $this->html->template("displayequip_body_origin", array("pareqid"=>$pareqid)) : "";

		$this->html->set("orderedby"    , $orderedby              );
		$this->html->set("ordereddate"  , $ordereddate            );
		$this->html->set("orderref"     , $orderref               );
		$this->html->set("supplier"     , $supplier               );
		$this->html->set("cablelength"  , $cablelength            );
		$this->html->set("manudate"     , substr($manudate, 0, 10));
		$this->html->set("deldate"      , substr($deldate , 0, 10));
		$this->html->set("position"     , $position               );
		$this->html->set("disabled"     , $disabled               );
		$this->html->set("origin"       , $origin                 );


		// eq type info
		$ct = new DbConfig($this->master, "eqtype");
		$ct->columns = array("et.quantityunit", "et.eqtypename", "et.isbatchflag", "et.alterneqtype");
		$ct->joinon  = "e.eqtypecodeid = et.id_equipmenttypes";
		$ct->select("e.id_equipment", $this->eqId);
		$this->db->read("eqtypeatts", $ct);
		$eqtypeatts = $this->db->eqtypeatts;

		$isBatch = $eqtypeatts->et_isbatchflag=="T";
		$isGroup = (!$isBatch && !empty($equipment->quantity));

		$this->html->set("quantityUnit", $eqtypeatts->et_quantityunit);
		$this->html->set("eqTypeName"  , $eqtypeatts->et_eqtypename  );
		$this->html->set("isBatchFlag" , $eqtypeatts->et_isbatchflag );
		$this->html->set("alternEqType", $eqtypeatts->et_alterneqtype);


		// status info
		$statattr = $this->master->getEqStatus($this->eqId, array("s.statusname", "eventdate", "createtime", "sl.websiteusercr"));
		if($statattr->count()<1) 
			$this->vb->warning(sprintf("Status information for equipment entry (%d, %s, %s) could not be found!", 
			                                  $this->eqId, $this->mtfId, $this->otherId));

		$statusId = $statattr->sl_statusid;
		$dis      = in_array($statattr->s_statusname, $statLock);

		$this->html->set("statusDisable"   , $dis ? "disabled" : ""                                                  );
		$this->html->set("statusId"        , $statusId                                                               );
		$this->html->set("statusName"      , $statattr->s_statusname                                                 );
		$this->html->set("statusCreateTime", timestamp(2, $statattr->eventdate)                                      );
		$this->html->set("statusCreateUser", $statattr->sl_websiteusercr                                             );
		$this->html->set("statusList"      , $this->html->makeSelect("statusId", $statOpts, $statusId, $statDis, array(), $dis, "setStatLoc();toggleEventDate()", "class='invisiblelike center'"));


		// locations info
		$locattr = $this->master->getEqLocation($this->eqId, array("sl.minorlocdesc", "sl.websiteusercr"));
		if($locattr->count()<1) 
			$this->vb->warning(sprintf("Location information for equipment entry (%d, %s, %s) could not be found!", 
			                                  $this->eqId, $this->mtfId, $this->otherId));
		$locationId = $locattr->sl_majorlocid;
		$dis        = in_array($locationId, array_keys($locLock)) ? true : false;
		
		$this->html->set("locationDisable"   , $dis ? "disabled" : ""                                              );
		$this->html->set("locationId"        , $locationId                                                         );
		$this->html->set("locationName"      , array_key_exists($locationId, $locOpts) ? $locOpts[$locationId] : "");
		$this->html->set("locationRoomDesc"  , $locattr->sl_minorlocdesc                                           );
		$this->html->set("locationCreateTime", timestamp(2, $locattr->eventdate)                                   );
		$this->html->set("locationCreateUser", $locattr->sl_websiteusercr                                          );
		$this->html->set("locationList"      , $this->html->makeSelect("locationId", $locOpts, $locationId, $locDis, array(), $dis, "setStatLoc();toggleEventDate()", "class='invisiblelike center'"));


		// all possible parents (necessary for the three tables to come)
		$cp = new DbConfig($this->master, "parent");
		$cp->column = "parentbatcheqentryid";
		$cp->raw("start with eqentryid='".$this->eqId."' connect by prior parentbatcheqentryid = eqentryid");
		$heritage->read($cp, true);
		$allpareqids = array($this->eqId);
		foreach($heritage as $row) array_push($allpareqids, $row->parentbatcheqentryid);
		$allpareqids = array_unique($allpareqids);


		// history table
		$statLoc  = $this->db->readTable("statuslocation", array("id_statuslocation", "statusid", "majorlocid", "websiteusercr", "eventdatedb", "eqentryid"),
		                                                  array("eqentryid"=>$allpareqids, "isvalidflag"=>"T"),
		                                                  array("TO_CHAR(EVENTDATE , 'YYYY-MM-DD HH24:MI:SS')"=>"eventdatedb" , 
		                                                        "TO_CHAR(createtime, 'YYYY-MM-DD HH24:MI:SS')"=>"createtimedb",
		                                                        "(CASE WHEN eqentryid=".$this->eqId." THEN 'T' ELSE 'F' END)"=>"ISEQFLAG"),  array("eventdatedb"=>"desc")); 
		$shipHist = $this->db->readTable("shippinghistory", array("shippingfrom", "shippingdestination", "shippingdatedb", "shipmentintid", "websiteusercr", "eqentryid"),  
		                                                  array("eqentryid"=>$allpareqids),
		                                                  array("TO_CHAR(shippingdate, 'YYYY-MM-DD HH24:MI:SS')"=>"shippingdatedb",
		                                                        "TO_CHAR(createtime  , 'YYYY-MM-DD HH24:MI:SS')"=>"createtimedb"),  array("shippingdatedb"=>"asc")); 
		$statIdFullySplitUp = array_search("fully split up"            , $statOpts); // FIXME: very bad to do hardcoded!
		$statIdTransit      = array_search("in transit"                , $statOpts);
		$statIdPartAss      = array_search("partially assembled"       , $statOpts);
		$statIdAss          = array_search("assembled"                 , $statOpts);
		$locIdUndefined     = array_search("undefined"                 , $locOpts );
		$locIdTransit       = array_search("in transit"                , $locOpts );
		$locIdParent        = array_search("(see parent)"              , $locOpts );
		$locIdFunct         = array_search("functional positions space", $locOpts );
		$dontStatus = array_filter(array($statIdFullySplitUp, $statIdTransit, $statIdPartAss, $statIdAss));
		$dontLoc    = array_filter(array($locIdTransit, $locIdParent, $locIdFunct)); 

		// build list of statuses
		$entries = array();
		foreach($statLoc as $row){
			if($row->eqentryid!=$this->eqId && ($row->statusid==$statIdFullySplitUp || $row->majorlocid==$locIdUndefined)) continue;
			array_push($entries, new HistTableEntry($statOpts, $locOpts, $row));
		}

		// add the shippings if they have a counterpart
		foreach($shipHist as $ship){
			$inserted = false;
			foreach($entries as $hte){
				if($hte->statusId!=$statIdTransit && $hte->locId!=$locIdTransit) continue;
				if(!isAlmost($ship->shippingdatedb, $hte->eventdatedb)) continue;
				$hte->add($ship);
				$inserted = true;
				break;
			}
			if($inserted) continue;
			$theHte = new HistTableEntry($statOpts, $locOpts, NULL);
			$theHte->insert($ship);
			array_push($entries, $theHte);
		}

		// sort
		function cmp($x, $y){ return $x->eventdatedb==$y->eventdatedb ? strtotime($x->created)<strtotime($y->created) : strtotime($x->eventdatedb)<strtotime($y->eventdatedb); }
		usort($entries, "cmp");

		// build table
		$rows = array();
		$i = count($entries);
		foreach($entries as $hte){
			$fields = array();
			$fields["intId"   ] = $i;
			$fields["style"   ] = $hte->direct ? "" : "style='font-style: italic;'";
			$fields["check"   ] = (in_array($hte->statusId, $dontStatus) || in_array($hte->location, $dontLoc) || count($entries)==1) ? "" : $hte->check;
			$fields["statusId"] = $hte->statusId;
			$fields["status"  ] = $hte->status;
			$fields["location"] = $hte->location;
			$fields["shipId"  ] = $hte->shipId;
			$fields["sentFrom"] = $hte->sentFrom;
			$fields["sentTo"  ] = $hte->sentTo;
			$fields["date"    ] = $hte->date;
			$fields["user"    ] = $hte->user;
			array_push($rows, $this->html->template("displayequip_body_histtable_row", $fields));
			$i--;
		}
		$histtable = $this->html->template("displayequip_body_histtable", array("rows"=>implode("", $rows)));
		$this->html->set("histtable", $histtable);


		////// match and build the union of the two (very ugly!)
		////$shippings = array();
		////foreach($shipHist as $row)
		////	$shippings[$row->shippingdatedb] = $row;

		////$used  = array();
		////$union = array();
		////foreach($statLoc as $row){
		////	$shipment = NULL;
		////	if($row->eqentryid!=$this->eqId && ($row->statusid==$statIdFullySplitUp || $row->majorlocid==$locIdUndefined)) continue;
		////	if($row->statusid==$statIdTransit || $row->majorlocid==$locIdTransit){
		////		foreach($shippings as $date=>$shiprow){
		////			if(!isAlmost($date, $row->eventdatedb)) continue;
		////			$shipment = $shiprow;
		////			array_push($used, $row->eventdatedb);
		////			break;
		////		}
		////	}
		////	#if(array_key_exists($row->eventdatedb, $union)) $add="_"+count(
		////	$union[$row->eventdatedb]=array($row, $shipment);
		////}
		////foreach($shippings as $date=>$row){
		////	if(in_array($date, $used)) continue;
		////	$union[$date] = array(NULL, $row);
		////}

		////krsort($union);
		////$rows = array();
		////$i = count($union);
		////foreach($union as $row){
		////	$status   = $row[0];
		////	$shipment = $row[1];
		////	$fields   = array("check"=>"", "shipId"=>"", "sentFrom"=>"", "sentTo"=>"", "statusId"=>"", "status"=>"", "location"=>"", "date"=>"", "user"=>"");
		////	$direct   = false; // true if from the item itself
//print ////$status."<br />";
		////	if(!empty($status)){
		////		$fields["check"   ] = $status->iseqflag=='T' ? "<center><input type='checkbox' name='idstats[]' value='".strval($status->id_statuslocation)."'></center>" : "";
		////		$fields["statusId"] = $status->statusid;
		////		$fields["status"  ] = array_key_exists($status->statusid  , $statOpts) ? $statOpts[$status->statusid]  : "";
		////		$fields["location"] = array_key_exists($status->majorlocid, $locOpts ) ? $locOpts[$status->majorlocid] : "";
		////		$fields["date"    ] = timestamp(2, $status->eventdatedb);
		////		$fields["user"    ] = $status->websiteusercr;
		////		if($status->iseqflag=='T') $direct = true;
		////	}
		////	if(!empty($shipment)){
		////		$fields["check"   ] = ""; // shipping statusses not deletable!
		////		$fields["shipId"  ] = $shipment->shipmentintid;
		////		$fields["sentFrom"] = array_key_exists($shipment->shippingfrom       , $locOpts) ? $locOpts[$shipment->shippingfrom       ] : "";
		////		$fields["sentTo"  ] = array_key_exists($shipment->shippingdestination, $locOpts) ? $locOpts[$shipment->shippingdestination] : "";
		////		$fields["date"    ] = timestamp(2, $shipment->shippingdatedb);
		////		$fields["user"    ] = $shipment->websiteusercr;
		////		if($shipment->eqentryid==$this->eqId) $direct = true;
		////	}
		////	$fields["intId"] = $i;
		////	$fields["style"] = $direct ? "" : "style='font-style: italic;'";
		////	array_push($rows, $this->html->template("displayequip_histtable_row", $fields));
		////	$i--;
		////}
		////$histtable = $this->html->template("displayequip_histtable", array("rows"=>implode("", $rows)));
		////$this->html->set("histtable", $histtable);


		// comments table
		$eqcomments = $this->db->readView("comtable", array("ec.shipid = sh.id_shippinghistory"), 
		                                              array("ec.id_eqcomments", "ec.eqcomment", "ec.typeofcomment", "eventdatedb", "ec.websiteusercr", 
		                                                    "sh.shipmentintid", "ec.eqentryid"), 
		                                              array("ec.eqentryid"=>$allpareqids, "ec.isvalidflag"=>"T"), 
		                                              array("TO_CHAR(EC.EVENTDATE, 'YYYY-MM-DD HH24:MI:SS')"=>"eventdatedb",
		                                                    "(CASE WHEN ec.eqentryid=".$this->eqId." THEN 'T' ELSE 'F' END)"=>"ISEQFLAG"),  
		                                              array("eventdatedb"=>"desc")); 

		$format = function($field, $row){
			$key  = $field->key;
			$item = $field->value;
			if(in_array($key, array("the_pk", "sh_shipmentintid", "ec_eqentryid", "iseqflag"))) return false;
			if($key=="ec_id_eqcomments"){
				return $row->iseqflag=='T' ? "<center><input type='checkbox' name='idcomms[]' value='".strval($item)."'></center>" : "";
			}
			if($key=="ec_typeofcomment" && $item=="shipping"){
				return $item ."<br />(ID: ".strval($row->sh_shipmentintid).")";
			}
			return ($item ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
		};
		$rowformat = function($row){ return $row->iseqflag!='T' ? "style='font-style: italic;'" : ""; };

		//foreach($eqcomments as $row) $row->setVar("theEqId", $this->eqId);
		$rows     = $this->html->makeTablerows($eqcomments, $format, false, $rowformat);
		$options  = $this->html->makeOptions($this->master->doctypes, -1, array(), array("all"=>"style=\"text-align:left;\""));
		$this->html->set("comtable", $this->html->template("displayequip_body_comtable", array("rows"=>$rows, "options"=>$options)));


		// documents table
		$docview = $this->db->readView("docview", array("d.id_documents = dl.docid",
		                                                "dl.shipid = sh.id_shippinghistory"),
		                                          array("dl.id_doclink", "d.id_documents", "d.docname", "d.typeofdoc", "eventdatedb", "dl.websiteusercr", "sh.shipmentintid", "dl.eqentryid"), 
		                                          array("dl.eqentryid"=>$allpareqids, "dl.isvalidflag"=>"T"), 
		                                          array("TO_CHAR(DL.EVENTDATE, 'YYYY-MM-DD HH24:MI:SS')"=>"eventdatedb",
		                                                "(CASE WHEN dl.eqentryid=".$this->eqId." THEN 'T' ELSE 'F' END)"=>"ISEQFLAG"), array("eventdatedb"=>"desc")); 

		$format = function($field, $row){
			$key  = $field->key;
			$item = $field->value;
			if(in_array($key, array("dl_eqentryid", "d_id_documents", "theEqId", "sh_shipmentintid", "iseqflag"))) return false;
			if($key=="dl_id_doclink"){
				return $row->iseqflag=='T' ? "<center><input type='checkbox' name='iddocs[]' value='".strval($item)."'></center>" : "";
			}
			if($key=="d_typeofdoc" && $item=="shipping"){
				return $item ."<br />(ID: ".strval($row->sh_shipmentintid).")";
			}
			if($key=="d_docname"){
				return "<a href=\"#\" onclick=\"set('docId', ".$row->d_id_documents.");submitIt('download')\">" . ($item != null ? htmlentities($item, ENT_QUOTES) : "document") . "</a>";
			}
			else if($key=="eventdatedb"){
				$item = timestamp(2, $item);
			}
			return ($item ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
		};
		$rowformat = function($row){ return $row->iseqflag!='T' ? "style='font-style: italic;'" : ""; };
		
		//foreach($docview as $row) $row->setVar("theEqId", $this->eqId);
		$rows = $this->html->makeTablerows($docview, $format, false, $rowformat);


		// existing docs for given mtf id (other equipment)
		$options_existing = array();
		if(!empty($this->post["docMtfId"])) {

			$extEqId = $this->master->getEqId($this->post["docMtfId"]);
			$cpex = new DbConfig($this->master, "parentext");
			$cpex->column = "parentbatcheqentryid";
			$cpex->raw("start with eqentryid='".$extEqId."' connect by prior parentbatcheqentryid = eqentryid");
			$heritage->read($cpex, true);
			$extallpareqids = array($extEqId);
			foreach($heritage as $row) array_push($extallpareqids, $row->parentbatcheqentryid);
			$extallpareqids = array_unique($extallpareqids);

			$extdocs = $this->db->readView("extdocs", array("d.id_documents = dl.docid", 
			                                                "e.id_equipment = dl.eqentryid"), 
			                                          array("dl.docid", "d.docname", "e.id_equipment", "e.partsbatchmtfid", "e.subbatchid"), 
			                                          array("dl.eqentryid"=>$extallpareqids), 
			                                          array(), array("dl.eventdate"=>"desc")); 

			if($extdocs->count()>0){
				foreach($extdocs as $row){
					if(empty($row->dl_docid)) continue;
					array_push($options_existing, "<option style=\"text-align:left;\" value=\"".$row->dl_docid."\">EQ ID: ".$row->e_id_equipment." | MTF ID: ".$row->e_partsbatchmtfid." | Subbatch ID: ".($row->e_subbatchid ? $row->e_subbatchid : "&nbsp;&nbsp;&nbsp;")." | Document: ".$row->d_docname."</option>\n");
				}
			}
		}
		if(count($options_existing)==0)
			$options_existing = array("<option style=\"text-align:left;\" value=\"0\">MTF-No. not given!</option>\n");
		$doctable = $this->html->template("displayequip_body_doctable", array("rows"            =>$rows   , 
		                                                                      "options_new"     =>$options,
		                                                                      "options_existing"=>implode("",$options_existing)));
		$this->html->set("doctable", $doctable);


		// parent table
		$cp = new DbConfig($this->master, "partable");
		$cp->columns = array("p.id_parenting", "p.parenteqentryid", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", 
		                     "p.position", "p.numberoflinks", "eventdatedb", "p.websiteusercr");
		$cp->joinon = "p.parenteqentryid = e.id_equipment";
		$cp->joinon = "e.eqtypecodeid    = et.id_equipmenttypes";
		$cp->reformat("TO_CHAR(P.EVENTDATE, 'YYYY-MM-DD HH24:MI')", "eventdatedb");
		$cp->select("p.eqentryid"      , $this->eqId);
		$cp->select("p.isactiveflag"   , "T"        );
		$cp->order ("e.partsbatchmtfid", "asc"      );
		//$cp->order ("p.eventdate"      , "desc"     );
		$cp->slim = true;
		$ptview = $this->db->read("partable", $cp);

		$format = function($item, $row, $key){
			$b1 = ""; $b2 = "";
			if($key=="p_id_parenting"){
				return !in_array($row["p_parenteqentryid"], array("1", "2")) ? "<center><input type='checkbox' name='idpars[]' value='".strval($row["p_parenteqentryid"])."'></center>" : ""; // FIXME: super BAAD to put orphanage ID hardcoded here!!!!
			}
			if($key=="p_parenteqentryid"){
				$b1 = "<a href=\"#\" onclick=\"set('newEqId', '".$item."');submitItAs('load', 'eqId')\">";
				$b2 = "</a>";
			}
			else if($key=="p_serviceends"){
				if     ($item=="S") $item = "start";
				else if($item=="E") $item = "end";
				else if($item=="T") $item = "tray";
				else                $item = "";
			}
			else if($key=="p_eventdate"){
				$item = timestamp(1, $item);
			}
			return $b1.((!empty($item) || $item=="0") ? htmlentities($item, ENT_QUOTES) : "&nbsp;").$b2;
		};

		$rows = $this->html->makeTablerows($ptview, $format, true);
		$this->html->set("partable", $this->html->template("displayequip_body_partable", array("rows"=>$rows)));


		// children table
		$cc = new DbConfig($this->master, "chiltable");
		$cc->columns = array("p.id_parenting", "p.eqentryid", "e.partsbatchmtfid", "e.otherid", "et.eqtypename", 
		                     "p.position", "p.numberoflinks", "eventdatedb", "p.websiteusercr");
		$cc->joinon = "p.eqentryid    = e.id_equipment";
		$cc->joinon = "e.eqtypecodeid = et.id_equipmenttypes";
		$cc->reformat("TO_CHAR(P.EVENTDATE, 'YYYY-MM-DD HH24:MI')", "eventdatedb");
		$cc->select("p.isactiveflag"   , "T"   );
		$cc->select(NULL               , "level = 1 start with p.parenteqentryid ='".$this->eqId."' connect by p.parenteqentryid = prior p.eqentryid");
		$cp->order ("e.partsbatchmtfid", "asc" );
		//$cc->order ("p.eventdate"      , "desc");
		$cc->slim = true;
		$clview = $this->db->read("chiltable", $cc);

		$format = function($item, $row, $key) {
			$b1 = ""; $b2 = "";
			if($key=="p_id_parenting"){
				return "<center><input type='checkbox' name='idchls[]' value='".strval($row["p_eqentryid"])."'></center>";
			}
			if($key=="p_eqentryid"){
				$b1 = "<a href=\"#\" onclick=\"set('newEqId', '".$item."');submitItAs('load', 'eqId')\">";
				$b2 = "</a>";
			}
			else if($key=="p_serviceends"){
				if     ($item=="S") $item = "start";
				else if($item=="E") $item = "end";
				else if($item=="T") $item = "tray";
				else                $item = "";
			}
			else if($key=="p_eventdate"){
				$item = timestamp(1, $item);
			}
			return $b1.((!empty($item) || $item=="0") ? htmlentities($item, ENT_QUOTES) : "&nbsp;").$b2;
		};

		$rows = $this->html->makeTablerows($clview, $format, true);
		$this->html->set("chiltable" , $this->html->template("displayequip_body_chiltable", array("rows"=>$rows)));
		$this->html->set("heritageED", $this->html->template("eventDate", array("title"=>"EVENT DATE", "name"=>"parEventDate", "addd"=>"", "addl"=>"invisdesc"), NULL, "all"));
		$this->html->set("heritage"  , $this->html->template("displayequip_body_heritage"));



		// ------ right side --------

		// conf indic
		$this->html->set("confIndic", $this->master->ncfs[$isncf]);


		// NCF dropdown
		$this->html->set("ncfdropdown", $this->html->makeSelect("isnonconfflag", $this->master->ncfs, $isncf, array(), array(), false, "", "class=\"invisiblelike ncf\""));


		// batch indic
		$batchindic = $this->master->getBatchIndic($eqtypeatts->et_isbatchflag!="F", $equipment->subbatchid, $equipment->quantity);
		$this->html->set("batchIndic", $batchindic);
		$batchcols  = $this->master->getBatchCols ($eqtypeatts->et_isbatchflag!="F", $equipment->subbatchid, $equipment->quantity);
		$this->html->set("batchCols" , $batchcols );


		// subbatch or group-children table
		$cj = new DbConfig($this->master, "cj");
		$cj->column = "eqentryid";
		$cj->reformat("FIRST_VALUE(STATUSID   IGNORE NULLS) OVER (PARTITION BY EQENTRYID ORDER BY EVENTDATE DESC)", "last_status"  );
		$cj->reformat("FIRST_VALUE(MAJORLOCID IGNORE NULLS) OVER (PARTITION BY EQENTRYID ORDER BY EVENTDATE DESC)", "last_location");
		$cj->select("isvalidflag", "T");
		$cj->table = "statuslocation";

		// note: distinction between group and not-group (batch or item)
		// groups have children as dedicated equipments with dedicated MTF ids
		// batches have children as dedicated equipments but with the same MTF id 
		// items don't have children
		$cs = new DbConfig($this->master, "subtable");
		if($isGroup) $cs->columns = array("distinct e2.id_equipment", "e2.subbatchid", "e2.quantity", "et.quantityunit", "l.sitename"); 
		else         $cs->columns = array("distinct e.id_equipment" , "e.subbatchid" , "e.quantity" , "et.quantityunit", "l.sitename");
		$cs->joinconfig = $cj;
		$cs->joinon = "e.eqtypecodeid = et.id_equipmenttypes";
		$cs->joinon = "bh.parentbatcheqentryid = e.id_equipment";
		if($isGroup){
			$cs->joinon = "e2.id_equipment = bh.eqentryid";
			$cs->joinon = "*cj.eqentryid = e2.id_equipment"; 
		}
		else 
			$cs->joinon = "*cj.eqentryid = e.id_equipment"; 
		$cs->joinon = "s.id_statuses = last_status";
		$cs->joinon = "l.id_locations = last_location";
		$cs->select("e.partsbatchmtfid", $this->mtfId);
		$cs->select("s.statusname"     , "not null"  );
		$cs->select("l.sitename"       , "not null"  );
		if($isGroup){
			$cs->order("e2.id_equipment");
		}
		else {
			$cs->select("e.subbatchid", "not null"             );
			$cs->select("s.statusname", "fully split up", "neq");
			$cs->order("e.id_equipment");
		}
		$stview = $this->db->read("subtable", $cs); 

		$format = function($field, $row){
			$first = $row->view->column(0)->key;
			$b1 = ""; $b2 = "";
			$item = $field->value;
			if($field->column->key == $first){
				$b1 = "<a href=\"#\" onclick=\"set('newEqId', '".$item."');submitItAs('load', 'eqId')\">";
				$b2 = "</a>";
			}
			return $b1 . ($item ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . $b2;
		};
		$rows = $this->html->makeTablerows($stview, $format);
		$this->html->set("subtable", $this->html->template("displayequip_body_subtable", array("rows"=>$rows)));


		// workflow table
		$img1  = $this->loadStatusSymbol($this->eqId, $statusId, 1 );
		$img2  = $this->loadStatusSymbol($this->eqId, $statusId, 2 );
		$img3  = $this->loadStatusSymbol($this->eqId, $statusId, 3 , false); // don't make 
		$img4  = $this->loadStatusSymbol($this->eqId, $statusId, 4 , false); // these three
		$img5  = $this->loadStatusSymbol($this->eqId, $statusId, 5 , false); // boxes dark gray
		$img6  = $this->loadStatusSymbol($this->eqId, $statusId, 6 );
		$img7  = $this->loadStatusSymbol($this->eqId, $statusId, 7 );
		$img8  = $this->loadStatusSymbol($this->eqId, $statusId, 8 );
		$img9  = $this->loadStatusSymbol($this->eqId, $statusId, 9 );
		$img10 = $this->loadStatusSymbol($this->eqId, $statusId, 10);
		$img11 = $this->loadStatusSymbol($this->eqId, $statusId, 11);
		$img12 = $this->loadStatusSymbol($this->eqId, $statusId, 12);
		$img28 = $this->loadStatusSymbol($this->eqId, $statusId, 28);
	
		$workflow = $this->html->template("displayequip_body_workflow", array("imagepath1" =>$img1,
		                                                                      "imagepath2" =>$img2,
		                                                                      "imagepath3" =>$img3,
		                                                                      "imagepath4" =>$img4,
		                                                                      "imagepath5" =>$img5,
		                                                                      "imagepath6" =>$img6,
		                                                                      "imagepath7" =>$img7,
		                                                                      "imagepath8" =>$img8,
		                                                                      "imagepath9" =>$img9,
		                                                                      "imagepath10"=>$img10,
		                                                                      "imagepath11"=>$img11,
		                                                                      "imagepath12"=>$img12,
		                                                                      "imagepath28"=>$img28));
		$this->html->set("workflow", $workflow);


		// ------ the template ------
		$this->html->set("body", $this->html->template("displayequip_body"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Generates the form and fills it with the information of the selected 
		* equipment entry (if so, i.e. if $this->eqid is not NULL), otherwise the
		* form will be simply empty */

		$this->html->set("saveActive", $this->globals["username"]=="default_user" ? "disabled" : "");
		equipLoadPage($this);
		$this->loadEquipment();
		return $this->html->template("displayequip");
	}

	// loadStatusSymbol
	// ---------------------------------------- 
	private function loadStatusSymbol($eqid, $actual, $position, $useDarkGray=true){
		/* Returns the path to the proper symbol at a given position for
		* the current actual status */
	
		$cv = new DbConfig($this->master, "maxevdate");
		$cv->table  = "statuslocation";
		$cv->column = "max(eventdate)";
		$cv->select("eqentryid"  , $eqid    );
		$cv->select("statusid"   , $position);
		$cv->select("isvalidflag", "T"      );
	
		$cs = new DbConfig($this->master, "statussymbol");
		$cs->columns = array("sl.statusid", "s.isworkflowstatflag");
		$cs->joinon  = "sl.statusid = s.id_statuses";
		$cs->select("sl.eqentryid"  , $eqid);
		$cs->select("sl.eventdate"  , $cv  );
		$cs->select("sl.isvalidflag", "T"  );
		$this->db->read("statussymbol", $cs);

		if($this->db->statussymbol->sl_statusid == $position)
			return "img/mbuttonh.png";

		$statuses = $this->db->readTable("statuses", array('isworkflowstatflag'), array("id_statuses"=>$actual));	
		if($useDarkGray && $position <= $actual && $statuses->isworkflowstatflag=="T")
			return "img/mbuttong.png";
		else
			return "img/mbuttongr.png";
	}

	// submitAddDoc
	// ---------------------------------------- 
	private function submitAddDoc(){
		/* Linking an existing document to the equipment */

		if(empty($this->post["docAdd"])) return true;
		$documents = $this->db->readTable("documents", array("docname"), 
		                                               array("id_documents"=>$this->post["docAdd"]));
		if($documents->count()==0) {
			$this->vb->error("There is no document with ID ".$this->post["docAdd"]." that could be linked!");
			return false;
		}
		$docName = $documents->docname;

		$eventDate = isValidDate($this->post["comdocEventDate"]) ? dbStringDate($this->post["comdocEventDate"]) : $this->master->eventDateDb;

		$this->db->doclink->append(array("eqentryid"    =>$this->eqId,
		                                 "docid"        =>$this->post["docAdd"],
		                                 "websiteusercr"=>$this->globals["username"],
		                                 "websiteusered"=>$this->globals["username"],
		                                 "eventdate"    =>$eventDate));

		$this->db->push();
		if($this->db->error()){
			$this->vb->error("Document ".$docName." could not be linked!", true);
			return false;
		}

		$this->vb->success("Document ".$docName." has successfully been linked!");
		return true;
	}

	// submitDownload
	// ---------------------------------------- 
	private function submitDownload(){
		/* Downloads a single document */
		downloadDoc($this);
	}

	// submitDelChild
	// ---------------------------------------- 
	private function submitDelChild() {
		/* Remove a parented child from the equipment */
		if(!isset($this->post["idchls"]) || empty($this->post["idchls"])) return false;
		foreach($this->post["idchls"] as $child){
			if(!parentingDelete($this, $child, $this->eqId)) 
				return false;
		}
		return true;
	}

	// submitDelComment
	// ---------------------------------------- 
	private function submitDelComment() {
		/* Remove a comment from the equipment */
		return $this->deleteEntries("eqcomments", $this->post["idcomms"]);
	}

	// submitDelDoc
	// ---------------------------------------- 
	private function submitDelDoc() {
		/* Remove a doclink from the equipment */
		return $this->deleteEntries("doclink", $this->post["iddocs"]);
	}

	// submitDelStatus
	// ---------------------------------------- 
	private function submitDelStatus() {
		/* Remove a statuslocation from the equipment */
		return $this->deleteEntries("statuslocation", $this->post["idstats"]);
	}

	// submitDelParent
	// ---------------------------------------- 
	private function submitDelParent() {
		/* Remove a parented parent from the equipment */
		if(!isset($this->post["idpars"]) || empty($this->post["idpars"])) return false;
		foreach($this->post["idpars"] as $parent){
			if(!parentingDelete($this, $this->eqId, $parent)) 
				return false;
		}
		return true;
	}

	// submitNewComment
	// ---------------------------------------- 
	private function submitNewComment() {
		/* Add new comment to the equipment */
		return addComment($this, $this->post["comText"], intval($this->post["comType"]), $this->post["comdocEventDate"], array($this->eqId), array(), "other", true);  
	}

	// submitNewDoc
	// ---------------------------------------- 
	private function submitNewDoc() {
		/* Upload a new document and link it to the equipment */
		return uploadDoc($this, "uplnewdoc", intval($this->post["docType"]), $this->post["comdocEventDate"], array($this->eqId), array(), "other", true);  
	}

	// submitParenting
	// ---------------------------------------- 
	private function submitParenting(){
		/* Creating parenting entries for the equipment; called by submit */
		$this->post["numberOfLinks"] = $this->post["parNum"]>0 ? $this->post["parNum"] : 1; 
		$idsChild  = equipGetIds($this, $this->post["typeIdChild"], $this->post["idChild"], $this->post["idChild"], $this->post["idChildSub"], $this->post["idChild"]);
		$idsParent = equipGetIds($this, $this->post["typeIdParent"], $this->post["idParent"], $this->post["idParent"], $this->post["idParentSub"], $this->post["idParent"]);
		$this->post["idChild"      ] = $idsChild [0];
		$this->post["idParent"     ] = $idsParent[0];
		$this->post["isActiveFlag" ] = "T";
		$this->reachedLimit = false;
		if(!parentingCheck($this, $this->post)) return false;
		return parentingDoIt($this, $this->post);
	}

	// submitSave
	// ---------------------------------------- 
	private function submitSave() {
		/* Update core equipment entry data */

		$equipment = $this->db->readTable("equipment"       , array("*"), array("id_equipment"=>$this->eqId));
		$eqdetails = $this->db->readTable("equipmentdetails", array("*"), array("eqentryid"   =>$this->eqId));
		if($equipment->count()!=1) { 
			$this->vb->error("The requested equipment entry does not exist!");
			return false;
		}

		// some checks on quantity
		if($this->post["quantity"]<$this->post["goodQuantity"]){
			$this->vb->error("GoodQuantity cannot exceed Quantity!");
			return false;
		}

		// update equipments variables (in the form, also parenting
		// variables can be shown, but those should not be editable)
		$fields = array("otherId", "goodQuantity", "quantity", "isnonconfflag");
		$update = false;
		foreach($fields as $field){
			$dbkey = strtolower($field);
			if($equipment->get($dbkey) == $this->post[$field]) continue;
			if(in_array($field, array("goodQuantity", "quantity"))){
				$q = $equipment->get("quantity");
				if(empty($q) || $q==1) continue; // not allowed to edit quantity or good quantity in this case
			}
			if(strtolower($field)=="isnonconfflag" && $this->post[$field]=="X") {
				$eq = $equipment->get($dbkey);
				if(empty($eq)) continue;
				$equipment->set($dbkey, "null");
				$update = true;
				continue;
			}
			$equipment->set($dbkey, $this->post[$field]);
			$update = true;
		}
		if($update){
			$equipment->set("websiteusered", $this->globals["username"]);
			$equipment->set("edittime"     , $this->master->eventDateDb);
		}

		if($eqdetails->count()==1) { 
			$fields = array("orderedby", "ordereddate", "orderreference", "supplier", 
			                "cablelength", "manufacturingdate", "deliverydate");
			$update = false;
			foreach($fields as $field){
				if($eqdetails->get($field)==NULL && $this->post[$field]==NULL) continue;
				if(in_array($field, array("manufacturingdate", "ordereddate"))){
					if(timestamp(1, $eqdetails->get($field))==$this->post[$field]) continue;
				}
				else if(strpos($field, "date")!==false){
					if(timestamp(2, $eqdetails->get($field))==$this->post[$field]) continue;
				}
				else {
					if($eqdetails->get($field) == $this->post[$field]) continue;
				}
				$eqdetails->set($field, $this->post[$field]);
				$update = true;
			}
			if($update){
				$eqdetails->set("websiteusered", $this->globals["username"]);
				$eqdetails->set("edittime"     , $this->master->eventDateDb);
			}
		}

		// retrieving from statuses
		$statattr = $this->master->getEqStatus($this->eqId);
		if($statattr->count()<1){ 
			$this->vb->warning(sprintf("Status information for equipment entry (%d, %s, %s) could not be found!", 
			                                  $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// retrieving from locations
		$locattr = $this->master->getEqLocation($this->eqId, array("sl.minorlocdesc"));
		if($locattr->count()<1){ 
			$this->vb->warning(sprintf("Location information for equipment entry (%d, %s, %s) could not be found!", 
			                                  $this->eqId, $this->mtfId, $this->otherId));
			return false;
		}

		// event date
		$eventDate = isValidDate($this->post["eventDate"]) ? dbStringDate($this->post["eventDate"]) : $this->master->eventDateDb;

		// updating status and location
		$toUpdate = array();
		if(array_key_exists("statusId", $this->post) && $statattr->sl_statusid != $this->post["statusId"]) 
			$toUpdate["statusid"] = $this->post["statusId"];
		if((array_key_exists("locationId", $this->post) && $locattr ->sl_majorlocid!=$this->post["locationId"]) || (array_key_exists("locationRoomDesc", $this->post) && $locattr ->sl_minorlocdesc != $this->post["locationRoomDesc"])){ 
			$locId = (array_key_exists("locationId", $this->post) && $locattr ->sl_majorlocid!=$this->post["locationId"]) ? $this->post["locationId"] : $locattr ->sl_majorlocid;
			$toUpdate["majorlocid"  ] = $locId;
			$toUpdate["minorlocdesc"] = $this->post["locationRoomDesc"];
		}
		if(count($toUpdate)>0 && $this->post["writeStatLoc"]=="1"){
			$toUpdate["eqentryid"    ] = $this->eqId;
			$toUpdate["websiteusercr"] = $this->globals["username"];
			$toUpdate["eventdate"    ] = $eventDate;
			$this->db->statuslocation->append($toUpdate);
		}

		$this->db->push();
		if($this->db->error()){
			$this->vb->error("The equipment entry could not be updated!", true);
			return false;
		}

		$this->vb->success("The equipment entry has successfully been updated!");
		return true;
	}
}

$page = new DisplayEquipPage($this, "displayequip");

?>
