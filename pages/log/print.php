<?php

require_once "library/page.php";
require_once "pages/all/ranges.php";



// Print Page
// ============================================
class PrintPage extends Page {


	// public members and methods
	// ========================================

	// members
	// ---------------------------------------- 
	public $items  = array();
	public $faulty = 0;

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->items = array();
		$this->loadDbItems();
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		$res = false;
		rangesLoadItems($this, 1);
		if     ($this->post["do"]=="add"    ) $res = rangesSubmitAddMode1($this);
		else if($this->post["do"]=="go"     ) $res = rangesSubmitGo($this, true);
		else if($this->post["do"]=="clr"    ) $res = $this->submitClear();
		else if($this->post["do"]=="saveNew") $res = $this->submitPrinterNew(); // saves new printer
		else if($this->post["do"]=="saveMod") $res = $this->submitPrinterMod(); // modify existing printer
		else if($this->post["do"]=="def"    ) $res = $this->submitPrinterDef(); // set printer as default
		else if($this->post["do"]=="csv"    ) $this->submitExportCsv();         // export sticker table to csv
		else if($this->post["do"]=="pdf"    ) $this->submitExportPdf();         // export sticker table to pdf

		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}



	// public methods for storing ranges in DB (invoked by ranges.php)
	// ========================================

	// getDbItems
	// ---------------------------------------- 
	public function getDbItems() {
		/* Retrieves the sticker entries from DB */
		$stickers = $this->db->readTable("stickers", array("eqentryid"), array("userid"=>$this->globals["userId"]));
		$eqIds    = array();
		foreach($stickers as $row) array_push($eqIds, $row->eqentryid);
		return $eqIds;
	}

	// loadDbItems
	// ---------------------------------------- 
	private function loadDbItems() {
		/* Loads the sticker entries from DB; only run once when invoking the page via menu */
		$eqIds = $this->getDbItems();
		rangesSubmitAddMode1($this, $eqIds);
	}

	// removeDbItems
	// ---------------------------------------- 
	public function removeDbItems($eqIds) {
		/* Remove the entries from the DB */
		if(count($eqIds)==0) return;
		$stickers = $this->db->stickers;
		$c = new DbConfig($this->master, "removeItems");
		$c->select("eqentryid", $eqIds);
		$stickers->delete($c);
		$stickers->push();
		$stickers->rebase();
		return true;
	}

	// storeDbItems
	// ---------------------------------------- 
	public function storeDbItems($eqIds) {
		/* Stores the sticker entries in the DB */
		if(count($eqIds)==0) return;
		$stickers = $this->db->stickers;
		foreach($eqIds as $eqId)
			$stickers->append(array("eqentryid"=>$eqId, "userid"=>$this->globals["userId"], "websiteusercr"=>$this->globals["username"])); 
		$stickers->push();
	}



	// private members and methods
	// ========================================

	// createPrinter
	// ---------------------------------------- 
	private function createPrinter($name, $sizeId, $marginTop, $marginBottom, $marginLeft, $marginRight, $asDef=false){
		/* Inserts a new printer to the data base */	

		if($asDef){
			$this->db->readTable("printers", array(), array("ownerid"=>$this->globals["userId"]));
			foreach($this->db->printers as $row) $row->persdefault = "F";
		}

		$this->db->printers->append(array("ownerid"      =>$this->globals["userId"],
		                                  "printername"  =>$name,
		                                  "stickersize"  =>$sizeId,
		                                  "topmargin"    =>$marginTop,
		                                  "bottommargin" =>$marginBottom,
		                                  "leftmargin"   =>$marginLeft,
		                                  "rightmargin"  =>$marginRight,
		                                  "persdefault"  =>$asDef ? "T" : "F",
		                                  "websiteusercr"=>$this->globals["username"]));
		$this->db->printers->push();
		if($this->db->error()){
			$this->vb->error("Cannot add printer ".$name."!", true);
			return -1;
		}
		return $this->db->printers->id_printers;
	}


	// exportData
	// ---------------------------------------- 
	private function exportData(){
		/* creates the txt from which all other exports are derived */

		$printer = $this->db->readTable("printers", array("stickersize"), array("id_printers"=>$this->post["printer"]));
		$ss      = $printer->stickersize;

		// all eqtypegroupcodes
		$eqgroups = array();
		$eq       = $this->db->readTable("eqtypegroupconts", array("eqtypegroupcode", "eqtypecode"), array(), array(), array(), true);
		foreach($eq->slim as $row) $eqgroups[$row["eqtypecode"]] = $row["eqtypegroupcode"];

		$f = fopen($this->master->downloadPath."/print_export.txt", "w");
		foreach($this->items as $idx=>$range){
			if(!in_array($idx+1, $this->post["subRanges"])) continue;
			foreach($range as $item){
				if($ss==3)
					fwrite($f, $item->mtfid.";\n");
				else{
					if(!array_key_exists($item->eqtypecode, $eqgroups) || $eqgroups[$item->eqtypecode] != "CABL")
						fwrite($f, sprintf("%s; %s\n", $item->mtfid, $item->otherid));
					else {
						$line = sprintf("%s; %s; %s __ %s\n", $item->mtfid, $item->eqtypename, $item->parstart, $item->parend);
						if(in_array($ss, array(4, 5, 6))){
							fwrite($f, $line);
							fwrite($f, $line);
							fwrite($f, $line);
						}
						else if($ss == 7){
							fwrite($f, $line."\n");
							fwrite($f, $line);
							fwrite($f, $line."\n");
						}
						else
							fwrite($f, $line);
					}
				}
			}
		}
		fclose($f);
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Generates the form and fills it with the requested information */

		// the entire ranges part
		$this->html->set("addhint", $this->html->template("print_headHint"));
		rangesLoadPage($this, true, false, "Access Type: Print Stickers", false, false, 1);


		// check if printers are available, otherwise insert them
		$printers = $this->db->readTable("printers", array("id_printers"), array("ownerid"=>$this->globals["userId"]));
		if($printers->count()==0){
			$sticksizes = $this->db->readTable("stickersizes", array("id_stickersizes", "stickersizename", "topoutermargin", "leftoutermrgin", "rightoutermargin", "bottomoutermargin"), array(), array(), array("id_stickersizes"=>"asc"));
			$i=0;
			foreach($sticksizes as $sticker)
				$this->createPrinter($sticker->stickersizename, $sticker->id_stickersizes, $sticker->topoutermargin, $sticker->bottomoutermargin, $sticker->leftoutermargin, $sticker->rightoutermargin, $i==0);
		}

		$maxSize = $this->db->readTable("printers", array("id_printers"), array("ownerid"=>$this->globals["userId"]))->count();


		// list of printers
		$printers = $this->db->readView("prtview", array("pr.stickersize = ss.id_stickersizes"), 
		                                           array("pr.id_printers", "pr.ownerid", "pr.printername", "pr.stickersize", "ss.width", "ss.height",
		                                                 "ss.xmarg", "ss.ymarg", "pr.topmargin", "pr.leftmargin", "pr.rightmargin", "pr.bottommargin",
		                                                 "ss.islandscapeflag", "pr.persdefault"), array("pr.ownerid"=>$this->globals["userId"]), array(),
		                                           array("pr.persdefault"=>"desc", "pr.id_printers"=>"asc"));
		$def  = 0;
		$opts = array();
		foreach($printers as $row){
			$item = sprintf("%s [ size: %d ] [ margins: %4.1f top, %4.1f left, %4.1f right, %4.1f bottom ]", $row->pr_printername, $row->pr_stickersize,
			                $row->pr_topmargin, $row->pr_leftmargin, $row->pr_rightmargin, $row->pr_bottommargin);
			$item .= $row->ss_islandscapeflag=="T" ? " [ landscape ]" : " [ portrait ]";
			$opts[$row->pr_id_printers] = $item;
			if($row->pr_persdefault=="T") $def = $row->id_printers;
		}

		$this->html->set("options_printer", $this->html->makeOptions($opts, array_key_exists("printer", $this->post) ? $this->post["printer"] : $def));


		// show printer form
		if(array_key_exists("do", $this->post) && in_array($this->post["do"], array("new", "mod"))){
			if($this->post["do"]=="mod"){
				$printers = $this->db->readTable("printers", array(), array("id_printers"=>$this->post["printer"]));
				$this->html->set("prtUpdate"     , $this->post["printer"] );
				$this->html->set("prtName"       , $printers->printername );
				$this->html->set("prtSize"       , $printers->stickersize );
				$this->html->set("maxSize"       , $maxSize               );
				$this->html->set("margTop"       , $printers->topmargin   );
				$this->html->set("margBottom"    , $printers->bottommargin);
				$this->html->set("margLeft"      , $printers->leftmargin  );
				$this->html->set("margRight"     , $printers->rightmargin );
				$this->html->set("checked_prtDef", $printers->persdefault=="T" ? "checked" : "");
				$this->html->set("prtButton"     , "saveMod"              );
			}
			else
				$this->html->set("prtButton"  , "saveNew");
			$this->html->set("printerOpts", $this->html->template("print_printer"));
		}


		// ------ the template ------
		return $this->html->template("print");
	}

	// submitClear
	// ---------------------------------------- 
	private function submitClear(){
		/* Clears the entire table */
		$eqids = array();
		foreach($this->items as $idx=>$range){
			foreach($range as $item)
				array_push($eqids, $item->eqid);
		}
		$this->items = array(); 
		return $this->removeDbItems($eqids);
	}

	// submitExportCsv
	// ---------------------------------------- 
	private function submitExportCsv() {
		/* Exports the list to CSV */

		$this->exportData();
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment;filename=stickers.csv");
		readfile($this->master->downloadPath."/print_export.txt");
		exit();
	}

	// submitExportPdf
	// ---------------------------------------- 
	private function submitExportPdf() {
		/* Exports the list to PDF */

		$this->exportData();

		$printer = $this->db->readView("prtview", array("pr.stickersize = ss.id_stickersizes"), 
		                                          array("pr.id_printers", "pr.ownerid", "pr.printername", "pr.stickersize", "ss.width", "ss.height",
		                                                "ss.xmarg", "ss.ymarg", "pr.topmargin", "pr.leftmargin", "pr.rightmargin", "pr.bottommargin",
		                                                "ss.islandscapeflag", "pr.persdefault"), array("pr.id_printers"=>$this->post["printer"]), array(),
		                                          array("pr.persdefault"=>"desc", "pr.id_printers"=>"asc"));

		$sizem = str_replace(" ","", sprintf("%4.1f,%4.1f", $printer->ss_xmarg, $printer->ss_ymarg));
		$setup = sprintf("%dx%d", $printer->ss_width, $printer->ss_height);
		$marginLeft   = sprintf("%4.1f", $printer->pr_leftmargin  );
		$marginTop    = sprintf("%4.1f", $printer->pr_topmargin   );
		$marginBottom = sprintf("%4.1f", $printer->pr_bottommargin);
		$marginRight  = sprintf("%4.1f", $printer->pr_rightmargin );
		$cmd = $printer->ss_islandscapeflag == "T" ? "MakeLsPDF" : "MakePDF";

		$script = $this->html->template("print_script", array("path"        =>$this->master->downloadPath,
		                                                      "cmd"         =>$cmd,
		                                                      "sizem"       =>$sizem,
		                                                      "setup"       =>$setup,
		                                                      "marginLeft"  =>$marginLeft,
		                                                      "marginTop"   =>$marginTop,
		                                                      "marginBottom"=>$marginBottom,
		                                                      "marginRight" =>$marginRight));
		$return = shell_exec($script);

		header("Content-Type: application/pdf");
		header("Content-disposition: attachment;filename=stickers.pdf");
		readfile($this->master->downloadPath."/output.pdf");
		unlink  ($this->master->downloadPath."/output.ps");
	}
   
	// submitPrinterDef
	// ---------------------------------------- 
	private function submitPrinterDef() {
		/* Sets the selected printer as default */
		$this->db->readTable("printers", array(), array("ownerid"=>$this->globals["userId"]));
		foreach($this->db->printers as $row) $row->persdefault = "F";
		$this->db->printers->push();
		$printers = $this->db->readTable("printers", array(), array("id_printers"=>$this->post["printer"]));
		$printers->persdefault = "T";
		$printers->push();
		if($this->db->error()){
			$this->vb->error("Cannot update printer ".$printers->printername."!", true);
			return false;
		}
		return true;
	}

	// submitPrinterMod
	// ---------------------------------------- 
	private function submitPrinterMod() {
		/* Modifies an existing printer */
		$asDef = isset($this->post["prtDef"]) && $this->post["prtDef"]==1;
		if($asDef){
			$this->db->readTable("printers", array(), array("ownerid"=>$this->globals["userId"]));
			foreach($this->db->printers as $row) $row->persdefault = "F";
			$this->db->printers->push();
		}
		$printers = $this->db->readTable("printers", array(), array("id_printers"=>$this->post["prtUpdate"]));
		$printers->printername  = $this->post["prtName"   ];
		$printers->stickersize  = $this->post["prtSize"   ];
		$printers->topmargin    = $this->post["margTop"   ];
		$printers->bottommargin = $this->post["margBottom"];
		$printers->leftmargin   = $this->post["margLeft"  ];
		$printers->rightmargin  = $this->post["margRight" ];
		$printers->persdefault  = $asDef ? "T" : "F";
		$printers->push();
		if($this->db->error()){
			$this->vb->error("Cannot update printer ".$printers->printername."!", true);
			return false;
		}
		return true;
	}

	// submitPrinterNew
	// ---------------------------------------- 
	private function submitPrinterNew() {
		/* Adds a new printer */
		return $this->createPrinter($this->post["prtName"], $this->post["prtSize"], $this->post["margTop"], $this->post["margBottom"], 
		                            $this->post["margLeft"], $this->post["margRight"], isset($this->post["prtDef"]) && $this->post["prtDef"]==1)>0; 
	}

}

$page = new PrintPage($this, "print");

?>
