<?php

require "library/page.php";


// News Page
// ============================================
class NewsPage extends Page {


	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		return $this->html->template("news");
	}
}

$page = new NewsPage($this, "news");


?>
