<?php

require_once "library/page.php";


// insertRows
// -------------------------------------------- 
function insertRows($page, $tablename, $toInsert){
	/* A little helper function necessary for the 
	* independent splitting */
	$table = $page->db->get($tablename);
	foreach($toInsert as $row) $table->append($row);
	$table->push();
	if($page->db->error()) return false;
	return true;
}


// splittingNormal
// -------------------------------------------- 
function splittingNormal($page, $parEqId, $items, $eventDateDb, $locattr=NULL, $statattr=NULL){
	/* Copies all info from the parent equipment (group) to 
	* the individual items ($items = list of eqIds) in the
	* context of the normal splitting */

	// FIXME: do statloc checks here too or only at group/batches page?
	if(empty($parEqId)) return false;

	if(empty($locattr )) $locattr  = $page->master->getEqLocation($parEqId, array("sl.statusid", "sl.minorlocdesc", "sl.isvalidflag", "eventdate"));
	if(empty($statattr)) $statattr = $page->master->getEqStatus  ($parEqId, array("isvalidflag", "sl.eventdate"));

	$statloc  = $page->db->statuslocation;
	$heritage = $page->db->batchheritage;

	// a single entry
	if(timestamp(0, $statattr->sl_eventdate)==timestamp(0, $locattr->eventdate)){
		foreach($items as $eqId)
			$statloc->append(array("eqentryid"    =>$eqId,
			                       "statusid"     =>$statattr->sl_statusid, 
			                       "majorlocid"   =>$locattr->sl_majorlocid, 
			                       "minorlocdesc" =>$locattr->sl_minorlocdesc,
			                       "isvalidflag"  =>$statattr->sl_isvalidflag,
			                       "websiteusercr"=>$page->globals["username"],
			                       "websiteusered"=>$page->globals["username"],
			                       "eventdate"    =>dbStringDate($statattr->sl_eventdate)));
	}

	// separate entries
	else {
		foreach($items as $eqId){
			$statloc->append(array("eqentryid"    =>$eqId,
			                       "statusid"     =>$statattr->sl_statusid, 
			                       "isvalidflag"  =>$statattr->sl_isvalidflag,
			                       "websiteusercr"=>$page->globals["username"],
			                       "websiteusered"=>$page->globals["username"],
			                       "eventdate"    =>dbStringDate($statattr->sl_eventdate)));
			$statloc->append(array("eqentryid"    =>$eqId,
			                       "majorlocid"   =>$locattr->sl_majorlocid, 
			                       "minorlocdesc" =>$locattr->sl_minorlocdesc,
			                       "isvalidflag"  =>$locattr->sl_isvalidflag,
			                       "websiteusercr"=>$page->globals["username"],
			                       "websiteusered"=>$page->globals["username"],
			                       "eventdate"    =>dbStringDate($locattr->eventdate)));
		}
	}
	$page->db->statuslocation->push();
	if($page->db->error()) return false;


	// create batch heritage links
	foreach($items as $eqId){
		$heritage->append(array("eqentryid"            =>$eqId,
		                        "parentbatcheqentryid" =>$parEqId,
		                        "websiteusercr"        =>$page->globals["username"],
		                        "websiteusered"        =>$page->globals["username"],
		                        "eventdate"            =>$eventDateDb));
		$heritage->push();
		if($page->db->error()) return false;
	}

	return true;

}



// splittingIndependent
// -------------------------------------------- 
function splittingIndependent($page, $parEqId, $items, $statloc=NULL, $parequipment=NULL, $pareqdetails=NULL, $pareqcomments=NULL, $pardoclinks=NULL){
	/* Copies all info from the parent equipment (group) to 
	* the individual items ($items = list of eqIds) in the
	* context of the independent splitting */

	// FIXME: do statloc checks here too or only at group/batches page?
	if(empty($parEqId)) return false;	

	// statloc info of parent
	if(empty($statloc)) $statloc = $page->db->readTable("statuslocation", array("statusid", "majorlocid", "minorlocdesc", "isvalidflag", "websiteusercr", "eventdatedb"),
	                                                  array("eqentryid"=>$parEqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"), array("eventdate"=>"asc"));
	$statIdFullySplitUp = array_search("fully split up", $page->master->getOptionsStatus());
	$parstatloc = array(); // no DbTable anymore (no problem with overwriting)!
	foreach($statloc as $row){
		if($row->statusid==$statIdFullySplitUp) continue; // discard fully split up
		array_push($parstatloc, $row);
	}

	// equipment info of parent
	if(empty($parequipment)) $parequipment  = $page->db->readTable("equipment"       , array("*"), array("id_equipment"=>$parEqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"));
	if(empty($pareqdetails)) $pareqdetails  = $page->db->readTable("equipmentdetails", array("*"), array("eqentryid"   =>$parEqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb",
	                                                                                                             "TO_CHAR(manufacturingdate, 'YYYY-MM-DD HH24:MI:SS')"=>"manufacturingdatedb",
	                                                                                                             "TO_CHAR(deliverydate     , 'YYYY-MM-DD HH24:MI:SS')"=>"deliverydatedb",
	                                                                                                             "TO_CHAR(ordereddate      , 'YYYY-MM-DD HH24:MI:SS')"=>"ordereddatedb"));

	// comments and docs for parents
	if(empty($pareqcomments)) $pareqcomments = $page->db->readTable("eqcomments"      , array("*"), array("eqentryid"   =>$parEqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"));
	if(empty($pardoclinks  )) $pardoclinks   = $page->db->readTable("doclink"         , array("*"), array("eqentryid"   =>$parEqId), array("TO_CHAR(eventdate, 'YYYY-MM-DD HH24:MI:SS.FF6')"=>"eventdatedb"));


	// copy data from parent to every item
	$insertStatLoc = array();
	$insertEqData  = array();
	$insertCom     = array();
	$insertDoc     = array();
	foreach($items as $eqId){

		// statuslocation
		foreach($parstatloc as $sl){
			$existing = $page->db->readTable("statuslocation", array("eqentryid"), 
			                                                   array("eqentryid"   =>$eqId,
			                                                         "statusid"    =>$sl->statusid, 
			                                                         "majorlocid"  =>$sl->majorlocid, 
			                                                         "minorlocdesc"=>$sl->minorlocdesc,
			                                                         "isvalidflag" =>$sl->isvalidflag));
			if($existing->count()>=1) continue;
			array_push($insertStatLoc, array("eqentryid"    =>$eqId,
			                                 "statusid"     =>$sl->statusid, 
			                                 "majorlocid"   =>$sl->majorlocid, 
			                                 "minorlocdesc" =>$sl->minorlocdesc,
			                                 "isvalidflag"  =>$sl->isvalidflag,
			                                 "websiteusercr"=>$sl->websiteusercr,
			                                 "websiteusered"=>$sl->websiteusered,
			                                 "eventdate"    =>dbStringDate(timestamp(3, $sl->eventdatedb))));
		}


		// equipment details
		$toInsert = array("eqentryid"        =>$eqId,
		                  "supplier"         =>$pareqdetails->supplier,
		                  "cablelength"      =>$pareqdetails->cablelength,
		                  "orderedby"        =>$pareqdetails->orderedby,
		                  "orderreference"   =>$pareqdetails->orderreference,
		                  "websiteusercr"    =>$pareqdetails->websiteusercr,
		                  "websiteusered"    =>$pareqdetails->websiteusered,
		                  "eventdate"        =>dbStringDate(timestamp(3, $pareqdetails->eventdatedb)));
		if(isValidDate($pareqdetails->manufacturingdatedb)) $toInsert["manufacturingdate"] = dbStringDate(timestamp(3, $pareqdetails->manufacturingdatedb));
		if(isValidDate($pareqdetails->deliverydatedb     )) $toInsert["deliverydate"     ] = dbStringDate(timestamp(3, $pareqdetails->deliverydatedb     ));
		if(isValidDate($pareqdetails->ordereddatedb      )) $toInsert["ordereddate"      ] = dbStringDate(timestamp(3, $pareqdetails->ordereddatedb      ));
		array_push($insertEqData, $toInsert);


		// comments
		foreach($pareqcomments as $com){
			array_push($insertCom, array("eqentryid"    =>$eqId,
			                             "typeofcomment"=>$com->typeofcomment,
			                             "eqcomment"    =>$com->eqcomment,
			                             "shipid"       =>$com->shipid,
			                             "isvalidflag"  =>$com->isvalidflag,
			                             "websiteusercr"=>$com->websiteusercr,
			                             "websiteusered"=>$com->websiteusered,
			                             "eventdate"    =>dbStringDate(timestamp(3, $com->eventdatedb))));
		}

		// documents
		foreach($pardoclinks as $doc){
			array_push($insertDoc, array("eqentryid"    =>$eqId,
			                             "docid"        =>$doc->docid,
			                             "shipid"       =>$doc->shipid,
			                             "isvalidflag"  =>$doc->isvalidflag,
			                             "websiteusercr"=>$doc->websiteusercr,
			                             "websiteusered"=>$doc->websiteusered,
			                             "eventdate"    =>dbStringDate(timestamp(3, $doc->eventdatedb))));
		}
	}

	// writing data all together
	if(!insertRows($page, "statuslocation"  , $insertStatLoc)) return false;
	if(!insertRows($page, "equipmentdetails", $insertEqData )) return false;
	if(!insertRows($page, "eqcomments"      , $insertCom    )) return false;
	if(!insertRows($page, "doclink"         , $insertDoc    )) return false;

	return true;
}


?>
