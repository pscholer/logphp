<?php

require_once "library/database.php";

// addComment
// -------------------------------------------- 
function addComment($page, $comment, $typeId, $evDate, $eqids, $shipids=array(), $defType="other", $useDate=true, $talk=true){
	/* Inserts a new comment stored in a form element with name $name
	* using a fixed typename ($typeofdoc) and a list of eqids
	* to link it to */

	$com = trim($comment);
	if(empty($com)) return true;

	$comtype = $defType;
	if(isset($typeId)){
		if(gettype($typeId)=="integer" && $typeId>=0) 
			$comtype = $page->master->doctypes[$typeId];
		if(gettype($typeId)=="string" && !empty($typeId))
			$comtype = $typeId;
	}

	$eventDate = isValidDate($evDate) ? dbStringDate($evDate) : $page->master->eventDateDb;

	$values = array("typeofcomment"=>$comtype,
	                "eqcomment"    =>$com,
	                "websiteusercr"=>$page->globals["username"],
	                "websiteusered"=>$page->globals["username"]);
	if($useDate) $values["eventdate"] = $eventDate;
	    
	foreach($eqids as $eqid){
		if(!empty($shipids) && array_key_exists($eqid, $shipids)) $values["shipid"] = $shipids[$eqid];
		$values["eqentryid"] = $eqid;
		$page->db->eqcomments->append($values);
	}

	$page->db->push();
	if($page->db->error()){
		if($talk) $page->vb->error("The comment could not be written!", true);
		return false;
	}

	if($talk) $page->vb->success("Comment written successfully!");
	return true;
}

// downloadDoc
// -------------------------------------------- 
function downloadDoc($page, $table="documents", $colid="id_documents", $colblob="docloc", $colname="docname"){
	/* Downloads a single document according to the docId */

	if(empty($page->post["docId"])) return;

	$doctable = $page->db->readTable($table, array($colname, $colblob),
	                                         array($colid=>$page->post["docId"]));
	$file = new DbFile($page->db, $doctable->$colname);
	$file->field = $doctable->getField($colblob);

	if($file->drop($page->master->downloadPath))
		$page->vb->success(sprintf("Document %s available for download <a href='%s' class='back' target='_blank'>here</a>.", $doctable->$colname, $file->tmp));
	else
		$page->vb->error(sprintf("Document %s could not be downloaded!", $doctable->$colname));

	//FIXME: alternatively:
	//header("Content-Type: image/png"); // here we would need to figure what type it is
	//header("Content-disposition: attachment;filename=image.png");
	//readfile($file->tmp);
}


// pageSelector
// -------------------------------------------- 
function pageSelector($page, $config){
	/* Build the page selector, i.e. the seggregation of all selected
	* rows into individual pages. In particular, this method does
	* (1) update the config with the appropriate limit attributes,
	* (2) build the page selector HTML code that is to be displayed
	* on top of the actual table. */

	$tot = $page->db->numConfig($config);

	$entries = isset($page->post["nEntries"]) && !empty($page->post["nEntries"]) ? $page->post["nEntries"] : $page->master->config->pageSize->value;
	$idx     = isset($page->post["p"]       ) && !empty($page->post["p"       ]) ? $page->post["p"       ] : 0;
	if(isset($page->post["nEntriesPrev"]) && $page->post["nEntriesPrev"]!=$page->post["nEntries"]) $idx = 0; // first page when changing number of entries per page

	if(isset($page->post["showAll"]) && $page->post["showAll"] == 1) {
		$idx     = 0;
		$entries = $tot;
	}

	$start = $idx*$entries;
	if($entries>0) $config->limit($entries, $start);

	$ps   = floor($tot/$entries)+($tot%$entries>0 ? 1 : 0);

	// shorthand
	$psa2 = array();
	if($idx>0) {
		array_push($psa2, sprintf("<a href='#' onclick='changePage(%d);return false'>%s</a>", 0     , "first"  ));
		array_push($psa2, sprintf("<a href='#' onclick='changePage(%d);return false'>%s</a>", $idx-1, "&laquo;"));
	}
	array_push($psa2, "<b>".$idx."</b>");
	if($idx+1<$ps) {
		array_push($psa2, sprintf("<a href='#' onclick='changePage(%d);return false'>%s</a>", $idx+1, "&raquo;"));
		array_push($psa2, sprintf("<a href='#' onclick='changePage(%d);return false'>%s</a>", $ps-1 , "last"   ));
	}
	$pages2 = implode(" | ", $psa2);

	// all pages
	$psa1 = array();
	for($p=0;$p<$ps;++$p)
		array_push($psa1, $p);
	$pages1 = $page->html->makeSelect("pageSel", $psa1, $idx, array(), array(), false, 'changePage(this.value)', "class='buttonlike flex'");
	//for($p=0;$p<$ps;++$p){
	//	if($p==$idx){
	//		array_push($psa1, sprintf("<b>%d</b>", $p));
	//		continue;
	//	}
	//	array_push($psa1, sprintf("<a href='#' onclick='changePage(%d);return false'>%d</a>", $p, $p));
	//}

	$page->html->set("pageSelector", $page->html->template("pageSelector", array("nTotal"  =>$tot,
	                                                                             "nEntries"=>$entries,
	                                                                             "pages1"  =>$pages1,
	                                                                             "pages2"  =>$pages2), NULL, "all"));
	
	return $start;
}


// uploadDoc
// -------------------------------------------- 
function uploadDoc($page, $docname, $typeId, $evDate, $eqids, $shipids=array(), $defType="other", $useDate=true, $talk=true){
	/* Uploads a document from a form element with name $name
	* using a fixed typename ($typeofdoc) and a list of eqids
	* to link it to */
	
	if($_FILES[$docname]['error']==4)
		return true;

	if($_FILES[$docname]['size']==0 || !is_uploaded_file($_FILES[$docname]['tmp_name']) || $_FILES[$docname]['error']!=0){ 
		if($talk) $page->vb->error("Document ".$_FILES[$docname]['name']." could not be uploaded!");
		return false;
	}

	$newdoc = new DbFile($page->db);
	$newdoc->seize($_FILES[$docname]);

	$doctype = $defType;
	if(isset($typeId)){
		if(gettype($typeId)=="integer" && $typeId>=0)
			$doctype = $page->master->doctypes[$typeId];
		if(gettype($typeId)=="string" && !empty($typeId))
			$doctype = $typeId;
	}

	$eventDate = isValidDate($evDate) ? dbStringDate($evDate) : $page->master->eventDateDb;

	// doc
	$values = array("fileformat"   =>$newdoc->type,
	                "typeofdoc"    =>$doctype,
	                "docname"      =>$newdoc->name,
	                "docloc"       =>$newdoc,
	                "websiteusercr"=>$page->globals["username"],
	                "websiteusered"=>$page->globals["username"]);
	if($useDate) $values["eventdate"] = $eventDate;

	$page->db->documents->append($values);
	$page->db->push();
	if($page->db->error()){
		if($talk) $page->vb->error("Document ".$newdoc->name." could not be uploaded!", true);
		return false;
	}

	// doclink
	$values = array("docid"        =>$page->db->documents->id_documents,
	                "websiteusercr"=>$page->globals["username"],
	                "websiteusered"=>$page->globals["username"]);
	if($useDate) $values["eventdate"] = $eventDate;

	foreach($eqids as $eqid){
		if(!empty($shipids) && array_key_exists($eqid, $shipids)) $values["shipid"] = $shipids[$eqid];
		$values["eqentryid"] = $eqid;
		$page->db->doclink->append($values);
	}

	$page->db->push();
	if($page->db->error()){
		if($talk) $page->vb->error("Document ".$newdoc->name." could not be uploaded!", true);
		return false;
	}

	if($talk) $page->vb->success("Document ".$newdoc->name." uploaded successfully!");
	return true;
}


?>
