<div class="bbox simple">
	<div class="border padd6x12">
		<div class="batchesQuantRow">
			<label class="batchesQuantQuant">Total quantity of Units:</label>
			<input class="invisiblelike batchesQuantInput" id="batchesDividedUnits"     name='divUnits' type="text" readonly value="$divUnits" pattern='$patternInt'>
			<label class="batchesQuantUnit">$quantityUnit</label>
			<span class="slash">/</span>                                          
			<label class="batchesQuantGood">Total quantity of Good Units:</label>
			<input class="invisiblelike batchesQuantInput" id="batchesDividedGoodUnits" name='divGoodUnits' type="text" readonly value="$divGoodUnits" pattern='$patternInt'>
			<label class="batchesQuantUnit">$quantityGoodUnit</label>
		</div>
		<div id="batchesQuantEnd" class="batchinatt">
			<span id='batchesLeftU'     class="$subbatchclass1">$subbatchmsg1</span>
			<span id='batchesLeftGoodU' class="$subbatchclass2">$subbatchmsg2</span>
		</div>
	</div>
</div>
