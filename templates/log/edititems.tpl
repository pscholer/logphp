<link rel="stylesheet" href="stylesNew/log/edititems.css">
$header
<div class='bbox simple'>
	<div class='border padd6x12'>
		Select and edit attribute<span class='mandatory'>*</span>:<br />
		<div class="column edititemsThreeQuarters">
			<div class="wfull">
				<div class="column wthird left">
					<select class='buttonlike flex' name='attId' onchange='submitIt("attr")'>
						<option value='-1' disabled $options_choose>Choose attribute</option>
						$options_selectatt
					</select>
				</div>
				<div class="column wthird left" >$furtherInput</div>
				<div class="column wthird right">$furtherInputAdd</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="wfull right">
				$eventDateField
			</div>
		</div>
		<div class="column wquarter hfull center fright">&nbsp;$saveButton</div>
		<div class="clear"></div>
	</div>
</div>	
