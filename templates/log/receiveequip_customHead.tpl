<div class='bbox simple'>
	<div class='border padd6x12'>Access Type: Receive Equipment</div>	
	<div class='border padd6x12 topm6'>
		Select shipment to receive:<br />
		<div class='column whalf left'>
			<label>Shipment-ID:</label>
			<input class='invisiblelike' name='shipIntId' type='number' 
			            value='$shipIntId' min='1' max='99999' placeholder='#####'><br />
		</div>
		<div class='column wthird left'>
			<button type="button" class='buttonlike flex' onclick='clearTrans($usersLocation);submitIt("loadShip")'>Load Shipment</button>
		</div>
		<div class='clear'></div>
        The last shipment was #$lastShipId from $lastShipFrom to $lastShipTo.
		<br />
		<br />
		- OR -
		<br />
		<br />
		show all equipment in transition:<br />
		<div class='column wthird left'>
			<label>sent from:</label>
			<select class="buttonlike flex" id='siteFromId' name='siteFromId'>
				$options_siteFromId
			</select>
		</div>
		<div class='column wthird left'>
			<label>sent to:</label>
			<select class="buttonlike flex" id='siteToId' name='siteToId'>
				$options_siteToId
			</select>
		</div>
		<div class='column wthird left fright'>                          
			<button class='buttonlike flex' onclick='clearShipId();submitIt("loadTrans")'>Load Equipment</button>
		</div>
		<div class='clear'></div>
	</div>
</div>
