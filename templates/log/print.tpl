<link rel="stylesheet" href="stylesNew/log/print.css">
$header
<div class='bbox simple'>
	<div class='bbox padd6x12'>
		Choose a printer:
		<!---<div class='caption'>choose a printer</div>-->
		<select class='invisiblelike general' name='printer' id='printer' onchange='changecolor("printer")'>
			$options_printer
		</select>
		<button type="button" class='buttonlike flex' onclick="submitIt('new')">add new printer</button> 
		<button type="button" class='buttonlike flex' onclick="submitIt('mod')">modify selected printer</button> 
		<button type="button" class='buttonlike flex' onclick="submitIt('def')">set selected printer as default</button> 
		<div class='clear'></div>
	</div>
</div>
$printerOpts
<div class='bbox simple'>
	<div class='bbox padd6x12'>
		With the selected elements:
		<button type="button" class='buttonlike flex' onclick="if(selectAll('subRanges[]')){submitIt('pdf');}">download printable PDF</button>
		or 
		<button type="button" class='buttonlike flex' onclick="if(selectAll('subRanges[]')){submitIt('csv');}">download CSV</button> 
	</div>
</div>	
<!--
<div class='bbox simple'>
	<div class='bbox padd6x12'>
		<input type="checkbox" name="toggle" onclick="toggleAll('subRanges[]', this)"> select / unselect all elements in list<br />
		For the selected elements:
		<button class='buttonlike flex' onclick="submitIt('pdf')">download printable PDF</button> 
		<button class='buttonlike flex' onclick="submitIt('csv')">download CSV</button> 
		<button class='buttonlike flex' onclick="submitIt('clr')">empty sticker table</button> 
	</div>
</div>
-->
