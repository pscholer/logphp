<input type="hidden" name="buffer"        value="$buffer" >
<input type="hidden" name="bufferIsComb"  value="$bufferIsComb" >
<input type="hidden" name="bufferCaption" value="$bufferCaption">
<input type="hidden" name="bufferLabel"   value="$bufferLabel"  >
<input type="hidden" name="showPlot"      value="">
<input type="hidden" name="showList"      value="">
<div class='bbox simple'>
	<div id='transTable' class='border padd6x12'>
		Available plots and lists:
		<table id='sortable' class='tablesorter floatThead'>
			<colgroup>
				<col width='6%' >
				<col width='50%'>
				<col width='26%'>
				<col width='6%' >
				<col width='6%' >
				<col width='6%' >
			</colgroup>
			<thead>
				<tr>
					<th>#</th>                         
					<th>$bufferCaption</th>
					<th>$bufferLabel</th>
					<th>No.</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				$rows
			</tbody>   
		</table>
	</div>
</div>
<div class='bbox simple'>
	<div class='bbox padd6x12'>
		<button class="buttonlike" onClick ="$('.floatThead').tableExport({type:'csv',escape:'false'});">Download table as <b>CSV</b> file</button>
		<button class="buttonlike" onClick ="$('.floatThead').tableExport({type:'excel',escape:'false'});">Download table as <b>EXCEL</b> file</button>
	</div>
</div>	
