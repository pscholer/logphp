<script type="text/javascript" src="jsNew/log/sendequip.js"></script>      
<link rel="stylesheet" href="stylesNew/log/sendequip.css">
$header
<div class='bbox simple'>
	<div class='border padd6x12'>
		<div class="column whalf left sendequipRow1">
			<label>send from<span class='mandatory'>*</span>:</label>
			<select class='buttonlike flex sendequipSelect' id='siteFrom' name='siteFrom'>
				$options_sitefrom
			</select>
		</div>
		<div class="column whalf left sendequipRow1">
			<label>send to<span class='mandatory'>*</span>:</label>
			<select class='buttonlike flex sendequipSelect' id='siteTo' name='siteTo'
			                   onchange='checkShippingSites()'>
				$options_siteto
			</select>
		</div>
		<div class='clear'></div>
		<div class="column whalf left sendequipRow2">
			<label>shipping date ($placeholderDateTime)<span class='mandatory'>*</span>:</label>
			<input class='invisiblelike sendequipInput' id='dateFrom' name='dateFrom' type='text' 
			          value='$dateFrom' pattern='$patternDateTime' onchange='changecolor("dateFrom")'>
		</div>
		<div class="column whalf left sendequipRow2">
			<label>expected return date ($placeholderDate):</label>
			<input class='invisiblelike sendequipInput' id='dateTo' name='dateTo' type='text' 
			          value='$dateTo' placeholder='$placeholderDate'
                      pattern='$patternDate' onchange='checkShippingDates();changecolor("dateTo");'>
		</div>
		<div class='clear'></div>
		<div class="column wthird left sendequipRow3">
			<label>shipping service provider:</label>
			<input class='invisiblelike sendequipInput' id='shipProv' name='shipProv' type='text' 
			          value='$shipProv' onchange='changecolor("shipProv")' pattern="$patternText">
		</div>
		<div class="column wthird left sendequipRow3">
			<label>person packing the package<span class='mandatory'>*</span>:</label>
			<input class='invisiblelike sendequipInput' id='shipPers' name='shipPers' type='text' 
			          value='$shipPers' onchange='changecolor("shipPers")' pattern="$patternText">
		</div>
		<div class="column wthird left sendequipRow3 fright">
			<label>attach shipping papers (max. 2MB):</label>
			<input id='newPaper' class='invisiblelike' type='file' name='newPaper' 
			                 onchange='changecolor("newPaper")' accept='$allowedUplTypes'>
		</div>
		<div class='clear'></div>
		<div class="column whalf left sendequipRow4">
			<label>shipping reference (URL), if leaving CERN:</label>
			<input class='invisiblelike sendequipInput' id='shipRef' name='shipRef' type='url' 
			          value='$shipRef' onchange='changecolor("shipRef")' placeholder='http://'>
			<br />
			<label>shipping comment:</label>
			<input class='invisiblelike sendequipInput' id='shipCom' name='shipCom' type='text' 
			          value='$shipCom' onchange='changecolor("shipCom")' pattern="$patternText">
		</div>
		<div class="column wthird fright heightAuto middle">
			<button type="button" class='buttonlike' onclick="if(checkSendEquip($faulty) && checkFileSize('newPaper') && selectAll('subRanges[]')){submitIt('send');}">SEND<br />EQUIPMENT</button>
		</div>
		<div class='clear'></div>
	</div>
</div>
