<!DOCTYPE html>
<html lang="en-us">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE">
	<meta charset="UTF-8">
	<title>$title - Time Development Plot</title>
	<link rel="stylesheet" href="../../stylesNew/all.css">
	<link rel="stylesheet" href="../../stylesNew/plots.css">   
	<link rel="stylesheet" href="../../stylesNew/log.css">
	<script type='text/javascript' src='https://www.google.com/jsapi'></script>
	<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
	<script type='text/javascript' src='../../js/tab-ex/xlsx.core.min.js'></script>                                            
	<script type='text/javascript' src='../../js/tab-ex/Blob.min.js'></script>
	<script type='text/javascript' src='../../js/tab-ex/FileSaver.min.js'></script>
	<script type='text/javascript' src='../../js/tab-ex/tableexport.min.js'></script>
	$scriptData
</head>
<body class="plotBody">
<table class="invisiTable" id="$tableId">
	<thead>
		<tr>
			<th colspan="$colspan">$plotTitle ($plotDate)</th>
		</tr>
		<tr>
			$invhead
		</tr>
	</thead>
	<tbody>
		$invbody
	</tbody>
</table>
<div class='plotFrame plotFull'>
	<div id='$canvasId'></div>
	<div class='plotButtons wrap'>
		<div class='fleft'>
			Download Data: 
			<a id='$linkCsv' class='plotLink' href='#'>csv</a>
		</div>
		<div class='fleft plotSep'> | </div>
		<div class='fleft'>
			<a id='$linkXls' class='plotLink' href='#'>excel</a>
		</div>
		<div class='fleft plotSep'> | </div>
		<div class='fleft' id='$png'></div>
	</div>
	<div class='clear'></div>
</div>
$scriptExport
</body>
</html>
