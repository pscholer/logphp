<script type="text/javascript" src="jsNew/log/overview.js"></script>
<link rel="stylesheet" href="stylesNew/log/overview.css">
<input type="hidden" name="predef"     value="$predef"   >
<input type="hidden" name="predefPrev" value="$predef"   >
<input type="hidden" name="doFilter"   value="$doFilter" >
<input type="hidden" name="orderBy"    value="$orderBy"  >
<input type="hidden" name="orderType"  value="$orderType">
<div class="gbox complex">
	<div id="overviewLeft" class="gbox column wsixth">
		Filter Results:
		<div class="gbox column heightAuto">
			<div class="gbox column wfull heightAuto">
				<label class='overviewLabel'>Site</label>
				<select id='filterSiteId' name='filterSiteId' class='invisiblelike $classFilter1' 
                           onchange='changecolor("filterSiteId")'>
					$options_filterSites
				</select>
<!---
				<input type="checkbox" name="filterSiteIdAny" value="1" $filterSiteIdAny> has been at/in at any point
-->
			</div>
			<div class="gbox column wfull heightAuto">
				<label class='overviewLabel'>Status</label>
				<select id='filterStatusId' name='filterStatusId' class='invisiblelike $classFilter2' 
                           onchange='changecolor("filterStatusId")'>
					$options_filterStatus
				</select> 									    
				<input type="checkbox" name="filterStatusIdAny" value="1" $filterStatusIdAny> has had at any point
			</div>									    
			<div class="gbox column wfull heightAuto">
				<label class='overviewLabel'>Conf. Flag</label>
				<select id='filterNcf' name='filterNcf' class='invisiblelike $classFilter5' 
                           onchange='changecolor("filterNcf")'>
					$options_filterNcfs
				</select> 									    
			</div>									    
			<div class="gbox column wfull heightAuto">
				<label class='overviewLabel'>Project</label>
				<select id='filterProjectId' name='filterProjectId' class='invisiblelike $classFilter3' 
                           onchange='changecolor("filterProjectId")'>
					$options_filterProjects
				</select>
			</div>
			<div class="gbox column wfull heightAuto">
				<label class='overviewLabel'>EQ-Type</label>
				<select id='filterTypeId' name='filterTypeId' class='invisiblelike $classFilter4' 
                           onchange='changecolor("filterTypeId")'>
					$options_filterTypes
				</select>
				<input type="checkbox" name="filterTypeIdSort" value="1" $filterTypeIdSort 
                           onchange="submitIt('resort')"> sort by name
			</div>
			<div class="gbox column wfull heightAuto">
				<label class='overviewLabel'>What?</label>
				<select id='whatFilter' name='predef'     class='invisiblelike $classFilter6' 
                           onchange='changecolor("whatFilter")'>
					$options_filterWhats
				</select>
			</div>
		</div>
		<div id="overviewButton" class="grey noborder normal">
			<button type="button" class="buttonlike" onclick='useFilter();submitIt("refresh")'>Apply Filter(s)</button>
		</div>
<!--		<div id="links" class="gbox column hhalf heightAuto">
			- OR -<br />Use Predefined Scheme:
			<ul id="overviewButtons">
				<li><a id="predefHere"  class="leftlink $classPredef1" href="#" onclick="changecolor('predefHere' );submitItAs('predef', 'here' )">Material now here</a></li>
				<li><a id="predefIncom" class="leftlink $classPredef2" href="#" onclick="changecolor('predefIncom');submitItAs('predef', 'incom')">Material incoming</a></li>
				<li><a id="predefOutgo" class="leftlink $classPredef3" href="#" onclick="changecolor('predefOutgo');submitItAs('predef', 'outgo')">Material outgoing</a></li>
				<li><a id="predefOrder" class="leftlink $classPredef4" href="#" onclick="changecolor('predefOrder');submitItAs('predef', 'order')">Material ordered</a></li>
			</ul>
		</div>
--> 
	</div>
	<div id="overviewRight" class="gbox column whalf">
		<div class="bbox simple">  
			<div class='border padd6x12'>
				<button type="button" class="buttonlike" onClick="submitIt('exportCsv')"  >Download Entire Table as<br /><b>CSV</b> File</button>
				<button type="button" class="buttonlike" onClick="submitIt('exportExcel')">Download Entire Table as<br /><b>EXCEL</b> File</button>
			</div>
		</div>
		$pageSelector
		<div class="full">
			<table class="tablesorter floatThead" id='sortable'>
				<thead>
					<tr>
						$table_head
					</tr>
				</thead>
				<tbody>
					$table_body
				</tbody>
			</table>
		</div>
		<table class="invisiTable" id='invisiTable'>
			<thead>
				<tr>
					$invisi_head
				</tr>
			</thead>
			<tbody>
				$invisi_body
			</tbody>
		</table>
	</div>
</div>
