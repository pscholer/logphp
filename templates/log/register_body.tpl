<input type='hidden' name='highestVersion' value='$highestVersion'>
<div class="bbox simple"> 
	<table>
		<tr>
			<td><label>Equipment Type / Version<span class='mandatory'>*</span>:</label></td>
			<td>
				<input id='eqType' style='width:62px;' maxlength='4' name='eqType' type='text' value='$eqType' pattern='$patternType' class="invisiblelike"
				onchange="if(checkEqType()){submitIt('code')}" $eqTypeReadOnly> <button type="button" onclick="if(checkEqType()){submitIt('code')}" $eqTypeDisabled class="buttonlike flex">&raquo;</button> $versionField
			</td>
		</tr>
		$extra
		<tr>
			<td><label>Other ID (Alias):</label></td>
			<td><input class="invisiblelike" name='otherId' type='text' value='$otherId' pattern="$patternText"></td>
		</tr>
		<tr> 
			<td class='helpEvDate'>
				<label>Real Register Date (Event Date): <span class='helpIcon'>?</span></label>
				<div class='legendEvDate'>
					<p>
						Always enter this date, when the date of the event that you want to report about
						is not the same as the date when you are reporting the action.
					</p>
					<p>
						For example: You should have registered something 3 weeks ago,
						but you only now had the time to enter this in the DB. Put here the date of 3 weeks ago,
						so the DB will not get confused if you want to send something last week, that only has been registered now...
					</p>                                    
				</div>                              
			</td>
			<td><input class="invisiblelike" name='eventDate' type='text' value='$eventDate' pattern='$patternDate' placeholder='$placeholderDate'></td>
		</tr>
		<tr>
			<td><label>Status:</label></td>
			<td>$selectStatus</td>
		</tr>
		<tr>
			<td><label>Supplier:</label></td>
			<td><input class="invisiblelike" name='supplier' type='text' value='$supplier' pattern="$patternText"></td>
		</tr>
		<tr>
			<td><label>Cable Length (Meters):</label></td>
			<td><input class="invisiblelike" name='cableLength' type='number' value='$cableLength' placeholder='0.00' min='0' max='999.99' step='0.01'></td>
		</tr>
		<tr>
			<td><label>Delivery Date:</label></td>
			<td><input class="invisiblelike" name='deliveryDate' type='text' value='$deliveryDate' pattern='$patternDate' placeholder='$placeholderDate'></td>
		</tr>
		<tr>
			<td><label>Manufacturing Date:</label></td>
			<td><input class="invisiblelike" name='manufacturingDate' type='text' value='$manufacturingDate' pattern='$patternDate' placeholder='$placeholderDate'></td>
		</tr>
		<tr>
			<td><label>Ordered by:</label></td>
			<td><input class="invisiblelike" name='orderedBy' type='text' value='$orderedBy' pattern="$patternText"></td>
		</tr>
		<tr>
			<td><label>Ordered Date:</label></td>
			<td><input class="invisiblelike" name='orderedDate' type='text' value='$orderedDate' pattern='$patternDate' placeholder='$placeholderDate'></td>
		</tr>
		<tr>
			<td><label>Order Reference (URL):</label></td>
			<td><input class="invisiblelike" name='orderRef' type='url' value='$orderRef' placeholder='http://'></td>
		</tr>
		<tr>
			<td><label>Actual Storage Location<br>(Main Site):</label></td>
			<td>$selectSite</td>
		</tr>
		<tr>
			<td><label>Actual Storage Location<br>(Building or Room):</label></td>
			<td><input class="invisiblelike" name='minorLocDesc' type='text' value='$minorLocDesc' pattern="$patternText"></td>
		</tr>
		<tr>
			<td><label>Comment regarding Registering /<br>Manufacturing / Delivery (optional):</label></td>
			<td><input class="invisiblelike" name='comment' type='text' value='$comment' pattern="$patternText"></td>
		</tr>
		<tr>
			<td><label>Select Document to upload (max. 2MB):</label></td>
			<td><input class="invisiblelike" name='uplnewdoc' type='file' accept='$allowedUplTypes'></td>
		</tr>
		<tr>
			<td><label>Type of Document:</label></td>
			<td>$selectDocType</td>
		</tr>
		<tr>
			<td><label>User Creating this Entry:</label></td>
			<td><input class="invisiblelike" name='username' type='text' value='$username' readonly></td>
		</tr>
		<tr>
			<td><span class='mandatory'>&nbsp;* mandatory</span></td>
			<td><button type="button" class="buttonlike" $activeStateDisabled onclick='if(checkAll() && checkFileSize("uplnewdoc")){submitIt("save")}'>REGISTER $entity</button></td>
		</tr>
	</table>
</div>
