<input type="hidden" name="eqId"  value="$eqId" >
<input type="hidden" name="items" value="$items">
<div class="bbox simple">
	<div class='border white padd6x12'>
		OPTIONAL: Add a child to every item that you just created
	</div>
	<div class="border white padd6x12 topm6">
		<div class='heritageRow1 whalf fleft center border'>
			<label class='invisdesc'>CHILD</label><br />
			<div class='fleft left whalf'>
				<select class='buttonlike full' name='typeIdChild' onchange="toggleId(true)">
					<option value='mtfId'  >MTF - no. ($placeholderMtfId):</option>
					<option value='eqId'   >Unique DB EQ ID:</option>
					<option value='otherId'>EQ ALIAS:</option>
				</select>
				<input class='invisiblelike full' name='idChild' type='text' value='$idChild' pattern="$patternText">
			</div>
			<div class='fleft center whalf'>
				<label class='invisdesc'>SUB BATCH ID:</label><br />
				<input class='invisiblelike full' id='heritageIdChildSub' name='idChildSub' type='text' value='$idChildSub' pattern="$patternInt">
			</div>
		</div>
		<div class='heritageRow1 whalf fleft center border'>
			<label class='invisdesc'>PARENT</label><br />
			<i>(every splitted item)</i>
		</div>
		<div class='clear'></div>
		<div id="heritageRow2" class="wfull border">
			<div id='heritagePosition' class='fleft'>
				<label class='invisdesc'>POSITION:</label><br />
				<input class='invisiblelike full' id='heritPos' name='parPosition' type='text' pattern="$patternText">
			</div>
			<div id='heritageLinks' class='fleft'>
				<label class='invisdesc'>MULTIPLICITY:</label><br />
				<input class='invisiblelike full' id='heritNum' name='parNum' type='text' pattern="$patternInt">
			</div>
			<div id='heritageEvDate' class='fleft'>
				$heritageED
			</div>
			<div id='heritageButton' class='fright'>
				<button type="button" class='buttonlike' onclick='submitIt("parenting")' $saveActive>Add Child to all Items</button>
			</div>
		</div>
		<div class='clear'></div>
		<div id="heritageRow3" class='wfull border'>
			<div class="whalf fleft center">
				<label class='invisdesc'>CABLE &amp; PIPE ENDS:</label><br />
				<input type='radio' name='parService' value='S' checked><label class='heritageCableLabel'>Start</label>
				<input type='radio' name='parService' value='E'        ><label class='heritageCableLabel'>End</label>
			</div>
			<div class="whalf fleft center">
				<label class="invisdesc">FOR SERVICES ONLY!</label>
			</div>
		</div>
	</div>	
</div>
