<input type="hidden" name="mtfs"   value="$mtfs"   />
<input type="hidden" name="others" value="$others" />
<div class="bbox simple">
	<div class="border padd6x12">
		<table class="greytable">
			<thead>
				<tr>
					<th>Equipment MTF ID</th><th>Other ID (EQ ALIAS)</th>
				</tr>
			</thead>
			<tbody>
				$rows
			</tbody>
		</table>
		<p id="groupsAliasHint">
			Please check this preview of your chosen MTF-# / Alias-range carefully. 
			You cannot undo this operation!
		</p>
	</div>
</div>
