<div class="whitediv">
	<div id='newsImg'>
		<img src='img/kids-playing-in-sandbox.png' alt='Sandbox' title='' width='40' height='30'>
	</div>
	<div id='newsTxt'>
		<p>Dear users,</p> 
		<p>we now go into production phase.
		You may still create equipment data here on the playground and test and play. 
		Please make sure that you read and understood all available documentation before 
		editing data on the production database.
		</p>
	</div>
</div>
