<div class="simple bbox">
	<div id="button">
		<button type="button" class="buttonlike" onClick ="submitIt('exportCsv')"   title="Note that this operation can take very long for large tables since the entire table content is retrieved!">Download <b>entire</b> table as <b>CSV</b> file</button>
		<button type="button" class="buttonlike" onClick ="submitIt('exportExcel')" title="Note that this operation can take very long for large tables since the entire table content is retrieved!">Download <b>entire</b> table as <b>EXCEL</b> file</button>
	<!--	<button type="button" class="buttonlike" onClick ="submitIt('exportInvCsv')">Download <b>import template</b> as <b>CSV</b> file</button> -->
	</div>
</div>
