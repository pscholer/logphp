<div class="bbox simple">
	<div class="border padd6x12">
		<p><b>Q: I do not find the information I need. It is scattered over many tables! :( </b></p>
		<p>
			You can ask for special, predefined views of the tables. For example, you might 
			be interested in...
			<br />
			<br />
			<span class="emph">
				"All readout boards, coming from a certain manufacturer (i.e. ELVIA), that did 
				not pass the QA/QC. On the same table there should be shown all the results of 
				the sub-tests of each of these boards."
			</span>
		</p>
		<p>
			To get this view here on the web-view page, you can send me an email 
			(<a href="mailto:kim.temming@physik.uni-freiburg.de">kim.temming@physik.uni-freiburg.de</a>),
			asking for what you need. You should be quite precise in the description of your 
			desired view (see above example!).
		</p>
		<p>
			If you need the data for plotting, it might be wise to specify this. All unwanted 
			columns should be removed from the view then, to simplify the plotting.
		</p>
		<br />
		<p><b>Q: I need information on the import files. How does the structure look and can I produce them myself?</b></p>
		<p>
			Yes, you can. You can download the headers for the csv import files for each table here 
			on the page. Go to "Tables" and select the desired table. Then you can download a csv 
			file ("Download import template as CSV"), which contains already the headers that are 
			needed for this specific table.
		</p>
		<p>
			Just keep the header line in the file and add your data as line two, separated by ";", 
			exactly like in the header. You don't need to fill in all fields, but you always need 
			to fill in the first column, since this is the ID of the item which is described in the 
			line. You may delete columns in the header which you don't need. You also may change 
			the order of the columns, except the first column: This MUST STAY in the first position.
		</p>
		<p>
			This file is then ready for import and can be sent to me, after changing the file name 
			to something meaningful. (See "Upload" tab on this page.)
		</p>
		<br />
		<p><b>Q: I want to do my own select-queries. Will this be implemented?</b></p>
		<p>
			If you really (really!) want to, we can give you this opportunity in the next few weeks. 
			But you have to know, that you need to understand the very complex table structure.
			<br />
			It would be much easier for you to just ask for a custom "Predefined View" and I will 
			provide that to you.
			<br />
			The queries will also not be saved somewhere if you like it and need it again later, 
			but you can of course just send it to me via Email, and it will be added to the 
			predefined views in very short time.
		</p>
		<br />
		<p><b>Unified Data Format // Data Format Conventions:</b></p>
		<p>
			We need to make sure, that we all use the same data formats and units in the central DB. 
			But this implies, that we also use the same formats everywhere, since otherwise a lot 
			of conversion needs to be done during import. So we all agreed on a set of units:
			<br />
			<br />
			<span class="emph">
				distances/length : mm<br>
				angles : mrad<br>
				tension : N/cm<br>
				pressure : mbar<br>
				pressure loss: mbar / h<br>
				flow loss : l/s  (l=liter)<br>
				leak rate : bar*l/s  (l=liter)<br>
				resistance: Ohm<br>
				current : nA<br>
				capacity : nF<br>
				temperature : °C
			</span>
			<br />
			<br />
			We also agreed on the following file formats, which can be uploaded into the database:
			<br />
			<br />
			<span class="emph">txt, csv, jpg, png, pdf, root-makros</span>
			<br />
			<br />
			For the fields in the databases, we should only use the following formats, if possible:
			<br />
			<br />
			<span class="emph">double precision, string, BLOB (files), pseudo-bools (= strings with 2-3 possible texts like "ok", "true", …)</span>
			<br />
			<br />
			And for date and time formats, we should only use the following format, so that the import can be done without problematic conversions...
			<br />
			<br />
			<span class="emph">YYYY-MM-DD HH24:MI:SS, like "2016-07-23 17:55:33"</span>
		</p>
	</div>
</div>
