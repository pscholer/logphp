<div class="simple bbox">
	<div class="border padd6x12 light smaller">
		<div class="column wquarter">
			Choose DB to be displayed:
		</div>
		<div class="column wquarter">
			<input name="dbchoice" type="radio" value="mm"  $dbchoice_mm_checked  onclick="submitIt('dbchoice')"> <label>CERN Central (MM)</label>
		</div>
		<div class="column wquarter">
			<input name="dbchoice" type="radio" value="val" $dbchoice_val_checked onclick="submitIt('dbchoice')"> <label>Module Validation (MM &amp; sTGC)</label>
		</div>
		<div class="column wquarter">
			<input name="dbchoice" type="radio" value="elx" $dbchoice_elx_checked onclick="submitIt('dbchoice')"> <label>Electronics QAQC (ELX)</label> <span class="helpEvDate"><span class='helpIcon'>?</span> 
				<div class='legendEvDate'>
					<p>
						The Electronics DB contains a lot of data.<br />
						So expect potentially long loading times depending on your request!
					</p>
				</div>
			</span>
		</div>
		<div class="clear"></div>
	</div>
</div>
