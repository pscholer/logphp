<div class="bbox simple"> 
	<div class="border padd6x12"> 
		<p> 
			The purpose of this web page is to display the content 
			of the CERN Central Database for everyone who is interested.
		</p> 
		<p> 
		The majority of the functionality is now in place. You can find all 
		the existing tables in the "Tables" tab, and some plots can be created in the "Predefined Views" tab. 
	
		</p>
		<br /> 
		<p> 
		The best way to search for specific data is to go to the "Custom search" tab and restrict the selection with the drop downs before clicking "SEARCH". You can also download files there and export the data to excel or csv. <br />
		<br />
		</p>
		<br />
		<br />
		<p>
		If you need to download hundreds of files or if you cannot narrow down the selection of the data you want in a way that satisfies you, please send a message to kim.heidegger@physik.uni-freiburg.de.
		<br />
		</p>
	</div>
</div>
