<div class="bbox simple">               
	<div class="border padd6x12 wrap">
		<div class="fleft wthird right topp6">
			Binning:
		</div>
		<div class="fleft wtwothirds">
			<input type='number' class="invisiblelike half" id='scaling' name="scaling" value='20'>
			<button class="buttonlike flex" type='button' onclick='Draw_TH1()'>Re-Draw</button>
		</div>
	</div>
	<div class="clear"></div>
	<div class="border padd6x12 topm6 center">
		<button class="buttonlike" type='button' onclick='GetJSON()' >Download JSON</button> 
		<a href='downloads/parser1D.py' target='_blank'><button class="buttonlike" type='button'>Download Script</button></a> 
	</div>
	<div class="border padd6x12 topm6" id="zone1">
		Here will be the drawing of TH1
	</div>
</div>
<script type='text/javascript' src='https://root.cern/js/5.9.1/scripts/JSRootCore.js'></script>
<script>
// Doc:  https://root.cern.ch/js/5.5.1/docu/JSROOT.html#jsroot-api

//--- Creation et dessin  Histogramme TH1 --------------------
var h1;

function Draw_TH1() {

	var bins = document.getElementById('scaling').value;
	console.log(bins);
	var values  = $json;
	var min = $json_min;
	var max = $json_max;
	var single = (max-min)/bins;
	var bin_values = [];
	 
	for(var i = 0; i < bins; i++) {
		bin_values.push(0);
	}
           
	for (var it = 0; it<values.length; it++ ){
		for (var act_bin = 1; act_bin<=bins; act_bin++){
			if (values[it]<min+act_bin*single){
				bin_values[act_bin-1] = bin_values[act_bin-1]+1;
				break;
			}
		}
	}
	console.log(bin_values);

	h1 = JSROOT.CreateHistogram('TH1F', parseInt(bins));
	h1.fTitle = '$plotTitle ($plotDate)';
	
	h1.fXaxis.fXmin = $json_min;
	h1.fXaxis.fXmax = $json_max;
	 
	h1.fXaxis.fTitle = 'measurement value';
	h1.fYaxis.fTitle = 'count';
	h1.InvertBit(JSROOT.TH1StatusBits.kNoStats);
	
	for (var index = 0; index<values.length; index++){
		h1.Fill(values[index]);
	}
	       
	document.getElementById('zone1').style.width = '1000px';
	document.getElementById('zone1').style.height = '700px';
	
	JSROOT.redraw('zone1', h1, '', function(painter){ var json = JSROOT.toJSON(painter.GetObject()); console.log(json);});
}; 

function GetJSON() {
	var json = JSROOT.toJSON(h1);
	var hiddenElement = document.createElement('a');
	hiddenElement.href = 'data:attachment/text,' + encodeURI(json);
	hiddenElement.target = '_blank';
	hiddenElement.download = 'myFile.json';
	hiddenElement.click();
};

Draw_TH1();

</script>
