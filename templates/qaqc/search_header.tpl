<input type="hidden" id='cols'    name='cols'    value='$cols'   >
<input type="hidden" id='setting' name='setting' value='$setting'>
<div class='bbox simple'>
	<div class='border searchRowBox'>
		<div class='fleft border padd6x12 light middle searchWthird searchRow1'>
			<div class='searchElement'>
				<label>COLLECTION</label><br />
				$select_collection
			</div>
			<div class='searchElement'>
				<label>ASPECT</label><br />
				$select_aspect
			</div>
			<div class='searchElement'>
				<label>CONTEXT</label><br />
				$select_context
			</div>
		</div>
		<div class='fleft border padd6x12 light middle searchWthird searchRow1'>
			<div class='searchElement'>
				<label>EQUIPMENT TYPE GROUP</label><br />
				$select_eqtypegroup
			</div>
			<div class='searchElement'>
				<label>EQUIPMENT TYPE</label><br />
				$select_eqtype
			</div>
		</div>
		<div class='fleft border padd6x12 light middle searchWthird searchRow1'>
			<div class='searchElement'>
				<label>PRODUCTION SITE NAME</label><br />
				$select_psname
			</div>
			<div class='searchElement'>
				<label>PRODUCTION SITE GROUP</label><br />
				$select_psgroup
			</div>
		</div>
		<div class='clear'></div>
	</div>
	<div class='clear'></div>
	<div class='border searchRowBox topm6'>
		<div class='fleft border padd6x12 light searchWthird'>
			<div class='searchElement'>
				<label>MTF ID</label><br />
				<input type="text" class="invisiblelike $mtfId_cls" id="mtfId" name="mtfId" pattern="$patternMtfId" value="$mtfId" onchange="changecolor('mtfId')">
			</div>
		</div>
		<div class='fleft border padd6x12 light searchWthird'>
			<div class='searchElement'>
				<label>EQUIPMENT ID</label><br />
				<input type="text" class="invisiblelike $eqId_cls" id="eqId" name="eqId" pattern="$patternInt" value="$eqId" onchange="changecolor('eqId')">
			</div>
		</div>
		<div class='fleft border padd6x12 light searchWthird'>
			<div class='searchElement'>
				<label>OTHER ID</label><br />
				<input type="text" class="invisiblelike $otherId_cls" id="otherId" name="otherId" pattern="$patternText" value="$otherId" onchange="changecolor('otherId')">
			</div>
		</div>
		<div class='clear'></div>
		<div class='border searchRowBox light searchWfull topm6'>
			<input type="checkbox" name="showConnected" value="1" $showConnected> <label>include also measurements for children of this equipment</label>
		</div>
	</div>
	<div class='clear'></div>
	<div class='border searchRowBox topm6'>
		<div class='border searchRowBox light searchWfull'>
			<div class="fleft searchWthird searchCol1">
				SELECT MEASUREMENT TIMESTAMP RANGE
			</div>
			<div class="fleft searchWthird searchCol2">
				<label>from Timestamp:</label>
				<input class="invisiblelike $msTmpFrom_cls" id='msTmpFrom' name='msTmpFrom' type='text' value='$msTmpFrom' pattern='$patternDate' placeholder='$placeholderDate' onchange="changecolor('msTmpFrom')">
			</div>
			<div class="fleft searchWthird searchCol2">
				<label>to Timestamp:</label>
				<input class="invisiblelike $msTmpTo_cls" id='msTmpTo' name='msTmpTo' type='text' value='$msTmpTo' pattern='$patternDate' placeholder='$placeholderDate' onchange="changecolor('msTmpTo')">
			</div>
			<div class='clear'></div>
		</div>
	</div>
	<div class='clear'></div>
	<div class='border searchRowBox topm6'>
		<div class='border searchRowBox light searchWfull'>
			<div class="fleft searchWthird searchCol1">
				SELECT MEASUREMENT VALUE RANGE
			</div>
			<div class="fleft searchWthird searchCol2">
				<label>from Value:</label>
				<input class="invisiblelike $msValFrom_cls" id='msValFrom' name='msValFrom' type='text' value='$msValFrom' pattern='$patternFloat' onchange="changecolor('msValFrom')">
			</div>
			<div class="fleft searchWthird searchCol2">
				<label>to Value:</label>
				<input class="invisiblelike $msValTo_cls" id='msValTo' name='msValTo' type='text' value='$msValTo' pattern='$patternFloat' onchange="changecolor(''msValTo')">
			</div>
			<div class='clear'></div>
		</div>
	</div>
	<div class='clear'></div>
	<div class='border searchRowBox topm6 wrap'>
		<div class='fleft searchWthird2 border light center'>
			<button type="button" class="buttonlike" onclick="toggleOnOff('tableSettings')">Show / Hide Table Settings</button>
		</div>
		<div class='fleft searchWthird2 border light center'>
			<button type="button" class="buttonlike" onclick="submitIt('reset')">Reset Fields</button>
		</div>
		<div class='fleft searchWthird2 border light center'>
			<button type="button" class="buttonlike" onclick="submitIt('search')">SEARCH</button>
		</div>
		<div class='clear'></div>
		<div id='searchSetting' class='fleft wtwothirds topm6'>
			Current Table Setting: $settingName<br />
			Load User Setting: $select_setting
		</div>
		<div id='searchCheckboxes' class='fleft wthird'>
			<input type="checkbox" name="showRepeated"  value="1" $showRepeated > Show also Repeated Measurements<br />
			<input type="checkbox" name="showDiscarded" value="1" $showDiscarded> Show also Discarded Measurements
		</div>
	</div>
	<div class='clear'></div>
	<div id="tableSettings" class='border searchRowBox topm6 wrap hidden'>
		<div class="border searchRowBox light searchWfull">
			<div id='searchSettingsLeft' class='fleft whalf'>
				<div class='searchSettingsHeadline'>TABLE SETTINGS / VISUALIZATION:</div>
				$settingsSett
			</div>
			<div id='searchSettingsRight' class='fleft whalf'>
				<div id='searchSettingsRightTop'>
					<div class='searchSettingsHeadline'>SHOW DESCRIPTIONS OF ...</div>
					$settingsDesc
				</div>
				<div id='searchSettingsRightBottom'>
					<div class='searchSettingsHeadline'>SHOW ADDITIONAL CONTENT ...</div>
					$settingsAdd
				</div>	                        	                     
			</div>
			<div class='clear'></div>
			<div class='wfull right'>
				<button type="button" class='buttonlike searchSettingButton' onclick='submitIt("applySettings")'>Apply</button>
				<button type="button" class='buttonlike searchSettingButton' onclick='if(askSettingName()){submitIt("saveSettings")}'>Save</button>
				<button type="button" class='buttonlike searchSettingButton' onclick='submitIt("resetSettings")'>Reset to Default</button>
			</div>
		</div>
	</div>	
</div>
