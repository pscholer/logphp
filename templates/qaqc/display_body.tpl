<input type="hidden" name="orderBy"   value="$orderBy"   >
<input type="hidden" name="orderType" value="$orderType" >
<input type="hidden" name="table" value="$table">
<div class="full">
	<table class="tablesorter floatThead" id='sortable'>
		<thead>
			<tr>
				$table_head
			</tr>
		</thead>
		<tbody>
			$table_body
		</tbody>
	</table>
</div>
<table class="invisiTable" id="invisiTable">
	<thead>
		<tr>
			$invisi_head
		</tr>
	</thead>
	<tbody>
		$invisi_body
	</tbody>
</table>
<br />
<br />
