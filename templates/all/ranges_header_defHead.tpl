<div class='bbox simple'>
	<div class='border padd6x12'>$title</div>
	<div class='clear'></div>
	<div class='border padd6x12 topm6'>
		<div class='column wthird left'>Choose equipment to add to the list:</div>
		<div class='column wthird'><input id='chooseRange' name='chooseRange' type='checkbox' value='1' onclick='toggleRange()' $check_chooseRange> select range</div>
		<div class='column wthird fright'><button type="button" class='buttonlike flex' onclick="if(checkRange()){submitIt('add')}">Add ID or Range</button></div>
		<div class='clear'></div>
		<div class='column wthird left'>
			<select class='buttonlike flex' id='typeId' name='typeId'>
				<option value='mtf'   $select_mtf  >MTF - no. ($placeholderMtfId):</option>
				<option value='eqid'  $select_eqid >Unique DB EQ ID:</option>
				<option value='alias' $select_alias>EQ ALIAS:</option>
			</select>
		</div>
		<div class='column wthird'>
			<input id='startRange' class='invisiblelike' type='text' name='startRange' placeholder='$startRangePlhd'
                                                    onchange='trimInpStr("typeId", "startRange")'> 
		</div>
		<div class='column wthird fright'>
			<input id='endRange' class='invisiblelike' type='text' name='endRange' placeholder='$endRangePlhd' $endRangeDisabled
                                                    onchange='trimInpStr("typeId", "endRange")'>
		</div>
		<div class='clear'></div>
		$addheader
	</div>
	<div class='clear'></div>
	$addhint
</div>
