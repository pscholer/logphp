<link rel="stylesheet" href="stylesNew/all/equip.css">
<input type="hidden" name="load" value="none" />
<div id="equipHeader" class="simple bbox">
	<div class="nbox hhalf">
		<div id="equipFields" class="column">
			<div id='equipFieldMtf'>
				<label>Search for MTF - no. ($placeholderMtfId):</label><br />
 				<input name='newMtfId' class='invisiblelike bigger' type='text' value='$newMtfId' onchange='trimInpStr("newMtfId")' pattern="$patternMtfId">
				<button type="button" class='buttonlike medium' onclick='submitItAs("load", "mtfId")'>Load MTF Entry</button>
			</div>
			<div id='equipFieldEqid'>
				<label>Search for unique DB EQ ID:</label><br />
				<input name='newEqId' class='invisiblelike medium' type="text" value='$newEqId' pattern="$patternInt">
				<button type="button" class='buttonlike medium' onclick='submitItAs("load", "eqId")'>Load EQ Entry</button>
			</div>
			<div id='equipFieldAlias'>
				<label>Search for EQ ALIAS:</label><br />
				<input name='newOtherId' class='invisiblelike bigger' type="text" value='$newOtherId' pattern="$patternText">
				<button type="button" class='buttonlike medium' onclick='submitItAs("load", "otherId")'>Load ALIAS Entry</button>
			</div>
		</div> 
		<div id="equipFieldButton" class="column bottom">
			<button type="button" class="buttonlike" onclick="$saveDo" $saveActive>$saveName</button>
		</div>    
	</div>
	<div class="clear"></div>
	<div id="equipFieldEvDate">$eventdate</div>
</div>
