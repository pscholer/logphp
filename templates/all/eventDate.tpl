<div class='helpEvDate $addd'>
	<label class="$addl">$title: <span class='helpIcon'>?</span></label>
	<span class='legendEvDate mt'>
		<p>
			Always enter this date, when the date of the event that you want to report about
			is not the same as the date when you are reporting the action.
		</p>
		<p>
			For example: You should have registered something 3 weeks ago,
			but you only now had the time to enter this in the DB. Put here the date of 3 weeks ago,
			so the DB will not get confused if you want to send something last week, that only has been registered now...
		</p>
	</span>
	<input class='invisiblelike' id='$name' name='$name' type='text' onchange='changecolor("$name")' placeholder='$placeholderDate' pattern="$patternDate">
</div>
