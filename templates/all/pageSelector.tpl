<input type="hidden" name="nEntriesPrev" value="$nEntries">
<input type="hidden" name="showAll"      value="$showAll" >
<div class="simple bbox">
	<div class='border padd6x12 center'>
		In total <b>$nTotal</b> entries have been selected.<br  />
		Choose number of entries per page: <input class="invisiblelike" type="number" name="nEntries" id="nEntries" value="$nEntries" min='1'> <button type="button" class="buttonlike flex" onclick="set('showAll','0');submitIt('refresh')">Go!</button><br />
		Jump to page: $pages1<br />
		Browse pages: $pages2<br />
		<button type="button" class="buttonlike flex" onclick="toggleShowAll();submitIt('refresh')">Toggle Show All Entries on One Page</button>
	</div>
</div>
