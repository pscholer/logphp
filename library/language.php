<?php


// Language
// ============================================
class Language {
	/* A manager for labels and texts that are to be displayed
	* according to the choice of language of the user */

	// members
	// ----------------------------------------
	public  $master = NULL;
	private $config = NULL;
	private $labels = array();
	private $texts  = array();


	// __construct
	// ----------------------------------------
	public function __construct($master, $lang){
		/* Constructor */
		$this->master = $master;
		$this->load($lang);
	}

	// label
	// ----------------------------------------
	public function label($name){
		/* Returns the label stored under the given name */
		if(!array_key_exists($name, $this->labels)) return $name;
		return $this->labels[$name];
	}

	// load
	// ----------------------------------------
	private function load($lang){
		/* Stores the labels and texts given in the configuration */
		return;
		$this->lang   = $lang;
		$this->labels = array();
		$this->texts  = array();
		$config = new Config($this->master, "lang");
		$config->load($this->master->config->get("base")."/languages/".$lang);
		foreach($config as $entry){
			if($entry->type->name=="label") $this->labels[$entry->name] = $entry->value; 
			if($entry->type->name=="text" ) $this->texts [$entry->name] = $entry->value; 
		}
	}

	// text
	// ----------------------------------------
	public function text($name){
		/* Returns the text stored under the given name */
		if(!array_key_exists($name, $this->texts)) return $name;
		return $this->texts[$name];
	}

}

?>
