<?php


// Message
// ============================================
class Message {
	/* A simple container for a verbosity notification */

    // __construct
    // ----------------------------------------
	public function __construct($message, $type=0, $dbError=NULL){
		/* Constructor */
		$this->message    = $message;
		$this->type       = $type;
		$this->dbError    = $dbError;
	}
}


// Verbosity
// ============================================
class Verbosity {
	/* Handling of any type of message the system generates as
	* a reaction to the users requests */

    // members
    // ----------------------------------------
	public  $master    = NULL;
	public  $db        = NULL;
	private $errors    = array();
	private $messages  = array();
	private $successes = array();
	private $warnings  = array();


    // __construct
    // ----------------------------------------
	public function __construct($master){
		/* Constructor */
		$this->master    = $master;
		$this->errors    = array();
		$this->messages  = array();
		$this->successes = array();
		$this->warnings  = array();
		$this->state     = 0; // the highest type of message buffered
	}

    // dump
    // ----------------------------------------
	public function dump($type=-1, $drain=false) {
		/* Creates HTML to dump the messages, either of specific $type
		* or all of them ($type=-1); Also resets the $state in case
		* $drain is set to true; note that the containers always are
		* cleared at every dump (thus not be included in the next dump) */
		$html  = "";
		$html .= ($type==-1 || $type==3) ? $this->dumpCache("vb_error"  , "ERROR"  , "errors"   , true) : "";
		$html .= ($type==-1 || $type==2) ? $this->dumpCache("vb_warning", "WARNING", "warnings"       ) : "";
		$html .= ($type==-1 || $type==1) ? $this->dumpCache("vb_success", "SUCCESS", "successes"      ) : "";
		$html .= ($type==-1 || $type==0) ? $this->dumpCache("vb_message", NULL     , "messages"       ) : "";
		if($drain) $this->state = 0;
		return $html;
	}

    // dumpCache
    // ----------------------------------------
	private function dumpCache($tpl, $prefix, $buffer, $inclDbError=false){
		/* Dumps all messages in a given container */
		$cache = array();
		foreach($this->{$buffer} as $msg){
			$message  = (!empty($prefix) ? $prefix.": " : "").$msg->message;
			$message .= ($inclDbError && !empty($msg->dbError)) ? "<br/>[Oracle Error: &quot;".$msg->dbError."&quot;]" : "";
			array_push($cache, $this->master->html->template($tpl, array("message"=>$message), NULL, "all"));
		}
		$this->{$buffer} = array();
		return implode("", $cache);
	}

    // error
    // ----------------------------------------
	public function error($message, $useDbError=false) {
		/* Stores an error message in the internal container */
		array_push($this->errors, new Message($message, 3, $useDbError ? $this->db->error() : NULL));
		$this->state = 3;
		$this->push(3);
	}

    // message
    // ----------------------------------------
	public function message($message) {
		/* Stores an information message in the internal container */
		array_push($this->messages, new Message($message));
		$this->push(0);
	}

    // push
    // ----------------------------------------
	private function push($type=-1) {
		/* Executes the redirection of the dump to an external container */
		if(empty($this->redirect[$type])) return;
		array_push($this->redirect[$type][0]->{$this->redirect[$type][1]}, $this->dump($type, $this->direct[$type]));
	}

    // redirect
    // ----------------------------------------
	public function redirect($levels, $object, $container, $drain=false){
		/* Links multiple verbosity levels to an external container */
		foreach($levels as $level) {
			$this->redirect[$level] = array($object, $container);
			$this->direct  [$level] = $drain;
		}
	}

    // success
    // ----------------------------------------
	public function success($message) {
		/* Stores an success message in the internal container */
		array_push($this->successes, new Message($message, 1));
		$this->push(1);
	}

    // warning
    // ----------------------------------------
	public function warning($message) {
		/* Stores a warning message in the internal container */
		array_push($this->warnings, new Message($message, 2));
		if($this->state<2) $this->state = 2;
		$this->push(2);
	}

}


?>
