<?php

date_default_timezone_set("Europe/Zurich");

require_once "container.php";
require_once "config.php";
require_once "database.php";
require_once "functions.php";
require_once "html.php";
require_once "language.php";
require_once "verbosity.php";



// loadObjects
// ============================================
function loadObjects($master){
	/* Instantiates all objects that are required for every
	* runtime; especially ContainerTypes */

	$ct = function($value){
		if(!in_array(gettype($value), array("array", "boolean", "double", "integer", "string"))) return false;
		return true;
	};
	$isPair = function($value) {
		if(!is_array($value) || count($value)!=2) return false;
		return true;
	};
	$isBooList = function($value) {
		if(!is_array($value) || count(array_filter($value, function($v){return gettype($v)=="boolean";}))!=count($value)) return false;
		return true;
	};
	$isIntList = function($value) {
		if(!is_array($value) || count(array_filter($value, function($v){return gettype($v)=="integer";}))!=count($value)) return false;
		return true;
	};
	$isDblList = function($value) {
		if(!is_array($value) || count(array_filter($value, function($v){return gettype($v)=="double"; }))!=count($value)) return false;
		return true;
	};
	$isNmbList = function($value) {
		if(!is_array($value) || count(array_filter($value, function($v){return in_array(gettype($v), array("double", "integer")); }))!=count($value)) return false;
		return true;
	};
	$isStrList = function($value) {
		if(!is_array($value) || count(array_filter($value, function($v){return gettype($v)=="string"; }))!=count($value)) return false;
		return true;
	};
	$isObjList = function($value) {
		if(!is_array($value) || count(array_filter($value, function($v){return gettype($v)=="object"; }))!=count($value)) return false;
		return true;
	};
	$isBooPair = function($value) use ($isPair, $isBooList) {
		if(!$isPair($value) || !$isBooList($value)) return false;
		return true;
	};
	$isIntPair = function($value) use ($isPair, $isIntList) {
		//if(!$isPair($value) || !$isIntList($value)) return false;
		return true;
	};
	$isDblPair = function($value) use ($isPair, $isDblList) {
		if(!$isPair($value) || !$isDblList($value)) return false;
		return true;
	};
	$isNmbPair = function($value) use ($isPair, $isNmbList) {
		if(!$isPair($value) || !$isNmbList($value)) return false;
		return true;
	};
	$isStrPair = function($value) use ($isPair, $isStrList) {
		if(!$isPair($value) || !$isStrList($value)) return false;
		return true;
	};
	$isObjPair = function($value) use ($isPair, $isObjList) {
		if(!$isPair($value) || !$isObjList($value)) return false;
		return true;
	};
	$isNmbPairList = function($value) use ($isNmbPair) {
		if(!is_array($value)) return false;
		foreach($value as $pair){
			if(!$isNmbPair($pair)) return false;
		}
		return true;
	};
	$isDblPairList = function($value) use ($isDblPair) {
		if(!is_array($value)) return false;
		foreach($value as $pair){
			if(!$isDblPair($pair)) return false;
		}
		return true;
	};


	// ContainerType
	$any          = new ContainerType($master, "any"        , "any"         , NULL                               );
	$bool         = new ContainerType($master, "boo"        , "boolean"     , false                              );
	$double       = new ContainerType($master, "dbl"        , "double"      , 0                                  );
	$integer      = new ContainerType($master, "int"        , "integer"     , 0                                  );
	$object       = new ContainerType($master, "obj"        , "object"      , NULL                               );
	$string       = new ContainerType($master, "str"        , "string"      , ""                                 );
	$pair         = new ContainerType($master, "pair"       , "array"       , array(NULL , NULL ), $isPair       );
	$boopair      = new ContainerType($master, "boopair"    , "array"       , array(false, false), $isBooPair    ); 
	$dblpair      = new ContainerType($master, "dblpair"    , "array"       , array(0.   , 0.   ), $isDblPair    );
	$intpair      = new ContainerType($master, "intpair"    , "array"       , array(0.   , 0.   ), $isIntPair    ); 
	$objpair      = new ContainerType($master, "objpair"    , "array"       , array(NULL , NULL ), $isObjPair    );
	$strpair      = new ContainerType($master, "strpair"    , "array"       , array(NULL , NULL ), $isStrPair    );
	$list         = new ContainerType($master, "list"       , "array"       , array()                            );
	$boolist      = new ContainerType($master, "boolist"    , "array"       , array()            , $isBooList    ); 
	$dbllist      = new ContainerType($master, "dbllist"    , "array"       , array()            , $isDblList    );
	$intlist      = new ContainerType($master, "intlist"    , "array"       , array()            , $isIntList    ); 
	$objlist      = new ContainerType($master, "objlist"    , "array"       , array()            , $isObjList    );
	$strlist      = new ContainerType($master, "strlist"    , "array"       , array()            , $isStrList    );
	$nmbpairlist  = new ContainerType($master, "nmbpairlist", "array"       , array()            , $isNmbPairList);
	$dblpairlist  = new ContainerType($master, "dblpairlist", "array"       , array()            , $isDblPairList);

	$master->setObj($any              );
	$master->setObj($bool             );
	$master->setObj($double           );
	$master->setObj($integer          );
	$master->setObj($object           );
	$master->setObj($string     , true);
	$master->setObj($pair             );
	$master->setObj($boopair          );
	$master->setObj($dblpair          );
	$master->setObj($intpair          );
	$master->setObj($objpair          );
	$master->setObj($strpair          );
	$master->setObj($list             );
	$master->setObj($boolist          );
	$master->setObj($dbllist          );
	$master->setObj($intlist          );
	$master->setObj($objlist          );
	$master->setObj($strlist          );
	$master->setObj($nmbpairlist      );
	$master->setObj($dblpairlist      );


	// ConfigOption templates
	$eq        = new ConfigOptionTpl($master, "configoption_eq"       , "eq"        , NULL   , $any        ); 
	$neq       = new ConfigOptionTpl($master, "configoption_neq"      , "neq"       , NULL   , $any        ); 
	$above     = new ConfigOptionTpl($master, "configoption_above"    , "above"     , 0      , $double     ); 
	$below     = new ConfigOptionTpl($master, "configoption_below"    , "below"     , 0      , $double     ); 
	$inset     = new ConfigOptionTpl($master, "configoption_inset"    , "inset"     , array(), $dblpair    ); 
	$outset    = new ConfigOptionTpl($master, "configoption_outset"   , "outset"    , array(), $dblpair    ); 
	$inranges  = new ConfigOptionTpl($master, "configoption_inranges" , "inrange"   , array(), $nmbpairlist); 
	$outranges = new ConfigOptionTpl($master, "configoption_outranges", "outrange"  , array(), $nmbpairlist); 
	$add       = new ConfigOptionTpl($master, "configoption_add"      , "add"       , ""     , $string     );
	$config    = new ConfigOptionTpl($master, "configoption_config"   , "config"    , ""     , $string     );
	$group     = new ConfigOptionTpl($master, "configoption_group"    , "group"     , 0      , $integer    );
	$link      = new ConfigOptionTpl($master, "configoption_link"     , "link"      , ""     , $string     );
	$pid       = new ConfigOptionTpl($master, "configoption_pageId"   , "pageId"    , ""     , $string     ); 
	$url       = new ConfigOptionTpl($master, "configoption_url"      , "url"       , ""     , $string     ); 
	$side      = new ConfigOptionTpl($master, "configoption_side"     , "side"      , "left" , $string     ); 
	$setvar    = new ConfigOptionTpl($master, "configoption_setvar"   , "setvar"    , ""     , $string     ); 
	$choice    = new ConfigOptionTpl($master, "configoption_choice"   , "choice"    , ""     , $string     ); 
	$offset    = new ConfigOptionTpl($master, "configoption_offset"   , "offset"    , 0      , $integer    ); 
	$pos       = new ConfigOptionTpl($master, "configoption_pos"      , "pos"       , 0      , $integer    ); 
	$repl      = new ConfigOptionTpl($master, "configoption_repl"     , "repl"      , ""     , $string     ); 
	
	$master->setObj($eq       , true);
	$master->setObj($neq            );
	$master->setObj($above          );
	$master->setObj($below          );
	$master->setObj($inset          );
	$master->setObj($outset         );
	$master->setObj($inranges       );
	$master->setObj($outranges      );
	$master->setObj($add            );
	$master->setObj($config         );
	$master->setObj($group          );
	$master->setObj($link           );
	$master->setObj($pid            );
	$master->setObj($url            );
	$master->setObj($side           );
	$master->setObj($setvar         );
	$master->setObj($choice         );
	$master->setObj($offset         );
	$master->setObj($pos            );
	$master->setObj($repl           );


	// ConfigEntry types (those are SuperContainerType instances)	
	$cetboo     = new SuperContainerType($master, "boolean"    , "boolean", false  , NULL      , array());
	$cetdbl     = new SuperContainerType($master, "double"     , "double" , 0.0    , NULL      , array());
	$cetint     = new SuperContainerType($master, "integer"    , "integer", 0      , NULL      , array());
	$cetstr     = new SuperContainerType($master, "string"     , "string" , ""     , NULL      , array());
	$cetobj     = new SuperContainerType($master, "object"     , "string" , ""     , NULL      , array());

	$cetlist    = new SuperContainerType($master, "array"      , "array"  , array(), NULL      , array());
	$cetboolist = new SuperContainerType($master, "boollist"   , "array"  , array(), $isBooList, array());
	$cetdbllist = new SuperContainerType($master, "doublelist" , "array"  , array(), $isDblList, array());
	$cetintlist = new SuperContainerType($master, "integerlist", "array"  , array(), $isIntList, array());
	$cetobjlist = new SuperContainerType($master, "objectlist" , "array"  , array(), $isObjList, array());
	$cetstrlist = new SuperContainerType($master, "stringlist" , "array"  , array(), $isStrList, array());

	//$cetdbign   = new SuperContainerType($master, "dbIgnore"   , "array"  , array(), $isStrList, array());
	//$cetdbupp   = new SuperContainerType($master, "dbUpper"    , "boolean", false  , NULL      , array());
	//$cetdbapl   = new SuperContainerType($master, "dbAutoPull" , "boolean", true   , NULL      , array());
	//$cetdbaps   = new SuperContainerType($master, "dbAutoPush" , "boolean", false  , NULL      , array());
	//$cetdbasy   = new SuperContainerType($master, "dbAutoSync" , "boolean", false  , NULL      , array());
	//$cetdbfps   = new SuperContainerType($master, "dbFinalPush", "boolean", true   , NULL      , array());

	$cetmenu    = new SuperContainerType($master, "menu"       , "string" , ""     , NULL      , array($pid, $url, $side, $setvar, $choice));
	$cetpath    = new SuperContainerType($master, "path"       , "string" , ""     , NULL      , array());

	$cetdbgrp   = new SuperContainerType($master, "dbgroup"    , "string" , ""     , NULL      , array());
	$cetdblim   = new SuperContainerType($master, "dblimit"    , "integer", 0      , NULL      , array($offset));
	$cetdbord   = new SuperContainerType($master, "dborder"    , "string" , "asc"  , NULL      , array($add));
	$cetdbraw   = new SuperContainerType($master, "dbraw"      , "string" , ""     , NULL      , array($pos));
	$cetdbref   = new SuperContainerType($master, "dbreformat" , "string" , ""     , NULL      , array($repl));
	$cetdbsel   = new SuperContainerType($master, "dbselect"   , "any"    , NULL   , $ct       , array($eq, $neq, $above, $below, $inset, $outset, $inranges, $outranges, $group, $link, $config));

	$master->setObj($cetboo          );
	$master->setObj($cetdbl          );
	$master->setObj($cetint          );
	$master->setObj($cetstr    , true);
	$master->setObj($cetobj          );
	$master->setObj($cetlist         );
	$master->setObj($cetboolist      );
	$master->setObj($cetdbllist      );
	$master->setObj($cetintlist      );
	$master->setObj($cetobjlist      );
	$master->setObj($cetstrlist      );
	//$master->setObj($cetdbign        );
	//$master->setObj($cetdbupp        );
	//$master->setObj($cetdbapl        );
	//$master->setObj($cetdbaps        );
	//$master->setObj($cetdbasy        );
	//$master->setObj($cetdbfps        );
	$master->setObj($cetmenu         );
	$master->setObj($cetpath         );
	$master->setObj($cetdbgrp        );
	$master->setObj($cetdblim        );
	$master->setObj($cetdbord        );
	$master->setObj($cetdbraw        );
	$master->setObj($cetdbref        );
	$master->setObj($cetdbsel        );

}




// LocationOrStatus
// ============================================
class LocationOrStatus {
	/* A container for basic status or location information
	* needed when building the status and site lists */

	// members
	// ---------------------------------------- 
	public  $master = NULL;
	public  $id     = 0;
	public  $name   = NULL;
	private $flags  = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $id, $name, $flags){
		/* Constructor */
		$this->master = $master;
		$this->id     = $id;
		$this->name   = $name;
		$this->flags  = $flags;
	}

	// suits
	// ---------------------------------------- 
	public function suits($flag, $value="A"){
		/* Returns true if the entity matches a given flag */
		if(!array_key_exists($flag, $this->flags)) return false;
		return ($this->flags[$flag] == $value);
	}
}


// Master
// ============================================
class Master {
	/* The master class to the project. It connects
	* all sub classes, stores global objects and 
	* variables, and keeps track of the IONs */

	// members
	// ---------------------------------------- 
	protected $locked  = false;
	public    $name    = NULL;
	public    $config  = NULL;
	public    $html    = NULL;
	public    $db      = NULL;
	public    $lang    = NULL;
	public    $vb      = NULL;
	public    $globals = array();
	public    $session = array();
	public    $post    = array();
	public    $get     = array();
	protected $objects = array();
	private   $default = array();
	private   $ions    = array();
	public    $eventDate       = NULL;
	public    $eventDateHr     = NULL;
	public    $eventDateHrS    = NULL;
	public    $eventDateSh     = NULL;
	public    $eventDateDb     = NULL;
	public    $preurl          = NULL;
	public    $base            = NULL;
	public    $downloadPath    = NULL;
	public    $ncfs            = array();
	public    $doctypes        = array();
	public    $batchIndic      = array();
	public    $docexts         = array();
	public    $fileexts        = array();
	public    $allowedUplTypes = array();
	public    $sites           = array();
	public    $statuses        = array();
	public    $patternBatch    = NULL;
	public    $patternNoBatch  = NULL;
	public    $pattern         = NULL;
	public    $letters         = NULL;
	public    $qaqcOwnerContexts = NULL; // until I find a better way
	public    $qaqcOwnerValues   = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($name, $cfgs) {
		/* Constructor */
		$this->locked  = false;
		$this->name    = $name;
		$this->objects = array();
		$this->default = array();
		$this->ions    = array();
		loadObjects($this);
		$this->config  = new Config($this, "master");
		foreach($cfgs as $path) $this->config->import($path);
		$this->vb      = new Verbosity  ($this);
		$this->html    = new HtmlHandler($this, $name);
		$this->db      = new DbHandler  ($this, $this->config);
		$this->lang    = new Language   ($this, "eng");
		$this->vb->db  = $this->db;
		$this->globals = array();
		$this->session = array();
		$this->post    = array();
		$this->get     = array();
		$this->loadGlobalVars();
		$this->loadGlobalReformats();
		$this->loadGlobalProperties();
		$this->html->set("title"   , $this->config->title->value, "all");
		$this->html->set("theme"   , $this->name                , "all");
		$themeadd = !empty($this->config->classbody->value) ? $this->config->classbody->value : "";
		$this->html->set("themeadd", $themeadd                  , "all");
		if(array_key_exists("lang", $this->globals) && $this->globals["lang"]!="eng") 
			$this->lang->load($this->globals["lang"]);
	}

	// __destruct
	// ---------------------------------------- 
	public function __destruct() {
		/* Destructor */
		$this->db->end();
	}




	// core functions
	// ======================================== 

	// getSites
	// ---------------------------------------- 
	public function getSites($flag=NULL, $value="A"){
		/* Returns the ordered list of sites allowed according to a flag */
		if(empty($flag)) return $this->sites;
		$theList = array();
		foreach($this->sites as $site){
			if(!$site->suits($flag, $value)) continue;
			array_push($theList, $site);
		}
		return $theList;
	}

	// getStatuses
	// ---------------------------------------- 
	public function getStatuses($flag=NULL, $value="A"){
		/* Returns the ordered list of statuses allowed according to a flag */
		if(empty($flag)) return $this->statuses;
		$theList = array();
		foreach($this->statuses as $status){
			if(!$status->suits($flag, $value)) continue;
			array_push($theList, $status);
		}
		return $theList;
	}

	// loadGlobalProperties
	// ---------------------------------------- 
	private function loadGlobalProperties() {
		/* collects global master variables from get and post and session */
		ini_set('post_max_size'      , $this->config->maxUploadSize->value);
		ini_set('upload_max_filesize', $this->config->maxUploadSize->value);
		$this->eventDate    = timestamp(3);
		$this->eventDateHr  = timestamp(2);                   // human readable
		$this->eventDateHrS = substr($this->eventDateHr, 0, strrpos($this->eventDateHr, ":")); // no seconds
		$this->eventDateSh  = timestamp(1);                   // short
		$this->eventDateFs  = str_replace(" ", "_", str_replace(":", "-", $this->eventDateHr)); // filesystem good
		$this->eventDateDb  = dbStringDate($this->eventDate); // database format
		$this->preurl       = "";
		$this->base         = rtrim($this->config->base->value, "/")."/";
		$this->downloadPath = "sessions/guest/";
		if(array_key_exists("sessId", $this->globals)){
			$this->preurl       = "&sessId=".$this->globals["sessId"];
			$this->downloadPath = "sessions/".$this->globals["sessId"]."/";
		}
		if(!is_dir($this->downloadPath)) mkdir($this->downloadPath, 0777, true);
		$this->ncfs        = array("T"=>"non-conform", "F"=>"conform", "X"=>"unknown", "W"=>"warning");
		$this->doctypes    = array("QA/QC", "supplier", "supplier-id", "delivery", "shipping", "non-conformity", "other");
		$this->batchIndic  = array("Batch"=>"Batch", "Item"=>"Item", "Group"=>"Group of Items", "Subbatch"=>"Sub Batch");
		$this->docexts     = array("text/plain"=>"txt", "text/csv"=>"csv", "text/comma-separated-values"=>"csv", "application/vnd.ms-excel"=>"xls", 
		                           "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"=>"xlsx",
		                           "application/vnd.oasis.opendocument.spreadsheet"=>"ods", "application/pdf"=>"pdf",
		                           "application/x-download"=>"pdf", "application/download"=>"pdf", "image/jpeg"=>"jpg",
		                           "image/png"=>"png", "image/tiff"=>"tif");
		$this->fileexts    = array(".root");
		$this->allowedUplTypes = array_keys($this->docexts);
		$this->allowedUplTypes = array_merge($this->allowedUplTypes, $this->fileexts);
		$this->loadSites();
		$this->loadStatuses();
		$this->loadLetters();
		$this->loadPattern();
		$this->patternMtfId    = sprintf("(20|99){1}(MN){1}(%s){1}(%s){1}[0-9]{5}", $this->letters, $this->pattern);
		$this->patternDate     = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
		$this->patternDateTime = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}";
		$this->html->set("allowedUplTypes"       , implode(",", $this->allowedUplTypes), "all");
		$this->html->set("letters"               , $this->letters                      , "all");
		$this->html->set("pattern"               , $this->pattern                      , "all");
		$this->html->set("patternBatch"          , $this->patternBatch                 , "all");
		$this->html->set("patternNoBatch"        , $this->patternNoBatch               , "all");
		$this->html->set("patternMtfId"          , $this->patternMtfId                 , "all");
		$this->html->set("patternDate"           , $this->patternDate                  , "all");
		$this->html->set("patternDateTime"       , $this->patternDateTime              , "all");
		$this->html->set("patternText"           , "[^'\\x22]+"                        , "all");
		//$this->html->set("patternText"           , "[a-zA-Z0-9\.,!?\-\)\(\[\]]+"       , "all");
		$this->html->set("patternInt"            , "[0-9]+"                            , "all");
		$this->html->set("patternFloat"          , "[0-9]+([\.,][0-9]+)?"              , "all");
		$this->html->set("placeholderMtfId"      , "20MN X XXXX # ####"                , "all");
		$this->html->set("placeholderDate"       , "YYYY-MM-DD"                        , "all");
		$this->html->set("placeholderDateTime"   , "YYYY-MM-DD HH:MM"                  , "all");
		$this->html->set("placeholderDateTimeSec", "YYYY-MM-DD HH:MM:SS"               , "all");
	}


	// loadGlobalReformats
	// ---------------------------------------- 
	private function loadGlobalReformats() {
		/* Defines abbreviations for all db tables to be used in the configs */
		$this->db->setGlobalReformat("allowedparenting"   , "ap"  );
		$this->db->setGlobalReformat("batchheritage"      , "bh"  );
		$this->db->setGlobalReformat("cablestickersview"  , "sv"  );
		$this->db->setGlobalReformat("centralmeas"        , "cm"  );
		$this->db->setGlobalReformat("contexts"           , "c"   );
		$this->db->setGlobalReformat("doclink"            , "dl"  );
		$this->db->setGlobalReformat("documents"          , "d"   );
		$this->db->setGlobalReformat("dropdownview"       , "ddv" );
		$this->db->setGlobalReformat("eqcomments"         , "ec"  );
		$this->db->setGlobalReformat("equipment"          , "e"   );
		$this->db->setGlobalReformat("equipmentdetails"   , "eds" );
		$this->db->setGlobalReformat("equipmenttypes"     , "et"  );
		$this->db->setGlobalReformat("eqtypegroups"       , "etg" );
		$this->db->setGlobalReformat("eqtypegroupconts"   , "etgc");
		$this->db->setGlobalReformat("fullmeasview"       , "fmv" );
		$this->db->setGlobalReformat("fullmeasviewall"    , "fmva");
		$this->db->setGlobalReformat("locations"          , "l"   );
		$this->db->setGlobalReformat("measdoclink"        , "mdl" );
		$this->db->setGlobalReformat("measdocuments"      , "md"  );
		$this->db->setGlobalReformat("parenting"          , "p"   );
		$this->db->setGlobalReformat("printers"           , "pr"  );
		$this->db->setGlobalReformat("prodoverview"       , "po"  );
		$this->db->setGlobalReformat("prodsitegroups"     , "psg" );
		$this->db->setGlobalReformat("prodsitegroupcont"  , "psgc");
		$this->db->setGlobalReformat("projects"           , "pj"  );
		$this->db->setGlobalReformat("shippinghistory"    , "sh"  );
		$this->db->setGlobalReformat("statuses"           , "s"   );
		$this->db->setGlobalReformat("statuslocation"     , "sl"  );
		$this->db->setGlobalReformat("stickers"           , "st"  );
		$this->db->setGlobalReformat("stickersizes"       , "ss"  );
		$this->db->setGlobalReformat("translations"       , "t"   );
		$this->db->setGlobalReformat("websiteusers"       , "wu"  );
		$this->db->setGlobalReformat("webtranslations"    , "wt"  );
		$this->db->setGlobalReformat("usersettingswebsite", "usw" );
	}


	// loadGlobalVars
	// ---------------------------------------- 
	private function loadGlobalVars() {
		/* Collects global master variables from get and post and session
		* and reorganizes them (i.e. moves variables around among the 
		* collections) */
		$this->session = parseCollection($_SESSION);
		$this->post    = parseCollection($_POST   );
		$this->get     = parseCollection($_GET    );
		if(isset($this->post["get"]) && !empty($this->post["get"])){
			$sessId = $this->post["sessId"]; // add here all core variables that should be kept
			$pageId = $this->post["get"   ];
			$pageId = $pageId=="home" ? "" : $pageId;
			$do     = isset($this->post["do"]) ? $this->post["do"] : NULL;
			$todo   = isset($this->post[$do ]) ? $this->post[$do ] : NULL;
			$this->post = array("sessId"=>$sessId, "pageId"=>$pageId, "do"=>$do, $do=>$todo);
		}
	
		storeInGlobals($this->globals, $this->session);	
		storeInGlobals($this->globals, $this->get    );	
		storeInGlobals($this->globals, $this->post   );	
		//foreach($this->session as $key=>$val)
		//	$this->globals[$key] = $val;
		//foreach($this->get     as $key=>$val)
		//	$this->globals[$key] = $val;
		//foreach($this->post    as $key=>$val)
		//	$this->globals[$key] = $val;
		foreach($this->session as $key=>$val)
			$this->session[$key] = $this->globals[$key];
		registerInSession($this->session);
		$this->html->setVars($this->globals, "all");
	}


	// loadLetters
	// ---------------------------------------- 
	private function loadLetters(){
		/* Loads the letters, i.e. the values of the column "letter" in the Projects
		* table; used to build the patterns for the MTF Ids */
		$table = $this->db->readTable("projects", array("letter"));
		$letters = array();
		foreach($table as $row) array_push($letters, $row->letter);
		$this->letters = implode("|", $letters);
	}


	// loadPattern
	// ---------------------------------------- 
	private function loadPattern() {
		/* Loads the EqTypeCodes and builds the patterns for MTF Id input for all cases */
		$table = $this->db->readTable("equipmenttypes", array("eqtypecode", "isbatchflag"));
		$eqtcs = array(0=>array(), 1=>array());
		foreach($table as $row) {
			$idx = $row->isbatchflag=="T" ? 1 : 0;
			array_push($eqtcs[$idx], $row->eqtypecode);
		}
		$this->patternBatch   = implode("|", $eqtcs[1]);
		$this->patternNoBatch = implode("|", $eqtcs[0]);
		$this->pattern        = implode("|", array($this->patternBatch, $this->patternNoBatch));
	}

	// loadSites
	// ---------------------------------------- 
	private function loadSites() {
		/* Loads the internal list of sites */
		$locations = $this->db->readTable("locations", array("*"), array(), array(), array("sort"=>"asc"), true);
		$this->sites = array();
		foreach($locations->slim as $row){
			$keys  = array_keys($row);
			$fkeys = array_filter($keys, function($k){ return strpos($k, "flag")!==false; });
			$flags = array_intersect_key($row, array_flip($fkeys));
			array_push($this->sites, new LocationOrStatus($this, $row["id_locations"], $row["sitename"], $flags));
		}
	}

	// loadStatuses
	// ---------------------------------------- 
	private function loadStatuses() {
		/* Loads the internal list of statuses */
		$statuses = $this->db->readTable("statuses", array("*"), array(), array(), array("sort"=>"asc"), true);
		$this->statuses = array();
		foreach($statuses->slim as $row){
			$keys  = array_keys($row);
			$fkeys = array_filter($keys, function($k){ return strpos($k, "flag")!==false; });
			$flags = array_intersect_key($row, array_flip($fkeys));
			array_push($this->statuses, new LocationOrStatus($this, $row["id_statuses"], $row["statusname"], $flags));
		}
	}




	// global reference handling
	// ======================================== 

	// delObj
	// ---------------------------------------- 
	public function delObj($typename, $objname) {
		/* Removes an object from the internal buffer */
		if(!array_key_exists($typename, $this->objects           )) return NULL;
		if(!array_key_exists($objname , $this->objects[$typename])) return NULL;
		unset($this->objects[$typename][$objname]);
	}

	// getObj
	// ---------------------------------------- 
	public function getObj($typename, $objname=NULL) {
		/* Returns the pointer to the stored object $objname of type $typename */
		if(!array_key_exists($typename, $this->objects           )) return NULL;
		if(empty($objname) && array_key_exists($typename, $this->default)) 
			$objname = $this->default[$typename];
		if(!array_key_exists($objname , $this->objects[$typename])) return NULL;
		return $this->objects[$typename][$objname];
	}

	// hasObj
	// ---------------------------------------- 
	public function hasObj($typename, $objname=NULL) {
		/* Returns the pointer to the stored object $objname of type $typename */
		if(!array_key_exists($typename, $this->objects           )) return false;
		if(empty($objname) && array_key_exists($typename, $this->default)) 
			$objname = $this->default[$typename];
		if(!array_key_exists($objname , $this->objects[$typename])) return false;
		return true;
	}

	// setObj
	// ---------------------------------------- 
	public function setObj($object, $default=false) {
		/* Stores an object in the internal buffer */
		$typename = get_class($object);
		if(!array_key_exists($typename, $this->objects)) $this->objects[$typename] = array();
		$this->objects[$typename][$object->name] = $object;
		if($default) $this->default[$typename]   = $object->name;
	}




	// ION handling
	// ======================================== 

	// checkION
	// ---------------------------------------- 
	public function checkION($name){
		/* Returns true if the given name is a valid ION */
		return true;
		$name = strtolower($name);
		//if(preg_match('/[^a-z0-9\_]/',        $name       )) return false;
		//if(preg_match('/[^a-z]/'     , substr($name, 0, 1))) return false;
		return !in_array($name, $this->ions);	
	}

	// cloneION
	// ---------------------------------------- 
	public function cloneION($name, $entity=NULL){
		/* Returns an ION for a cloned object with original name $name */
		$use = !empty($entity) ? $entity->name."_".$name : $name;
		if($this->checkION($use          )) return $use;
		if($this->checkION($use."_cloned")) return $use."_cloned";
		$i=1;
		while(true){
			$new = $use."_cloned".strval($i);
			if($this->checkION($new)) return $new;
		}
	}

	// makeION
	// ---------------------------------------- 
	public function makeION($name){
		/* Turns a invalid ION into a valid one */
		return $name."_".strval(timestamp(3));	
	}

	// registerION
	// ---------------------------------------- 
	public function registerION($name){
		/* Registers a valid ION in the list of currently used IONS */
		return true;
		//if(in_array($name, $this->ions)) return false;
		array_push($this->ions, $name);
		return true;
	}

	// removeION
	// ---------------------------------------- 
	public function removeION($name){
		/* Removes an ION from the list of currently used IONs */
		//if(!in_array($name, $this->ions)) return false;
		$key = array_search($name, $this->ions);
		unset($this->ions[$key]);
		return true;
	}




	// generally useful methods
	// ======================================== 

	// getHost
	// ---------------------------------------- 
	public function getHost(){
		/* Returns the hostname. Leave as dedicated function in case of format changes */
		return gethostname();
	}

	// getVar
	// ---------------------------------------- 
	public function getVar($key) {
		/* Returns a global variable $key from the appropriate container */
		if(array_key_exists($key, $this->globals)) return $this->globals[$key];
		if(array_key_exists($key, $this->post   )) return $this->post   [$key];
		if(array_key_exists($key, $this->get    )) return $this->get    [$key];
		return NULL;
	}

}




// Gate
// ============================================
class Gate extends Master {
	/* Child to the master, and builds the page */

		
	// members
	// ---------------------------------------- 
	public  $userSite      = NULL;
	private $permissions   = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($name, $configs) {
		/* Constructor */
		parent::__construct($name, $configs);
		$this->loadPermissions();
		$this->lockNoUser();
		$this->loadPage();
	}




	// page building
	// ======================================== 

	// getPermission
	// ---------------------------------------- 
	public function getPermission($pageId=NULL) {
		/* Returns the permissions for a given page id */
		if(!array_key_exists($pageId, $this->permissions)) return true;
		return $this->permissions[$pageId];
	}

	// loadHtml
	// ---------------------------------------- 
	private function loadHtml($content=NULL, $pageId=NULL) {
		/* Builds and prints the HTML code of the final page through the main template */

		$menus = $this->html->makeMenuNew();
		//$menus = $this->html->makeMenu();

		$fs = 100; //$this->globals["fontsize"];

		$this->html->set("imgsrc"              , $this->config->imgsrc  ->value, "main");
		$this->html->set("imgtitle"            , $this->config->imgtitle->value, "main");
		$this->html->set("imgtheme"            , $this->config->imgtheme->value, "main");
		//$this->html->set("idBody"              , "body_".$this->name           , "main");
		$this->html->set("pageId"              , $pageId                       , "main");
		$this->html->set("fontsize"            , $fs                           , "main");
		$this->html->set("checked_fontsize_100", ($fs==100)?"checked":""       , "main");
		$this->html->set("checked_fontsize_80" , ($fs== 80)?"checked":""       , "main");
		$this->html->set("navigationLeft"      , $menus[0]                     , "main");
		$this->html->set("navigationRight"     , $menus[1]                     , "main");
		$this->html->set("content"             , $content                      , "main");

		echo $this->html->template("main", array(), NULL, "all", "main"); 
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage($pageId = NULL) {
		/* The heart of it all: build the Page object and execute it
		* yielding the HTML code that is displayed eventually. */

		if($this->locked) return;

		// temporarily for QAQC: dbchoice
		if($this->name=="qaqc") {
			$dbchoice = "mm";
			if(isset($this->post["dbchoice"])) $dbchoice = $this->post["dbchoice"];
			$this->html->set("dbchoice_".$dbchoice."_checked", "checked");
			$this->html->set("dbchoice", $this->html->template("_dbchoice"));
			$this->qaqcOwnerContexts = $this->config->get("dbOwner_contexts_".$dbchoice)->value;
			$this->qaqcOwnerValues   = $this->config->get("dbOwner_values_".  $dbchoice)->value;
		}

		if(empty($pageId))
			if(array_key_exists("pageId", $this->globals)) $pageId = $this->globals["pageId"];
		if(empty($pageId))
			$pageId = "news";

		$this->globals["pageId"] = $pageId;
		$this->preurl = "&sessId=".$this->globals["sessId"]."&pageId=".$pageId;

		eval("require \"pages/".$this->name."/".$pageId.".php\";");

		$this->lockNoPage       ($page);
		$this->lockNoPermissions($page);

		if($this->locked) return;

		$doSubmit = !($_SERVER['REQUEST_METHOD']=="GET" || empty($this->post["do"]));
		if($doSubmit && isset($this->post["do"]) && $this->post["do"]=="save") 
			$this->html->set("activeState", "disabled");
		$content  = $doSubmit ? $page->submit() : $page->load();

		$this->loadHtml($content, $pageId);

	}

	// loadPermissions
	// ---------------------------------------- 
	private function loadPermissions() {
		/* Builds the permissions variable that contains the callbacks to
		* the permission checks associated to the corresponding pageId */
		$isDefault = ($this->globals["username"]=="default_user");
		$isKim     = ($this->globals["username"]=="temming"     );
		$this->permissions = array();
		// both themes
		$this->permissions["display"     ] = true;
		// log theme
		$this->permissions["admin"       ] = $this->requireAdmin();
		$this->permissions["batches"     ] = $this->requireEdit();
		$this->permissions["contact"     ] = true;
		$this->permissions["displayequip"] = ($this->requireEdit() || $isDefault);
		$this->permissions["edititems"   ] = $this->requireMultiEdit();
		$this->permissions["functional"  ] = $this->requireEdit();
		$this->permissions["groups"      ] = $this->requireEdit();
		$this->permissions["help"        ] = true;
		$this->permissions["news"        ] = true;
		$this->permissions["overview"    ] = ($this->requireEdit() || $isDefault);
		$this->permissions["print"       ] = $this->requireEdit();
		$this->permissions["prod"        ] = ($this->requireEdit() || $isDefault);
		$this->permissions["receiveequip"] = $this->requireEdit();
		$this->permissions["register"    ] = $this->requireEdit();
		$this->permissions["sendequip"   ] = $this->requireEdit();
		$this->permissions["shipping"    ] = $this->requireEdit();
		// qaqc theme
		$this->permissions["search"      ] = true;
		$this->permissions["views"       ] = true;
	}


	// permissions and locks
	// ======================================== 

	// checkLogin
	// ---------------------------------------- 
	private function checkLogin() {
		/* Checks if the user is logged in. Keep as separate function
		* for potential future development */
		if(!empty($_SESSION["userId"])) return true;
		return false;
	}

	// lockNoPage
	// ---------------------------------------- 
	private function lockNoPage($page) {
		/* Lock the page if the Page object could not be instantiated. */
		if(is_object($page)) return;
		$content = $this->html->template("lock_noPage", array(), NULL, "all");
		$this->loadHtml($content);
		$this->locked = true;
	}

	// lockNoPermissions
	// ---------------------------------------- 
	private function lockNoPermissions($page) {
		/* Lock the page if the user does not have the required permissions. */
		if($this->permissions[$page->id]) return;
		//if($page->permissions()) return;
		$content = $this->html->template("lock_noPermissions", array(), NULL, "all");
		$this->loadHtml($content);
		$this->locked = true;
	}

	// lockNoUser
	// ---------------------------------------- 
	private function lockNoUser() {
		/* Lock the page if the user does not have the required permissions. */
		if($this->checkLogin()) return;
		session_destroy();
		$content = $this->html->template("lock_noLogin", array(), NULL, "all");
		$this->loadHtml($content);
		$this->locked = true;
	}

	// requireAdmin
	// ---------------------------------------- 
	public function requireAdmin(){
		/* Returns true if the current user meets all requirements 
		* related to admin-only pages. */
		if($this->globals["isAdmin"]) return true;
		return false;
	}

	// requireEdit
	// ---------------------------------------- 
	public function requireEdit(){
		/* Returns true if the current user meets all requirements 
		* related to edit pages. */
		if($this->globals["canEdit"]) return true;
		return false;
	}

	// requireMultiEdit
	// ---------------------------------------- 
	public function requireMultiEdit(){
		/* Returns true if the current user meets all requirements 
		* related to multi-edit pages. */
		if($this->globals["canMultiEdit"]) return true;
		return false;
	}











	// getBatchCols
	// ---------------------------------------- 
	public function getBatchCols($isbatch, $subbatchid, $quantity){
		if($isbatch){
			if($subbatchid!="") return "Subbatch";
			else                return "Batch";
		}
		else {
			if($quantity!="") return "Group";
			else              return "Item";
		}
		return "";
	}


	// getBatchIndic
	// ---------------------------------------- 
	public function getBatchIndic($isbatch, $subbatchid, $quality){
		return $this->batchIndic[$this->getBatchCols($isbatch, $subbatchid, $quality)];
	}






	// helper DB retrieval functions
	// ======================================== 


	// getEqLocation
	// ---------------------------------------- 
	public function getEqLocation($eqid, $additionals=array(), $viewname="locattr") {
		/* Retrieves locatoin information to the $eqid, by default only the majorlocid
		* is read; the function returns the pointer to the view (as it is the best
		* info container) */

		$cm = new DbConfig($this, "getEqLocation_max_".timestamp());
		$cm->column = "max(sl.eventdate)";
		$cm->joinon = "e.id_equipment = sl.eqentryid";
		$cm->select("e.id_equipment", $eqid     );
		$cm->select("sl.majorlocid" , "not null");
		$cm->select("sl.isvalidflag", "T"       );

		$c = new DbConfig($this, "getEqLocation_".timestamp());
		$c->joinon   = "sl.majorlocid = l.id_locations";
		$c->column   = "sl.majorlocid";
		$c->columns  = $additionals;
		$c->reformat("to_char(sl.createtime, 'YYYY-MM-DD')"               , "createtime"); 
		$c->reformat("to_char(sl.eventdate , 'YYYY-MM-DD HH24:MI:SS.FF6')", "eventdate" );
		$c->select  ("sl.eqentryid"  , $eqid);
		$c->select  ("sl.eventdate"  , $cm  );
		$c->select  ("sl.isvalidflag", "T"  );

		$this->db->separate($viewname, $c);
		return $this->db->get($viewname);
	}	


	// getEqStatus
	// ---------------------------------------- 
	public function getEqStatus($eqid, $additionals=array(), $viewname="statattr") {
		/* Retrieves status information to the $eqid, by default only the status id 
		* is read; the function returns the pointer to the view (as it is the best
		* info container) */

		$cm = new DbConfig($this, "getEqStatus_max_".timestamp());
		$cm->column = "max(sl.eventdate)";
		$cm->joinon = "e.id_equipment = sl.eqentryid";
		$cm->select("e.id_equipment", $eqid     );
		$cm->select("sl.statusid"   , "not null");
		$cm->select("sl.isvalidflag", "T"       );

		$c = new DbConfig($this, "getEqStatus_".timestamp());
		$c->joinon   = "sl.statusid = s.id_statuses";
		$c->column   = "sl.statusid";
		$c->columns  = $additionals;
		$c->reformat("to_char(sl.createtime, 'YYYY-MM-DD')"               , "createtime"); 
		$c->reformat("to_char(sl.eventdate , 'YYYY-MM-DD HH24:MI:SS.FF6')", "eventdate" );
		$c->select  ("sl.eqentryid"  , $eqid);
		$c->select  ("sl.eventdate"  , $cm  );
		$c->select  ("sl.isvalidflag", "T"  );

		$this->db->separate($viewname, $c);
		return $this->db->get($viewname);
	}	


	// getEqId
	// ---------------------------------------- 
	public function getEqId($mtfid=NULL, $otherid=NULL){

		if(empty($mtfid) && empty($otherid)) 
			return -1;

		if(!empty($mtfid))
			$equipment = $this->db->readTable("equipment", array("id_equipment"), array("partsbatchmtfid"=>$mtfid, "subbatchid"=>"null"));
		else
			$equipment = $this->db->readTable("equipment", array("id_equipment"), array("otherid"        =>$otherid));

		if($equipment->count()==0) return -1;
		$eqid = $equipment->id_equipment;
		//$equipment->revert();
		return $eqid;
	}

	// getOptionsSites
	// ---------------------------------------- 
	public function getOptionsSites($flag=NULL, $value="A") {
		/* Returns the list of sites that can be inserted into a select field */
		$raw = $this->getSites($flag, $value);
		$options = array();
		foreach($raw as $site)
			$options[$site->id] = $site->name;
		return $options;
	}

	// getOptionsStatus
	// ---------------------------------------- 
	public function getOptionsStatus($flag=NULL, $value="A") {
		/* Returns the list of statuses that can be inserted into a select field */
		$raw = $this->getStatuses($flag, $value);
		$options = array();
		foreach($raw as $status)
			$options[$status->id] = $status->name;
		return $options;
	}

	// getUserSite
	// ---------------------------------------- 
	public function getUserSite(){
		/* Returns the ID of the sire of the user */
		if(!empty($this->userSite)) return $this->userSite;
		$this->userSite = 0;
		$table = $this->db->readTable("websiteusers", array("usersiteid"), array("username"=>$this->globals["username"]));
		$this->userSite = $table->usersiteid;
		//$table->revert();
		return $this->userSite;
	}

}

?>
