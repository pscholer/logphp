<?php


// Box
// ============================================
trait Box {
	/* An entity with a reference to the master and
	a unique ION */

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// __destruct
	// ---------------------------------------- 
	public function __destruct(){
		/* Destructor */
		//$this->master->removeION($this->name);
	}

	// validate
	// ---------------------------------------- 
	protected function validate(){
		/* Validates itself and sets the status to true if all
		* is fine; to be overloaded by children */
		$this->isValid = true;
	}




	// hierarchy
	// ======================================== 

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

}

?>
