<?php

// File System
// ============================================
// ============================================

// rm
// --------------------------------------------
function rm($path){
	if(is_file($path)){
		unlink($path);
		return;
	}
	foreach(glob(rtrim($path, "/")."/*") as $subpath)
		rm($subpath);
}


// Variable Handling
// ============================================
// ============================================

// convertTo
// --------------------------------------------
function convertTo($master, $variable, $type="any"){
	if(!in_array($type, array("any", "array", "boolean", "double", "integer", "object", "string"))) return NULL;
	$is = gettype($variable);
	if($is=="string") $variable = trim($variable);
	if($is=="string" && strtolower(trim($variable))=="null") return NULL;
	if($is==$type) return $variable;
	if($type=="any" && $is=="string"){
		if(substr($variable, 0, 1)=="%") return $master->getObj(substr($variable, 1));
		return parseString($variable);
	}
	if($is=="array"){
		if($type=="boolean") return false; // no rule
		if($type=="double" ) return NULL;  // no rule
		if($type=="integer") return NULL;  // no rule
		if($type=="object" ) return NULL;  // no rule
		if($type=="string" ) {
			$elms = array();
			foreach($variable as $elm)
				array_push($elms, convertTo($master, $elm, "string"));
			return "(".implode(",", $elms).")";
		}
	}
	if($is=="boolean"){
		if($type=="array"  ) return array($variable);
		if($type=="double" ) return $variable ? 1.0    : 0.0    ;
		if($type=="integer") return $variable ? 1      : 0      ;
		if($type=="object" ) return NULL;  // no rule
		if($type=="string" ) return $variable ? "true" : "false";
	}
	if($is=="double"){
		if($type=="array"  ) return array($variable);
		if($type=="boolean") return $variable!=0 ? true : false;
		if($type=="integer") return intval($variable);
		if($type=="object" ) return NULL;  // no rule
		if($type=="string" ) return strval($variable);
	}
	if($is=="integer"){
		if($type=="array"  ) return array($variable);
		if($type=="boolean") return $variable!=0 ? true : false;
		if($type=="double" ) return doubleval($variable);
		if($type=="object" ) return NULL;  // no rule
		if($type=="string" ) return strval($variable);
	}
	if($is=="object"){
		if($type=="array"  ) return property_exists($variable, "value") ? convertTo($master, $variable->value, "array"  ) : NULL;
		if($type=="boolean") return property_exists($variable, "value") ? convertTo($master, $variable->value, "boolean") : NULL;
		if($type=="double" ) return property_exists($variable, "value") ? convertTo($master, $variable->value, "double" ) : NULL;
		if($type=="integer") return property_exists($variable, "value") ? convertTo($master, $variable->value, "integer") : NULL;
		if($type=="string" ) {
			if(property_exists($variable, "name")) return "%".$variable->name;
			if(method_exists($variable, '__toString')) return strval($variable);
			return NULL;
		}
	}
	if($is=="string"){
		if($type=="array"  ) return parseString($variable, true);
		if($type=="boolean") return strtolower($variable)=="true" ? true : false;
		if($type=="double" ) return doubleval($variable);  
		if($type=="integer") return intval   ($variable);
		if($type=="object" ) return substr($variable,0,1)=="%" ? $master->getObj(substr($value, 1)) : NULL;
	}
	return NULL;
}

// parseCollection
// --------------------------------------------
function parseCollection($theCollection) {
	$result = array();
	foreach($theCollection as $key=>$value)
		$result[$key] = parseString($value);
	return $result;
}

// parseString
// --------------------------------------------
function parseString($theVariable, $asList=false) {
	/* Parses a string $theVariable and converts it to whatever the
	* string encodes (no object references though); if the string
	* has the format "(.., ..., ....)"  or ".., ..., ...." (in the
	* latter case $asList must be set to true), the string is
	* converted to an array with every individual elements parsed
	* in a second stage separately; if the list contains keys as 
	* well (i.e. _every_ element has the format "key:value") the
	* keys are assigned to the output array, otherwise the elements
	* are simply pushed to the array */
	if(!is_string($theVariable)) return $theVariable;
	$theVariable = trim($theVariable);
	if(strtolower($theVariable)=="true" ) return true;
	if(strtolower($theVariable)=="false") return false;
	if(substr($theVariable,0,1)=="(" && substr($theVariable,-1)==")"){
		$asList      = true;
		$theVariable = substr($theVariable, 1, -1);
	}
	if($asList){
		preg_match_all("/(?:[^,({]|\([^)}]*\))+/", $theVariable, $elms);
		$result = array();
		if(count(array_filter($elms[0], function($x) { return strpos($x, ":")===false; }))==0){
			foreach($elms[0] as $elm){
				$expls = explode(":", $elm);
				$result[parseString($expls[0])] = parseString($expls[1]);
			}
		}
		else {
			foreach($elms[0] as $elm)
				array_push($result, parseString($elm));
		}
		return $result;	
	}
	if(!is_numeric($theVariable)) return $theVariable;
	$f = floatval($theVariable);
	$i = intval  ($theVariable);
	if($f && $i!=$f) return $f;
	if($i          ) return $i;
	return $theVariable;
}


// Variable Collections
// ============================================
// ============================================

// extractVar
// --------------------------------------------
function extractVar($key, $session, $post, $get, $reqmethod) {
	if(                      array_key_exists($key, $session)) return $session[$key];
	if($reqmethod=="POST" && array_key_exists($key, $post   )) return $post   [$key];
	if($reqmethod=="GET"  && array_key_exists($key, $get    )) return $get    [$key];
	return NULL;
}

// registerInSession
// --------------------------------------------
function registerInSession($vars){
	/* Registers all variables in the collection
	* into the session */
	foreach($_SESSION as $key=>$val)
		unset($_SESSION[$key]);
	foreach($vars as $key=>$val)
		$_SESSION[$key] = $val;
}

// storeInGlobals
// --------------------------------------------
function storeInGlobals(&$globals, $vars){
	/* Registers all variables in the collection
	* into the session */
	foreach($vars as $key=>$val){
		if(isset($globals[$key]) && empty($val)) continue;
		$globals[$key] = $val;
	}
}


// String Handling
// ============================================
// ============================================

// replaceFirst
// --------------------------------------------
function replaceFirst($from, $to, $content){
    $from = '/'.preg_quote($from, '/').'/';
    return preg_replace($from, $to, $content, 1);
}

// stringWithin
// --------------------------------------------
function stringWithin($string, $start, $end){
	if(preg_match("/".$start."(.*?)".$end."/", $string, $match) == 1)
		return $match[1];
	return "";
}

// stripLeft
// --------------------------------------------
function stripLeft($string, $list){
	$string = trim($string);
	if(count($list)==0) return $string;
	foreach($list as $entry){
		if(substr($string, 0, strlen($entry)+1)==$entry." "){
			$string = substr($string, strlen($entry)+1);
			$string = trim($string);
		}
	}
	return $string;
}


// List Handling
// ============================================
// ============================================

// findAll
// --------------------------------------------
function findAll($string, $needle){
	$pos     = array();
	$lastPos = 0;
	while(($lastPos = strpos($string, $needle, $lastPos))!==false){
		array_push($pos, $lastPos);
		$lastPos = $lastPos + strlen($needle);
	}
	return $pos;
}

// findAny
// --------------------------------------------
function findAny($array, $string){
	foreach($array as $value){
		if(strpos($string, $value)===false) continue;
		return $value;
	}
	return NULL;
}

// findObject
// --------------------------------------------
function findObject($objects, $name, $value){
	/* Returns the pointer to the object in an array $objects
	* whose member $name matches a given value $value */
	foreach($objects as $object){
		if($object->$name==$value) return $object;
	}
	return NULL;
}

// removeFromArray
// --------------------------------------------
function removeFromArray(&$array, $keys){
	/* Removes all elements from an array $array if the key is
	* contained in $keys. */
	foreach($keys as $key){
		if(!array_key_exists($key, $array)) continue;
		unset($array[$key]);
	}
}


// Data Base Helpers
// ============================================
// ============================================

// extractColumns
// --------------------------------------------
function extractColumns($raw, $seekers, $isTable = false){
	/* Extracts column names (format either <table>.<columnname> or <columnname>)
	* from a string $raw using a list of either table names or column names */
	$elements = preg_split("/[^\w]/", $raw);
	$found = array_intersect($elements, $seekers);
	$cols = array();
	foreach($found as $idx=>$key){
		$col = $isTable ? $elements[$idx].".".$elements[$idx+1] : $elements[$idx];
		array_push($cols, $col);
	}
	return $cols;
}
 
// extractColumnsSelect
// --------------------------------------------
function extractColumnsSelect($sql){
	/* Extract names of selected columns (format either <table>.<columnname> or <columnname>)
	* from an SQL SELECT ... FROM statement (columns are within those two keywords); note this
	* is a very naive parser, it merely returns the list of elements of the list within the
	* two SQL keywords but does NOT reformat them */
    $sqlu = strtoupper($sql);
    if(strpos($sqlu, "SELECT ")===false || strpos($sqlu, " FROM ")===false) return NULL;
	$selectPos = strpos($sqlu, "SELECT ");
	$fromPos   = -1;
	$froms     = findAll($sqlu, " FROM ");
	foreach($froms as $from){
		$sub = substr($sqlu, $selectPos, $from);
		if(substr_count($sub, "(") - substr_count($sub, ")") == 0){ 
			$fromPos = $from;
			break;
		}
	}
	$def      = substr($sqlu, $selectPos+7, $fromPos-7);
    $raw      = explode(",", $def);
    $elements = array();
    $rem      = array();
    foreach($raw as $elm){
        array_push($rem, $elm);
        $txt = implode(",", $rem);
        if(substr_count($txt, "(") - substr_count($txt, ")") == 0){
			array_push($elements, $txt);
            $rem = array();
        }
    }
    return $elements;
}

// getFieldsFromSql
// --------------------------------------------
function getFieldsFromSql($sql, $start="SELECT ", $end=" FROM") {
	$flist  = explode(",", stringWithin($sql, $start, $end));
	$flist  = array_map('trim', $flist);
	$result = array();
	foreach($flist as $raw){
		if(strpos($raw, " as ")===false) {
			array_push($result, $raw);
			continue;
		}
		array_push($result, substr($raw, strpos($raw, " as ")+4));
	}
	return $result;
}

// sqlDataType
// --------------------------------------------
function sqlDataType($sqlType){
	$s = strtoupper($sqlType);
	if(strpos($s, "VARCHAR"  )!==false) return "string";
	if(strpos($s, "TIMESTAMP")!==false) return "string";
	if(strpos($s, "DATE"     )!==false) return "string";
	if(strpos($s, "NUMBER"   )!==false) return "integer";
	if(strpos($s, "FLOAT"    )!==false) return "double";
	if(strpos($s, "LOB"      )!==false) return "object";
	return "string";
}


// Timestamps and Dates
// ============================================
// ============================================

// addDays
// --------------------------------------------
function addDays($date, $days, $format="Y-m-d H:i:s"){
	return date($format, strtotime($date." ".strval($days)." day"));
}

// completeTime
// --------------------------------------------
function completeTime($str=NULL){
	/* Returns the unix timestamp for the current time, completed
	up to seconds. It means, if $str contains a date in the format
	YYYY-MM-DD then the current hours, minutes, and seconds are 
	added */
	if(empty($str)) return time();
	$theTime = strtotime($str);
	if(($theTime - 23*3600)%86400 == 0) // is day
		return $theTime + (time() - strtotime(date('Y-m-d')));
	if(($theTime - 23*3600)%3600  == 0) // is hour
		return $theTime + (time() - strtotime(date('Y-m-d H:00:00')));
	if(($theTime - 23*3600)%60    == 0) // is minute
		return $theTime + (time() - strtotime(date('Y-m-d H:i:00')));
	return $theTime;
}

// convertDate
// --------------------------------------------
function convertDate($date=NULL, $old='d-M-y h.i.s.u A', $new="Y-m-d H:i:s"){
	/* Converts a date string from an old to a new format */
	if(empty($date)) return strval(date($new)); // returns today if empty
	$d = DateTime::createFromFormat($new, $date);
	if(gettype($d)=="boolean" && !$d) return NULL; 
	if($d->format($new) === $date) return $date; // date already in good format
	$d = DateTime::createFromFormat($old, $date);
	if(gettype($d)=="boolean" && !$d) return NULL;
	return $d->format($new);
}

// dbStringDate
// --------------------------------------------
function dbStringDate($date){
	if(empty($date)) return $date;
	return sprintf("TO_TIMESTAMP('%s','YYYY-MM-DD HH24:MI:SS.FF6')", timestamp(2, $date, true));
}

// earliest
// --------------------------------------------
function earliest($eventDate, $dates){
	if(isValidDate($eventDate) || count($dates)==0) return timestamp(3, $eventDate);
	array_filter($dates, "isValidDate");
	$normed = array();
	foreach($dates as $date)
		array_push($normed, timestamp(3, $date));
	sort($normed);
	return $normed[0];
}

// isAlmost
// --------------------------------------------
function isAlmost($date1, $date2, $tolerance=2){
	/* Checks if two dates are the same within a given tolerance in seconds */
	$sd1 = strtotime($date1);
	$sd2 = strtotime($date2);
	return (abs($sd1-$sd2)<=$tolerance);
}

// isEarlier
// --------------------------------------------
function isEarlier($date1, $date2){
	return $date1<$date2; ## assuming same format for now
}

// isFuture
// --------------------------------------------
function isFuture($date){
	$theDate = new DateTime($date);
	$theNow  = new DateTime();
	$theDate->setTime(0,0,0);
	$theNow ->setTime(0,0,0);
	return $theDate > $theNow;
}

// isToday
// --------------------------------------------
function isToday($date){
	$theDate = new DateTime($date);
	$theNow  = new DateTime();
	$theDate->setTime(0,0,0);
	$theNow ->setTime(0,0,0);
	return $theDate == $theNow;
}

// isValidDate
// --------------------------------------------
function isValidDate($date, $format='Y-m-d H:i:s'){
	if(empty($date) || $date=="YYYY-MM-DD") return false;
	$d = DateTime::createFromFormat($format, strval(date($format, strtotime($date))));
	if(gettype($d)=="boolean" && !$d) return false;
	return true; // format should not matter, no? 
	//return $d->format($format) == $date;
}

// latest
// --------------------------------------------
function latest($eventDate, $dates){
	if(isValidDate($eventDate) || count($dates)==0) return timestamp(3, $eventDate);
	array_filter($dates, "isValidDate");
	$normed = array();
	foreach($dates as $date)
		array_push($normed, timestamp(3, $date));
	rsort($normed);
	return $normed[0];
}

// timestamp
// --------------------------------------------
function timestamp($format=0, $str=NULL, $complete=false){
	/* Returns a timestamp in different formats
	* 0 = default, one big number
	* 1 = readable
	* 2 = readable long
	* 3 = SQL format
	* 4 = ION format */
	$theTime = $complete ? completeTime($str) : (empty($str) ? time() : strtotime($str));
	if($format==4) {
		$elms = explode(" ", microtime());
		return date("YmdHis", $theTime).$elms[0];	
	}
	$usecs = gettimeofday(false);
	if($format==3) return date("Y-m-d H:i:s", $theTime).".".$usecs["usec"];
	if($format==2) return date("Y-m-d H:i:s", $theTime);
	if($format==1) return date("Y-m-d"      , $theTime);
	return date("YmdHis", $theTime);
}





?>
