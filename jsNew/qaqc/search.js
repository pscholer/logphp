
// askSettingName
// --------------------------------------------
function askSettingName() {
	var tabsetname = prompt("Please enter a setting name:", "setting name");
	if (tabsetname != null) {
		tabsetname = tabsetname.replace(/ /g,"_");
		document.getElementById("setting").value = tabsetname;		
		return true;
	}
	return false;
}

// checkChoice
// --------------------------------------------
function checkChoice(value){
	var db = document.querySelector('input[name="dbchoice"]:checked').value;
	if(db=='elx' && value=='VMM3'){
		window.alert("Measurements of VMM3 have been moved to an external table and cannot be accessed via this WebView anymore. The size of the VMM3 data sample slowed down the QAQC ELX database by a lot while these data were very rarely queried. Please ask the QAQC DB administrator if you still want to access these data.");
		return false;
	}
	return true;
}
