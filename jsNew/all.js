
// changecolor
// --------------------------------------------
function changecolor(id){
	document.getElementById(id).classList.add("emphasized");
	//document.getElementById(id).style.color="#000000";	
	//document.getElementById(id).style.backgroundColor="#ffbf00";
}

// changePage
// --------------------------------------------
function changePage(newP){
	set('p', newP);
	submitIt('changePage');
}

// changeTheme
// --------------------------------------------
function changeTheme(newT){
	set('theme', newT);
	get('home');
}

// checkFileSize
// --------------------------------------------
function checkFileSize(name){
	var field = document.mainform.elements[name].files;
	if(!field || !field[0]) return true;
	if(field[0].size/1000/1000>2) {
		alert("The file you selected cannot be uploaded since it exceeds the maximal allowed size of 2 MB!");
		return false;
	}
	return true;
}

// checkNone
// --------------------------------------------
function checkNone(name){
	checkboxes = document.getElementsByName(name);
	for(var i=0; i<checkboxes.length; i++) {
		if(checkboxes[i].checked) return false;
	}
	return true;
}

// disableAll
// --------------------------------------------
function disableAll(){
	var elements = document.mainform.elements;
	for(var i=0; i<elements.length; ++i){
		elements[i].readonly = true;
	}
}

// exportTable
// --------------------------------------------
function exportTable(flag, ending){
	$(flag).tableExport({type: ending, escape:'false'});
}

// get
// --------------------------------------------
function get(what = "get"){
	set("get", what);
	document.mainform.submit();
}

// selectAll
// --------------------------------------------
function selectAll(name){
	if(!checkNone(name)) return true; // nothing to be completed
	checkboxes = document.getElementsByName(name);
	for(var i=0; i<checkboxes.length; i++) {
		checkboxes[i].checked = true;
	}
	return confirm("Attention: you didn't select any element in the table, so we have selected all elements in your table for you. If that's OK with you, click 'OK' to proceed. If not, click 'Cancel' in order to select the table entries manually.");
}

// toggle
// --------------------------------------------
function toggle(name, value1, value2){
	var element = document.getElementsByName(name)[0];
	if(element.value == value1) {
		element.value = value2;
		return;
	}
	element.value = value1;
}

// toggleAll
// --------------------------------------------
function toggleAll(name, source){
	checkboxes = document.getElementsByName(name);
	for(var i=0; i<checkboxes.length; i++) {
		checkboxes[i].checked = source.checked;
	}
}

// toggleAllBtn
// --------------------------------------------
function toggleAllBtn(name, sid){
	source = document.getElementById(sid);
	if(source.checked) source.checked = false;
	else               source.checked = true;
	toggleAll(name, source);
}

// toggleRows
// --------------------------------------------
function toggleRows(cname){
	trs = document.getElementsByClassName(cname);
	for(var i=0; i<trs.length; i++){
		if(trs[i].style.display === "none")
			trs[i].style.display = "block";
		else
			trs[i].style.display = "none";
	}
}

// toggleOnOff
// --------------------------------------------
function toggleOnOff(id){
	elm = document.getElementById(id);
	if(elm.style.display === "none")
		elm.style.display = "block";
	else 
		elm.style.display = "none";
}

// toggleShowAll
// --------------------------------------------
function toggleShowAll(){
	field = document.getElementsByName("showAll")[0];
	if(field.value=="1") field.value = "0";
	else                 field.value = "1";
}

// set
// --------------------------------------------
function set(name, value){
    document.getElementsByName(name)[0].value = value;
}

// setName
// --------------------------------------------
function setName(id, name){
    document.getElementById(id).name = name;
}

// submitIt
// --------------------------------------------
function submitIt(what){
	if(what=='save') disableAll();
	set('do', what);
	var elements = document.mainform.elements;
	for(var i = 0, element; element = elements[i++];){
		if(!element.reportValidity()) return;
	}
	document.mainform.submit();
}

// submitItAs
// --------------------------------------------
function submitItAs(doing, detail){
	set(doing, detail);
	submitIt(doing);
}

// toggleFontSize
// --------------------------------------------
function toggleFontSize() {
	var ftsize = document.querySelector('input[name="fontsize"]:checked').value;		
	if(ftsize == "100") {
		document.cookie = "fontsize=100";
		document.cookie = "checked100=checked";
		document.cookie = "checked80=";   	   	
		document.getElementById("body").setAttribute("style","font-size:100%;")   	
	} 
	else {
		document.cookie = "fontsize=80";   	   	 
		document.cookie = "checked100=";
		document.cookie = "checked80=checked";   	   	
		document.getElementById("body").setAttribute("style","font-size:80%;")    
	}
}

// toggleSort
// --------------------------------------------
function toggleSort(column) {
    var prev = document.getElementsByName("orderType")[0].value;
	var type = "asc";
	if     (prev=="asc" ) type="desc";
	else if(prev=="desc") type="none"; // resetting to 'no sorting'
	document.getElementsByName("orderBy"  )[0].value = column;
	document.getElementsByName("orderType")[0].value = type;
	return true;
}

// trimInpStr
// --------------------------------------------
function trimInpStr(id, field) {
	var byId = id;
	if(id == "typeId") {
		var inpField = document.getElementById(id).value;
		if(inpField == "mtf") { byId = field; }
		else                  { return;       }
	}   			
	var inpStr = document.getElementById(byId).value;
	document.getElementById(byId).value = inpStr.replace(/\s/g,""); 	
}

