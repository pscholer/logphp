
// askComment
// --------------------------------------------
function askComment() {
	var comtext = prompt("Please enter your comment", "new comment");
	if (comtext == null) return;
	document.getElementsByName("comText")[0].value = comtext;
	submitIt("newcom");
}


// askEventDate
// --------------------------------------------
function askEventDate() {
	var EvD = prompt("Please enter the event date in case the date of the event that you want to report is NOT the same as the date when you are reporting the action. Otherwise just skip it by pressing enter and today's date is used instead.", "YYYY-MM-DD");
	var isValEvD = /\d{4,}-\d{2,}-\d{2,}$/.test(EvD);
	if(isValEvD || EvD == "" || EvD == "YYYY-MM-DD") {
		document.getElementsByName("comdocEventDate")[0].value = EvD;
		return true;
	} 
	alert("Please give the event date in the valid format!");
	return false;
}


// askMtfId
// --------------------------------------------
function askMtfId() {
	var promptMTF = prompt("Please enter the MTF number (20MN X XXXX # ####) of existing Document(s)", "");
	if(promptMTF == null) return false;
	var MTF = promptMTF.replace(/\s+/g,"");
	if(MTF == "") return false;   	  
	document.getElementsByName("docMtfId")[0].value = MTF;
	return true;
}


// setStatLoc
// --------------------------------------------
function setStatLoc(){
	document.getElementsByName("writeStatLoc")[0].value = "1";
}


// toggleEventDate
// --------------------------------------------
function toggleEventDate() {
	document.getElementById("equipFieldEvDate").style.display = "block";
}


// toggleId
// --------------------------------------------
function toggleId(isChild=false) {
	var typeName = "typeIdParent";
	var elName   = "idParent"; 
	if(isChild){
		typeName = "typeIdChild";
		elName   = "idChild"; 
	}
	var type = document.getElementsByName(typeName)[0].value;
	if(document.getElementsByName(elName)[0].value=="") return;
	document.getElementsByName(elName)[0].value = document.getElementsByName(type)[0].value;
}


// toggleParenting
// --------------------------------------------
function toggleParenting(mtfId, useAsParent=false){
	if(useAsParent){
		document.getElementsByName("idChild" )[0].value = "";
		document.getElementsByName("idParent")[0].value = mtfId;
	}
	else {
		document.getElementsByName("idChild" )[0].value = mtfId;
		document.getElementsByName("idParent")[0].value = "";
	}
	document.getElementById("heritageMain").style.display = "block";
}
 
