
// clearShipId
// --------------------------------------------
function clearShipId() {
	document.getElementsByName("shipIntId")[0].value = "";
}

// clearTrans
// --------------------------------------------
function clearTrans(usersLocation) {
    document.getElementsByName("siteFromId")[0].value = 0;
    document.getElementsByName("siteToId"  )[0].value = usersLocation;
}

//// checkRecDate
//// --------------------------------------------
//function checkRecDate(){
//	var send     = document.getElementById('latestShipDate').value;
//	var sendDate = new Date(send);
//	var rec      = document.getElementById('inRecDate').value;
//	var recDate  = new Date(rec);
//	if(!isNaN(sendDate.valueOf()) && !isNaN(recDate.valueOf()) && sendDate.valueOf()>=recDate.valueOf()){
//		alert("Please enter a receiving date, which is after the sending date!");
//		return false;
//	} 
//	return true;
//}

// checkRecDateNew
// --------------------------------------------
function checkRecDateNew(){
	// in contrast to the other method, this one unwraps the ship date for every
	// entry and only looks at the elements selected
	var rec      = document.getElementsByName('recDate')[0].value;
	var recDate  = new Date(rec);
	if(!rec || rec.length==0 || isNaN(recDate.valueOf())){
		recDate = new Date(); // just use today
		//var yn = confirm("You have not given a valid receiving date so I will use NOW instead. Click 'OK' to confirm and 'Cancel' in order to specify a valid receiving date manually.");
		//if(!yn) return false;
	}	
	var allDates = document.getElementsByName("itemsDates" )[0].value.split(";");
	var items    = document.getElementsByName("subRanges[]")[0].value;
	if(!allDates || !items) return true;
	var dates    = [];
	for(var i = 0; i < allDates.length; i++){
		var elm = allDates[i].split(",");
		if(!items.includes(elm[0])) continue;
		dates.push(elm[1]);
	}
	dates.sort();
	var sendDate = new Date(dates[dates.length - 1]);
	if(!isNaN(sendDate.valueOf()) && sendDate.valueOf()>recDate.valueOf()){
		alert("Please enter a receiving date, which is after the sending date!");
		return false;
	} 
	return true;
}


// checkRecEquip
// --------------------------------------------
function checkRecEquip() {
	var destId     = document.getElementsByName("locId"    )[0].value;
	var statusId   = document.getElementsByName("statusId" )[0].value;
	var person     = document.getElementsByName("recPerson")[0].value;
	if (destId == 0 || destId == 27) {
		alert("No valid destination selected!");
		return false;
	} 
	else if (statusId == 0 || statusId == 26) {
		alert("No valid status selected!");
		return false;
	}
	else if(!person || person.length === 0){
		alert("Please give a valid receiving person!");
		return false;
	} 
	else if(!checkRecDateNew()){
		return false;
	} 
	else if(!checkRecLocs()){
		return false;
	} 
	return true;
}


// checkRecLocs
// --------------------------------------------
function checkRecLocs() {
	var destId   = document.getElementsByName("locId"      )[0].value;
	var allToIds = document.getElementsByName("itemsToIds" )[0].value.split(";");
	var items    = document.getElementsByName("subRanges[]")[0].value;
	if(!allToIds || !items) return true;
	var toIds    = [];
	for(var i = 0; i < allToIds.length; i++){
		var elm = allToIds[i].split(",");
		if(!items.includes(elm[0])) continue;
		if( toIds.includes(elm[1])) continue;
		toIds.push(elm[1]);
	}
	if(toIds.length<1) 
		return false; // nothing selected
	if(toIds.length>1){
		alert("You cannot receive equipments sent to different locations at the same time!");
		return false;
	}
	if(destId != toIds[0]){
		alert("You cannot receive equipments at different location than they have been sent to!");
		return false;
	} 
	return true;
}

//// toggleSelectAll
//// --------------------------------------------
//function toggleSelectAll(){
//	var checkAll   = document.getElementById("checkAll");
//	var checkboxes = document.getElementsByClassName("itemId");
//	if (checkAll.checked) {
//		for (var i in checkboxes){
//			checkboxes[i].checked = true;
//		}
//	} 
//	else {
//		for (var i in checkboxes){
//			checkboxes[i].checked = false;
//		}
//	}
//}

