
// checkAliasRange
// --------------------------------------------
function checkAliasRange() {
	var quantity  = document.getElementsByName("quantity" )[0].value;
	var itemfirst = document.getElementsByName("rangeFrom")[0].value;
	var itemlast  = document.getElementsByName("rangeTo"  )[0].value;
	if (itemfirst == "" && itemlast == "") {
		return confirm("WARNING: You only entered an Other ID (EQ Alias) but no range. EQ Alias for the splitted items will be generated automtically!");
	}
	var range = parseInt(itemlast) - parseInt(itemfirst) + 1;
	if (range != parseInt(quantity)) {
		alert("ERROR: The EQ Alias range you specified is invalid, numbers don't add up!");
		return false;
	} 
	return true;
}


// confirmIndepSplitting
// --------------------------------------------
function confirmIndepSplitting(){
	if(document.getElementsByName("splitIndep")[0].checked){
		alert("WARNING: This is NOT a standard operation! Please BE SURE that you want to do this and that you know what you are doing! If you are not sure, please ask Kim et al before carrying out this operation!");
	}
}


// setRangeTo
// --------------------------------------------
function setRangeTo() {
	var quantity  = document.getElementsByName("quantity" )[0].value;	
	var itemfirst = document.getElementsByName("rangeFrom")[0].value; 
	var itemlast  = parseInt(itemfirst) + parseInt(quantity) - 1;
	document.getElementsByName("rangeTo")[0].value = itemlast;
}


// setRangeFrom
// --------------------------------------------
function setRangeFrom() {
	var quantity  = document.getElementsByName("quantity")[0].value;
	var itemlast  = document.getElementsByName("rangeTo" )[0].value;
	var itemfirst = parseInt(itemlast) - parseInt(quantity) + 1;
	document.getElementsByName("rangeFrom")[0].value = itemfirst;
}


// setRangeMaxs
// --------------------------------------------
function setRangeMaxs() {
	var quantity = document.getElementsByName("quantity"   )[0].value;	
	var digits   = document.getElementsByName("rangeDigits")[0].value;
	inFrom       = document.getElementsByName("rangeFrom"  )[0];
	inTo         = document.getElementsByName("rangeTo"    )[0];         	
	if (digits == 3) {
		inFrom.setAttribute("max", 999 - parseInt(quantity)); // set a new value;
		inTo  .setAttribute("max", 999); // set a new value;
	} else if (digits == 4) {
		inFrom.setAttribute("max", 9999 - parseInt(quantity)); // set a new value;
		inTo  .setAttribute("max", 9999); // set a new value;
	} else {
		inFrom.setAttribute("max", 99999 - parseInt(quantity)); // set a new value;
		inTo  .setAttribute("max", 99999); // set a new value;
	}	
}


// setSeqFieldsColor
// --------------------------------------------
function setSeqFieldsColor() {
	document.getElementsByName("rangeOtherId")[0].style.color="#333333";
	document.getElementsByName("rangeFrom"   )[0].style.color="#333333";
	document.getElementsByName("rangeTo"     )[0].style.color="#333333";
	document.getElementsByName("rangeDigits" )[0].style.color="#333333";			 
}

