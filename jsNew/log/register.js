
// checkEqType
// --------------------------------------------
function checkEqType(){
	var eqType  = document.getElementsByName("eqType")[0].value;
	var pattern = document.getElementsByName("eqType")[0].getAttribute("pattern");
	if(eqType.match(pattern)){
		return true;
	} 
	return false;
}


// checkEqVersion
// --------------------------------------------
function checkEqVersion(){
	var eqVersion = document.getElementsByName("eqVersion")[0].value;
	var highest   = document.getElementsByName("eqVersion")[0].getAttribute("max");
	if(eqVersion <= highest){
		return true;
	}
	return false;
}


// checkAll
// --------------------------------------------
function checkAll(){
	var mtf = document.getElementsByName("mtfId")[0].value;
	if(!checkEqType()){
		window.alert("You need to provide an equipment type and version and assemble the MTF Id first before the equipment can be registered!");
		return false;
	}
	if(!checkEqVersion()){
		window.alert("You need to provide a valid equipment version in order to register your equipment!");
		return false;
	}
	if(!mtf || mtf.length===0){
		window.alert("MTF Id still not assembled: please confirm the version number first!");
		return false;
	}
	if(document.getElementsByName("quantity").length>0){
		var quant = document.getElementsByName("quantity")[0].value;
		if(!quant || quant.length===0){
			window.alert("Please give a valid quantity value!");
			return false;
		}
	}
	return true;
}


